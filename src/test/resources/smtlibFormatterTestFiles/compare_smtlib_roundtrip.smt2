(a = b)
(a = b = c)
(a ∨ b)
(a ∧ b)
(a ∨ b ∨ c)
(a ∧ b ∧ c)
¬a
(t(0) = x(0))
(3 · 3.0)
(3 · 3.0 · 0xABCdef)
(3 + 0b101)
(3 + 0b101 + "string")
(3 / 3.0)
(3 - 0b101)
∃ i ∈ ℤ:i
∀ i ∈ ℤ:i
∃ i ∈ ℤ, a ∈ Bool:i
∀ i ∈ ℤ, a ∈ Bool:i
let x := 4.0 in42.123
a pattern p attribute moreAttributes named "stringConstant" noValue emptyListValue () listValue (v1 v2) listValue (v1)
a list ((v1 v1) v2)
(a ? b : c)
ite(a, b, c, d)
ite(a, b)
f as ℤ(a)
x[index, 7]
ℤ
f as ℤ Bool ℤ(a)
reset
push 6
pop 7
const a: Nat
fun f: ∅ → C
fun f: A → C
fun f: (A, B) → C
(define-fun g () Int 42)
(define-fun g ((a Int)) Int (+ a 3))
(define-fun g ((a Int) (b Nat)) Int (+ a b))
check-sat