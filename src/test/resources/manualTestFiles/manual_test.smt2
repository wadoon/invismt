; for solver tests
(push 1)
	(declare-fun IsNat (Int) Bool)
	(assert (IsNat 0))
	(assert (forall ((x Int)) (ite (IsNat (+ x 1)) (or (= x 0) (IsNat x)) (and (= x 0) (IsNat x)))))
	(check-sat)
(pop 1)

; for splitAnd, splitOr and DeMorgan tests
(push 1)
	(declare-const a Bool)
	(declare-const b Bool)
	(push 1)
		(assert (! (and a b) :attribute "value" :named A))
		(check-sat)
	(pop 1)

	(push 1)
		(assert (or a b))
		(check-sat)
	(pop 1)
	
	(push 1)
		(assert (not (and a b)))
		(check-sat)
	(pop 1)
(pop 1)

; for induction and instantiation tests
(push 1)
	(declare-const x Int)
	(assert (forall ((y Int)) (= x y)))
	(check-sat)
(pop 1)

; skolemization
(push 1)
	(declare-const x Int)
	(assert (exists ((y Int)) (= x y)))
	(check-sat)
(pop 1)

; cut rule tests
(push 1) 
	(declare-fun f () Int)
	(assert (> f 0))
	(check-sat)
(pop 1)

; rewrite tests and type checking
(push 1)
	(declare-const a Bool)
	(declare-const b Bool)
	(assert (=> a b))
	(check-sat)
(pop 1)

; excess syntax constructs
(push 1)
	(assert (let ((a true)) a))
	(check-sat)
(pop 1)
