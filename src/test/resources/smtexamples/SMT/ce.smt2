;==========OPTIONS==========


;(set-option :print-success true) 

(set-option :produce-models true)

(set-logic FP)

;==========SORTS==========

;==========FUNCTIONS==========



(declare-fun |_x0_0| 
 () 
 (_ FloatingPoint 8 24))






(declare-fun |_x3_0| 
 () 
 (_ FloatingPoint 8 24))




(declare-fun in0 
 () 
 (_ FloatingPoint 8 24))


(declare-fun |_x2_0| 
 () 
 (_ FloatingPoint 8 24))




(declare-fun |_x1_0| 
 () 
 (_ FloatingPoint 8 24))

;==========ASSERTIONS==========

;The negated proof obligation

(assert 
 (not
 
  (=>
  
   (and
   
    (fp.eq
    |_x0_0|
    
     (fp #b0 #b00000000 #b00000000000000000000000)
   )
      
   
    (fp.leq
    
     (fp.neg
     
      (fp #b0 #b01111111 #b00000000000000000000000)
    )
    in0
   )
   
    (fp.leq
    in0
    
     (fp #b0 #b01111111 #b00000000000000000000000)
   )
   
    (fp.leq
    
     (fp.neg
     
      (fp #b0 #b01111111 #b00000000000000000000000)
    )
    |_x0_0|
   )
   
    (fp.leq
    |_x0_0|
    
     (fp #b0 #b01111111 #b00001100110011001100110)
   )
   
    (fp.leq
    
     (fp.neg
     
      (fp #b0 #b01111111 #b00000000000000000000000)
    )
    |_x1_0|
   )
   
    (fp.leq
    |_x1_0|
    
     (fp #b0 #b01111111 #b00001100110011001100110)
   )
   
    (fp.leq
    
     (fp.neg
     
      (fp #b0 #b01111111 #b00000000000000000000000)
    )
    |_x2_0|
   )
   
    (fp.leq
    |_x2_0|
    
     (fp #b0 #b01111111 #b00000000000000000000000)
   )
   
    (fp.leq
    
     (fp.neg
     
      (fp #b0 #b01111111 #b00000000000000000000000)
    )
    |_x3_0|
   )
   
    (fp.leq
    |_x3_0|
    
     (fp #b0 #b01111111 #b00000000000000000000000)
   )
   
    (fp.leq
    
     (fp.add
     RNE
     
      (fp.add
      RNE
      
       (fp.add
       RNE
       
        (fp.add
        RNE
        
         (fp.add
         RNE
         
          (fp.add
          RNE
          
           (fp.add
           RNE
           
            (fp.add
            RNE
            
             (fp.add
             RNE
             
              (fp.add
              RNE
              
               (fp.mul
               RNE
               
                (fp.neg
                
                 (fp #b0 #b01110101 #b00000110001001001101111)
               )
               |_x1_0|
              )
              
               (fp.mul
               RNE
               
                (fp.mul
                RNE
                
                 (fp #b0 #b01111110 #b00010001011010000111001)
                |_x0_0|
               )
               |_x0_0|
              )
             )
             
              (fp.mul
              RNE
              
               (fp.mul
               RNE
               
                (fp.neg
                
                 (fp #b0 #b01111111 #b00000000000000000000000)
               )
               |_x0_0|
              )
              |_x1_0|
             )
            )
            
             (fp.mul
             RNE
             
              (fp.mul
              RNE
              
               (fp.neg
               
                (fp #b0 #b01111110 #b00010000011000100100111)
              )
              |_x0_0|
             )
             |_x2_0|
            )
           )
           
            (fp.mul
            RNE
            
             (fp.mul
             RNE
             
              (fp #b0 #b01111101 #b11010111000010100011111)
             |_x0_0|
            )
            |_x3_0|
           )
          )
          
           (fp.mul
           RNE
           
            (fp.mul
            RNE
            
             (fp #b0 #b01111110 #b00001101110100101111001)
            |_x1_0|
           )
           |_x1_0|
          )
         )
         
          (fp.mul
          RNE
          
           (fp.mul
           RNE
           
            (fp #b0 #b01111101 #b11111101111100111011011)
           |_x1_0|
          )
          |_x2_0|
         )
        )
        
         (fp.mul
         RNE
         
          (fp.mul
          RNE
          
           (fp.neg
           
            (fp #b0 #b01111101 #b11111000110101001111111)
          )
          |_x1_0|
         )
         |_x3_0|
        )
       )
       
        (fp.mul
        RNE
        
         (fp.mul
         RNE
         
          (fp #b0 #b01111100 #b00100110111010010111100)
         |_x2_0|
        )
        |_x2_0|
       )
      )
      
       (fp.mul
       RNE
       
        (fp.mul
        RNE
        
         (fp.neg
         
          (fp #b0 #b01111100 #b11010111000010100011111)
        )
        |_x2_0|
       )
       |_x3_0|
      )
     )
     
      (fp.mul
      RNE
      
       (fp.mul
       RNE
       
        (fp #b0 #b01111100 #b00000110001001001101111)
       |_x3_0|
      )
      |_x3_0|
     )
    )
    
     (fp #b0 #b01111011 #b00000010000011000100101)
   )
  )
  
   (fp.leq
   
    (fp.add
    RNE
    
     (fp.add
     RNE
     
      (fp.sub
      RNE
      
       (fp.sub
       RNE
       
        (fp.mul
        RNE
        
         (fp #b0 #b01111111 #b10000000000000000000000)
        |_x0_0|
       )
       
        (fp.mul
        RNE
        
         (fp #b0 #b01111110 #b01100110011001100110011)
        |_x1_0|
       )
      )
      
       (fp.mul
       RNE
       
        (fp #b0 #b01111110 #b01100110011001100110011)
       |_x2_0|
      )
     )
     
      (fp.mul
      RNE
      
       (fp #b0 #b01111101 #b10011001100110011001101)
      |_x3_0|
     )
    )
    
     (fp.mul
     RNE
     
      (fp #b0 #b01111110 #b00000000000000000000000)
     in0
    )
   )
   
    (fp #b0 #b01111111 #b00001100110011001100110)
  )
 )
))

(check-sat)
(get-model)
                                        ;==========UNTRANSLATED PARTS==========

;*Untranslated term:  wellFormed(heap)
;    No translation for function wellFormed
;(1::Formula)
;
;*Untranslated term:  equals(examples.pinebench.PineBench::exactInstance(self),TRUE)<<origin(requires (implicit)) ([])>>
;    No translation for function examples.pinebench.PineBench::exactInstance
;(1::boolean)
;
;*Untranslated term:  wellFormed(anon_heap_LOOP<<anonHeapFunction>>)
;    No translation for function wellFormed
;(1::Formula)
;
;*Untranslated term:  equals(self<<origin(requires (implicit)) ([])>>,null)<<origin(requires (implicit)) ([requires (implicit)])>>
;    Translation Failed: Unsupported Sort: examples.pinebench.PineBench
