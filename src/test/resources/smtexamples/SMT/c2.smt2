; --- Preamble
(set-option :produce-models true)

(set-logic FP)
    ; --- Declarations
(define-fun u2f ((x Float32)) Float32 x)


(declare-fun ui__x1_0 
 () Float32)

(declare-fun ui__x0_0 
 () Float32)

(declare-fun ui__x2_0 
 () Float32)

(declare-fun ui__x3_0 
 () Float32)

(declare-fun ui_in0 
 () Float32)

(declare-fun ui_x3 
 () Float32)

(declare-fun ui_x2 
 () Float32)

(declare-fun ui_x1 
 () Float32)

(declare-fun ui_x0 
 () Float32)
; --- Sequent

(assert 
 (fp.leq 
  (fp.add RNE 
   (fp.add RNE 
    (fp.add RNE 
     (fp.add RNE 
      (fp.add RNE 
       (fp.add RNE 
        (fp.add RNE 
         (fp.add RNE 
          (fp.add RNE 
           (fp.add RNE 
            (fp.mul RNE 
             (fp.neg 
              (fp #b0 #b01110101 #b00000110001001001101111)) 
             (u2f ui__x1_0)) 
            (fp.mul RNE 
             (fp.mul RNE 
              (fp #b0 #b01111110 #b00010001011010000111001) 
              (u2f ui__x0_0)) 
             (u2f ui__x0_0))) 
           (fp.mul RNE 
            (fp.mul RNE 
             (fp.neg 
              (fp #b0 #b01111111 #b00000000000000000000000)) 
             (u2f ui__x0_0)) 
            (u2f ui__x1_0))) 
          (fp.mul RNE 
           (fp.mul RNE 
            (fp.neg 
             (fp #b0 #b01111110 #b00010000011000100100111)) 
            (u2f ui__x0_0)) 
           (u2f ui__x2_0))) 
         (fp.mul RNE 
          (fp.mul RNE 
           (fp #b0 #b01111101 #b11010111000010100011111) 
           (u2f ui__x0_0)) 
          (u2f ui__x3_0))) 
        (fp.mul RNE 
         (fp.mul RNE 
          (fp #b0 #b01111110 #b00001101110100101111001) 
          (u2f ui__x1_0)) 
         (u2f ui__x1_0))) 
       (fp.mul RNE 
        (fp.mul RNE 
         (fp #b0 #b01111101 #b11111101111100111011011) 
         (u2f ui__x1_0)) 
        (u2f ui__x2_0))) 
      (fp.mul RNE 
       (fp.mul RNE 
        (fp.neg 
         (fp #b0 #b01111101 #b11111000110101001111111)) 
        (u2f ui__x1_0)) 
       (u2f ui__x3_0))) 
     (fp.mul RNE 
      (fp.mul RNE 
       (fp #b0 #b01111100 #b00100110111010010111100) 
       (u2f ui__x2_0)) 
      (u2f ui__x2_0))) 
    (fp.mul RNE 
     (fp.mul RNE 
      (fp.neg 
       (fp #b0 #b01111100 #b11010111000010100011111)) 
      (u2f ui__x2_0)) 
     (u2f ui__x3_0))) 
   (fp.mul RNE 
    (fp.mul RNE 
     (fp #b0 #b01111100 #b00000110001001001101111) 
     (u2f ui__x3_0)) 
    (u2f ui__x3_0))) 
  (fp #b0 #b01111011 #b00000010000011000100101)))

(assert 
 (fp.leq 
  (u2f ui__x3_0) 
  (fp #b0 #b01111111 #b00000000000000000000000)))

(assert 
 (fp.leq 
  (fp.neg 
   (fp #b0 #b01111111 #b00000000000000000000000)) 
  (u2f ui__x3_0)))

(assert 
 (fp.leq 
  (u2f ui__x2_0) 
  (fp #b0 #b01111111 #b00000000000000000000000)))

(assert 
 (fp.leq 
  (fp.neg 
   (fp #b0 #b01111111 #b00000000000000000000000)) 
  (u2f ui__x2_0)))

(assert 
 (fp.leq 
  (u2f ui__x1_0) 
  (fp #b0 #b01111111 #b00001100110011001100110)))

(assert 
 (fp.leq 
  (fp.neg 
   (fp #b0 #b01111111 #b00000000000000000000000)) 
  (u2f ui__x1_0)))

(assert 
 (fp.leq 
  (u2f ui__x0_0) 
  (fp #b0 #b01111111 #b00001100110011001100110)))

(assert 
 (fp.leq 
  (fp.neg 
   (fp #b0 #b01111111 #b00000000000000000000000)) 
  (u2f ui__x0_0)))

(assert 
 (fp.leq 
  (u2f ui_in0) 
  (fp #b0 #b01111111 #b00000000000000000000000)))

(assert 
 (fp.leq 
  (fp.neg 
   (fp #b0 #b01111111 #b00000000000000000000000)) 
  (u2f ui_in0)))

(assert 
 (fp.eq 
  (u2f ui_x3) 
  (fp #b0 #b00000000 #b00000000000000000000000)))

(assert 
 (fp.eq 
  (u2f ui_x2) 
  (fp #b0 #b00000000 #b00000000000000000000000)))

(assert 
 (fp.eq 
  (u2f ui_x1) 
  (fp #b0 #b00000000 #b00000000000000000000000)))

(assert 
 (fp.eq 
  (u2f ui_x0) 
  (fp #b0 #b00000000 #b00000000000000000000000)))

(assert 
 (fp.eq 
  (u2f ui__x0_0) 
  (fp #b0 #b00000000 #b00000000000000000000000)))

(assert 
 (not 
  (fp.leq 
   (fp.add RNE 
    (fp.add RNE 
     (fp.sub RNE 
      (fp.sub RNE 
       (fp.mul RNE 
        (fp #b0 #b01111111 #b10000000000000000000000) 
        (u2f ui__x0_0)) 
       (fp.mul RNE 
        (fp #b0 #b01111110 #b01100110011001100110011) 
        (u2f ui__x1_0))) 
      (fp.mul RNE 
       (fp #b0 #b01111110 #b01100110011001100110011) 
       (u2f ui__x2_0))) 
     (fp.mul RNE 
      (fp #b0 #b01111101 #b10011001100110011001101) 
      (u2f ui__x3_0))) 
    (fp.mul RNE 
     (fp #b0 #b01111110 #b00000000000000000000000) 
     (u2f ui_in0))) 
   (fp #b0 #b01111111 #b00001100110011001100110))))

(check-sat)
(get-model)
