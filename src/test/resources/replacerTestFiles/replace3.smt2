(assert (param (! a :pattern p) param))
(assert (param (match term ((a b))) param))
(assert (forall ((x Int)) (param (! a :pattern p) param)))
(assert (forall ((x Int)) (param (match term ((a b))) param)))