/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver.z3;

import invismt.solver.Solver;
import invismt.solver.SolverProvider;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class Z3SolverProviderTest {

	private final String configDir = "/config";

	/**
	 * Tests if External Z3 config exists and checks if the Config is right
	 */
	@Test
	void shouldReadInExternalZ3() {
		SolverProvider solverProvider = new Z3SolverProvider();
		List<? extends Solver> list = solverProvider.getSolvers(configDir);
		for (Solver solver : list) {
			if (solver.getName().equals("Z3")) {
				if (solver.getCommand().size() != 1) {
					fail();
				}
				assertEquals("z3", solver.getCommand().get(0));
				return;
			}
		}
		fail();
	}
}