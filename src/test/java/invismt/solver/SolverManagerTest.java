/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver;

import invismt.solver.z3.Z3SolverProvider;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SolverManagerTest {

	/**
	 * Test if all Existing Solver are read in.
	 */
	@Test
	void shouldReadAllExistingSolvers() {
		SolverManager solverManager = new SolverManager();
		String configDir = "/config";
		solverManager.addProvider(new Z3SolverProvider(), configDir);
		// Need to be updated if new Solver are Added
		String[] nameOfSolvers = new String[]{"Z3", "Z3 TimeOut 10s"};
		List<Solver> solvers = solverManager.getSolvers();
		for (int i = 0; i < solvers.size(); i++) {
			assertEquals(nameOfSolvers[i], solvers.get(i).getName());
		}
	}
}