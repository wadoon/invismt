/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;

import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.rule.CreationRule;
import invismt.rule.Rule;
import invismt.solver.AnswerType;
import invismt.solver.SolverAnswer;
import invismt.solver.SolverService;
import invismt.tree.exception.ConflictingStateException;
import invismt.tree.exception.PTElementLocked;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

/**
 * @author Tim Junginger
 */
@ExtendWith(MockitoExtension.class)
class PTElementTest {

	private static final String ELEMENTLOCKED = "Element threw PTElementLockedException when element was not locked.";
	private static boolean notified;
	@Mock
	private static Rule rule;

	/**
	 * Test {@link PTElement#getParent()} and {@link PTElement#setParent(PTElement)}.
	 */
	@Test
	void shouldReturnCorrectParent() {
		// initial parent
		PTElement parent = new PTElement(null, null, null, null);
		PTElement child = new PTElement(parent, null, null, null);
		assertSame(parent, child.getParent());
		// set parent
		PTElement parent2 = new PTElement(null, null, null, null);
		child.setParent(parent2);
		assertSame(parent2, child.getParent());
	}

	/**
	 * Test {@link PTElement#getSatisfiable()} and {@link PTElement#setSatisfiable(ProofState)}.
	 */
	@Test
	void shouldReturnCorrectSatisfiable() {
		PTElement element = new PTElement(null, null, null, null);
		assertSame(ProofState.UNKNOWN, element.getSatisfiable());
		try {
			element.setSatisfiable(ProofState.SAT);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertSame(ProofState.SAT, element.getSatisfiable());
	}

	/**
	 * Test {@link PTElement#getScript()} and {@link PTElement#setScript(Script)}.
	 */
	@Test
	void shouldReturnCorrectScript() {
		Script script = new Script(List.of(new SyntaxCommand.CheckSat()));
		PTElement element = new PTElement(null, script, null, null);
		assertSame(script, element.getScript());
		Script script2 = new Script(List.of(new SyntaxCommand.Exit()));
		element.setScript(script2);
		assertSame(script2, element.getScript());
	}

	/**
	 * Test {@link PTElement#getRule()}.
	 */
	@Test
	void shouldReturnCorrectRule() {
		SyntaxCommand.CheckSat check = new SyntaxCommand.CheckSat();
		Script script = new Script(List.of(check));
		Rule rule = new CreationRule(script, check);
		PTElement element = new PTElement(null, null, rule, null);
		assertSame(rule, element.getRule());
	}

	/**
	 * Test {@link PTElement#getChildren()}.
	 */
	@Test
	void shouldReturnCorrectChildren() {
		PTElement element = new PTElement(null, null, null, null);
		PTElement child1 = new PTElement(element, null, null, null);
		PTElement child2 = new PTElement(element, null, null, null);
		element.addChildren(child1);
		element.addChildren(child2);
		assertEquals(2, element.getChildren().size());
		assertSame(child1, element.getChildren().get(0));
		assertSame(child2, element.getChildren().get(1));
	}

	/**
	 * Test {@link PTElement#rmChildren(PTElement)}.
	 */
	@Test
	void shouldRemoveCorrectChildren() {
		PTElement element = new PTElement(null, null, null, null);
		PTElement child1 = new PTElement(element, null, null, null);
		PTElement child2 = new PTElement(element, null, null, null);
		element.addChildren(child1);
		element.addChildren(child2);
		element.rmChildren(child1);
		assertEquals(1, element.getChildren().size());
		assertSame(child2, element.getChildren().get(0));
	}

	/**
	 * Test {@link PTElement#getSolverService()}.
	 */
	@Test
	void shouldReturnCorrectService() {
		PTElement element = new PTElement(null, null, null, null);
		SolverService service = new SolverService(null, null) {
			@Override
			public void start() {
				// ignore
			}
		};
		assertNull(element.getSolverService());
		try {
			element.lock(service);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		}
		assertTrue(element.isLocked());
		assertSame(service, element.getSolverService());
		try {
			element.unlockAbort();
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertFalse(element.isLocked());
		assertNull(element.getSolverService());
	}

	/**
	 * Test {@link PTElement#unlockAbort()}.
	 */
	@Test
	void shouldAlwaysUnlockAbortSuccessfully() {
		PTElement element = new PTElement(null, null, null, null);
		assertNull(element.getSolverService());
		try {
			element.unlockAbort();
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertFalse(element.isLocked());
		assertNull(element.getSolverService());
	}

	/**
	 * Test {@link PTElement#unlock(invismt.solver.SolverAnswer)}.
	 */
	@Test
	void shouldNotUnlockWithSetSatisfiableWhenNotLocked() {
		PTElement element = new PTElement(null, null, null, null);
		SolverAnswer answer = new SolverAnswer(AnswerType.SAT, "");
		assertSame(ProofState.UNKNOWN, element.getSatisfiable());
		assertFalse(element.isLocked());
		try {
			element.unlock(answer);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertSame(ProofState.UNKNOWN, element.getSatisfiable());
	}

	/**
	 * Test {@link PTElement#unlock(invismt.solver.SolverAnswer)}.
	 */
	@Test
	void shouldUnlockWithSatisfiableWhenLocked() {
		PTElement element = new PTElement(null, null, null, null);
		SolverService service = new SolverService(null, null) {
			@Override
			public void start() {
				// ignore
			}
		};
		SolverAnswer answer = new SolverAnswer(AnswerType.SAT, "");
		try {
			element.lock(service);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		}
		assertTrue(element.isLocked());
		try {
			element.unlock(answer);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertFalse(element.isLocked());
		assertSame(ProofState.SAT, element.getSatisfiable());
		assertNull(element.getSolverService());
	}

	/**
	 * Test {@link PTElement#setSatisfiable(ProofState)}.
	 */
	@Test
	void shouldNotSetSatisfiableWhenLocked() {
		PTElement element = new PTElement(null, null, null, null);
		SolverService service = new SolverService(null, null) {
			@Override
			public void start() {
				// ignore
			}
		};
		try {
			element.lock(service);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		}
		assertTrue(element.isLocked());
		assertThrows(PTElementLocked.class, () -> element.setSatisfiable(ProofState.SAT));
	}

	/**
	 * Test {@link PTElement}.
	 */
	@Test
	void shouldPreventChildWithWrongParent() {
		PTElement parent1 = new PTElement(null, null, null, null);
		PTElement parent2 = new PTElement(null, null, null, null);
		PTElement child = new PTElement(parent2, null, null, null);

		parent1.addChildren(child);
		// either parent1 does not have child as child because child has wrong parent
		// or child's parent is set to parent1.
		assertTrue(!parent1.getChildren().contains(child) || child.getParent() == parent1);
	}

	/**
	 * Test {@link PTElement}.
	 */
	@Test
	void shouldPreventChildWithWrongParentConstructor() {
		PTElement child = new PTElement(null, null, null, null);
		PTElement parent = new PTElement(null, List.of(child), null, null, null);
		assertTrue(!parent.getChildren().contains(child) || child.getParent() == parent);
	}

	/**
	 * Test {@link PTElement#lock(SolverService)}.
	 */
	@Test
	void shouldThrowPTElementLockedExceptionWhenAlreadyLocked() {
		PTElement element = new PTElement(null, null, null, null);
		SolverService service = new SolverService(null, null) {
			@Override
			public void start() {
				// ignore
			}
		};
		try {
			element.lock(service);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		}
		assertTrue(element.isLocked());
		try {
			element.lock(service);
			fail("Element is locked, but could be locked again.");
		} catch (PTElementLocked e) {
			return;
		}
	}

	/**
	 * Test {@link PTElement#notifyListeners()}.
	 */
	@Test
	void shouldNotifyListenersOnChange() {
		PTElement element = new PTElement(null, null, null, null);
		notified = false;
		element.addChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				notified = true;
			}
		});
		element.notifyListeners();
		assertEquals(true, notified);
	}

	/**
	 * Test {@link PTElement#rmChangeListener(PropertyChangeListener)}.
	 */
	@Test
	void shouldRemoveListenerCorrect() {
		PTElement element = new PTElement(null, null, null, null);
		notified = false;
		PropertyChangeListener listener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				notified = true;
			}
		};
		element.addChangeListener(listener);
		element.notifyListeners();
		assertEquals(true, notified);
		element.rmChangeListener(listener);
		notified = false;
		element.notifyListeners();
		assertEquals(false, notified);
	}

	/**
	 * Tests {@link PTElement#evaluateSatisfiableFromChildren()}
	 */
	@Test
	void shouldBecomeUnsatWhenChildrenUnsat() {
		PTElement parent = new PTElement(null, null, null, null);
		PTElement child = new PTElement(parent, null, rule, null);
		parent.addChildren(child);
		try {
			child.setSatisfiable(ProofState.UNSAT);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertEquals(ProofState.INTERNAL_UNSAT, parent.getSatisfiable());
	}

	/**
	 * Tests {@link PTElement#evaluateSatisfiableFromChildren()}
	 */
	@Test
	void shouldNotBecomeUnsatWhenOneChildSat() {
		PTElement parent = new PTElement(null, null, null, null);
		PTElement child1 = new PTElement(parent, null, rule, null);
		PTElement child2 = new PTElement(parent, null, rule, null);
		parent.addChildren(child1);
		parent.addChildren(child2);
		try {
			child1.setSatisfiable(ProofState.UNSAT);
			child2.setSatisfiable(ProofState.SAT);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertNotEquals(ProofState.INTERNAL_UNSAT, parent.getSatisfiable());
	}

	/**
	 * Tests {@link PTElement#evaluateSatisfiableFromChildren()}
	 */
	@Test
	void shouldBecomeUnsatWhenChildrenOfChildrenUnsat() {
		PTElement parent = new PTElement(null, null, null, null);
		PTElement child = new PTElement(parent, null, rule, null);
		PTElement child2 = new PTElement(child, null, rule, null);
		parent.addChildren(child);
		child.addChildren(child2);
		try {
			child2.setSatisfiable(ProofState.UNSAT);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertEquals(ProofState.INTERNAL_UNSAT, parent.getSatisfiable());
	}

	/**
	 * Tests {@link PTElement#evaluateSatisfiableFromChildren()}
	 */
	@Test
	void shouldNotBecomeUnsatWhenOneChildOfChildSat() {
		PTElement parent = new PTElement(null, null, null, null);
		PTElement child = new PTElement(parent, null, rule, null);
		PTElement child2 = new PTElement(child, null, rule, null);
		PTElement child3 = new PTElement(child, null, rule, null);
		parent.addChildren(child);
		child.addChildren(child2);
		child.addChildren(child3);
		try {
			child2.setSatisfiable(ProofState.UNSAT);
			child3.setSatisfiable(ProofState.SAT);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertNotEquals(ProofState.INTERNAL_UNSAT, parent.getSatisfiable());
	}

	/**
	 * Tests {@link PTElement#evaluateSatisfiableFromChildren()}
	 */
	@Test
	void shouldBecomeSatWhenAllChildrenSatAndConfluent() {
		when(rule.isConfluent()).thenReturn(true);
		PTElement parent = new PTElement(null, null, null, null);
		PTElement child = new PTElement(parent, null, rule, null);
		parent.addChildren(child);
		try {
			child.setSatisfiable(ProofState.SAT);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertEquals(ProofState.INTERNAL_SAT, parent.getSatisfiable());
	}

	/**
	 * Tests {@link PTElement#evaluateSatisfiableFromChildren()}
	 */
	@Test
	void shouldBecomeSatWhenAllChildreOfChildrenSatAndConfluent() {
		when(rule.isConfluent()).thenReturn(true);
		PTElement parent = new PTElement(null, null, null, null);
		PTElement child = new PTElement(parent, null, rule, null);
		PTElement child2 = new PTElement(parent, null, rule, null);
		parent.addChildren(child);
		child.addChildren(child2);
		try {
			child2.setSatisfiable(ProofState.SAT);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertEquals(ProofState.INTERNAL_SAT, parent.getSatisfiable());
	}

	/**
	 * Tests {@link PTElement#evaluateSatisfiableFromChildren()}
	 */
	@Test
	void shouldNotBecomeSatWhenNotConfluent() {
		PTElement parent = new PTElement(null, null, null, null);
		PTElement child = new PTElement(parent, null, rule, null);
		parent.addChildren(child);
		try {
			child.setSatisfiable(ProofState.SAT);
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		try {
			parent.evaluateSatisfiableFromChildren();
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertNotEquals(ProofState.INTERNAL_SAT, parent.getSatisfiable());
	}

	/**
	 * Tests {@link PTElement#evaluateSatisfiableFromChildren()}
	 */
	@Test
	void shouldNotChangeWhenChildrenEmpty() {
		PTElement parent = new PTElement(null, null, null, null);
		try {
			parent.evaluateSatisfiableFromChildren();
		} catch (PTElementLocked e) {
			fail(ELEMENTLOCKED);
		} catch (ConflictingStateException e) {
			fail(e.getMessage());
		}
		assertEquals(ProofState.UNKNOWN, parent.getSatisfiable());
	}

}
