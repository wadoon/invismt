/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.rule.CreationRule;
import invismt.rule.RuleApplyCommand;
import invismt.tree.exception.CommandFailedToExecute;
import org.junit.jupiter.api.Test;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Test {@link ProofObligation}
 *
 * @author Tim Junginger
 */
class ProofObligationTest {

	private static final String COMMANDFAILED = "Command failed to execute when it should not.";
	private static final String NOTLISTENED = "Listener was not notify when it should be.";
	private static boolean listened = false;

	/**
	 * Test {@link ProofObligation#getProofTree()}.
	 */
	@Test
	void shouldAcceptNullScript() {
		ProofObligation obligation = new ProofObligation(null);
		assertEquals(null, obligation.getProofTree().getActivePTElement().getScript());
	}

	/**
	 * Test {@link ProofObligation#getProofTree()}.
	 */
	@Test
	void shouldAcceptScriptWithEmptyExpressions() {
		ProofObligation obligation = new ProofObligation(new Script(List.of()));
		assertEquals(0, obligation.getProofTree().getActivePTElement().getScript().getTopLevelExpressions().size());
	}

	/**
	 * Test {@link ProofObligation#getProofTree()}.
	 */
	@Test
	void shouldCreateCorrectTree() {
		Expression expr = new SyntaxCommand.CheckSat();
		Script script = new Script(List.of(expr));
		ProofObligation obligation = new ProofObligation(script);
		assertSame(script, obligation.getProofTree().getActivePTElement().getScript());
		assertEquals(0, obligation.getProofTree().getActivePTElement().getChildren().size());
	}

	/**
	 * Test {@link ProofObligation#getTreeCommandHistory()}.
	 */
	@Test
	void shouldReturnCorrectHistory() {
		ProofObligation obligation = new ProofObligation(null);
		PTElement element = new PTElement(null, null, null, null);
		Expression expr = new SyntaxCommand.CheckSat();
		Script script = new Script(List.of(expr));
		TreeCommandHistory history = obligation.getTreeCommandHistory();
		assertNotEquals(null, history);
		TreeCommand command = new RuleApplyCommand(new CreationRule(script, expr), element);
		try {
			obligation.executeCommand(command);
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertSame(history.getDone().peek(), command);
	}

	/**
	 * Test {@link ProofObligation#getProofTree()}.
	 */
	@Test
	void shouldNotifyListenersOnExecute() {
		ProofObligation obligation = new ProofObligation(null);
		listened = false;
		PropertyChangeListener listener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				assertSame(obligation, evt.getSource());
				assertSame(obligation, evt.getNewValue());
				listened = true;
			}
		};
		obligation.addChangeListener(listener);
		PTElement element = new PTElement(null, null, null, null);
		Expression expr = new SyntaxCommand.CheckSat();
		Script script = new Script(List.of(expr));
		TreeCommand command = new RuleApplyCommand(new CreationRule(script, expr), element);
		try {
			obligation.executeCommand(command);
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		if (!listened) {
			fail(NOTLISTENED);
		}
	}

	/**
	 * Test {@link ProofObligation#addChangeListener(PropertyChangeListener)}.
	 */
	@Test
	void shouldNotifyListenersOnUndo() {
		ProofObligation obligation = new ProofObligation(null);
		listened = false;
		PropertyChangeListener listener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				assertSame(obligation, evt.getSource());
				assertSame(obligation, evt.getNewValue());
				listened = true;
			}
		};
		obligation.addChangeListener(listener);
		try {
			obligation.undo();
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		if (!listened) {
			fail(NOTLISTENED);
		}
	}

	/**
	 * Test {@link ProofObligation#addChangeListener(PropertyChangeListener)}.
	 */
	@Test
	void shouldNotifyListenersOnRedo() {
		ProofObligation obligation = new ProofObligation(null);
		listened = false;
		PropertyChangeListener listener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				assertSame(obligation, evt.getSource());
				assertSame(obligation, evt.getNewValue());
				listened = true;
			}
		};
		obligation.addChangeListener(listener);
		try {
			obligation.redo();
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		if (!listened) {
			fail(NOTLISTENED);
		}
	}

	/**
	 * Tests {@link ProofObligation#rmChangeListener(PropertyChangeListener)}
	 */
	@Test
	void shouldRemoveListeners() {
		ProofObligation obligation = new ProofObligation(null);
		listened = false;
		PropertyChangeListener listener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				assertSame(obligation, evt.getSource());
				assertSame(obligation, evt.getNewValue());
				listened = true;
			}
		};
		obligation.addChangeListener(listener);
		try {
			obligation.undo();
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertEquals(true, listened);
		obligation.rmChangeListener(listener);
		listened = false;
		try {
			obligation.undo();
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertEquals(false, listened);
	}

}
