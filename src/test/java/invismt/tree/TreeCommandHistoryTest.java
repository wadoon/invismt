/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;

import invismt.tree.exception.CommandFailedToExecute;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

/**
 * Test {@link TreeCommandHistory}
 *
 * @author Tim Junginger
 */
class TreeCommandHistoryTest {

	private static final String COMMANDFAILED = "Command failed to execute although it should not.";

	private static boolean executed = false;
	private static boolean undone = false;
	private static boolean redone = false;

	/**
	 * Test {@link TreeCommandHistory#addCommand(TreeCommand)}.
	 */
	@Test
	void shouldAddCommandCorrectly() {
		TreeCommandHistory history = new TreeCommandHistory();
		TreeCommand command = new TestCommand();
		try {
			history.addCommand(command);
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertEquals(1, history.getDone().size());
		assertSame(command, history.getDone().peek());
	}

	/**
	 * Test {@link TreeCommandHistory#addCommand(TreeCommand)}.
	 */
	@Test
	void shouldExecuteCommand() {
		TreeCommandHistory history = new TreeCommandHistory();
		TreeCommand command = new TestCommand();
		executed = false;
		try {
			history.addCommand(command);
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertEquals(true, executed);
	}

	/**
	 * Test {@link TreeCommandHistory#undo()}.
	 */
	@Test
	void shouldUndoCommandCorrectly() {
		TreeCommandHistory history = new TreeCommandHistory();
		TreeCommand command = new TestCommand();
		try {
			history.addCommand(command);
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		try {
			history.undo();
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertEquals(0, history.getDone().size());
		assertEquals(1, history.getUndo().size());
		assertSame(command, history.getUndo().peek());
	}

	/**
	 * Test {@link TreeCommandHistory#undo()}.
	 */
	@Test
	void shouldUndoCommand() {
		TreeCommandHistory history = new TreeCommandHistory();
		TreeCommand command = new TestCommand();
		try {
			history.addCommand(command);
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		undone = false;
		try {
			history.undo();
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertEquals(true, undone);
	}

	/**
	 * Test {@link TreeCommandHistory#redo()}.
	 */
	@Test
	void shouldRedoCommandCorrectly() {
		TreeCommandHistory history = new TreeCommandHistory();
		TreeCommand command = new TestCommand();
		try {
			history.addCommand(command);
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		try {
			history.undo();
		} catch (CommandFailedToExecute e1) {
			fail(COMMANDFAILED);
		}
		try {
			history.redo();
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertEquals(0, history.getUndo().size());
		assertEquals(1, history.getDone().size());
		assertSame(command, history.getDone().peek());
	}

	/**
	 * Test {@link TreeCommandHistory#undo()}.
	 */
	@Test
	void shouldRedoCommand() {
		TreeCommandHistory history = new TreeCommandHistory();
		TreeCommand command = new TestCommand();
		try {
			history.addCommand(command);
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		try {
			history.undo();
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		redone = false;
		try {
			history.redo();
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertEquals(true, redone);
	}

	/**
	 * Tests {@link TreeCommandHistory#undo()}
	 *
	 * @throws CommandFailedToExecute
	 */
	@Test
	void shouldNotUndoCommandWhenFailed() throws CommandFailedToExecute {
		TreeCommand failingCommand = mock(TreeCommand.class);
		doThrow(new CommandFailedToExecute()).when(failingCommand).undo();
		TreeCommandHistory history = new TreeCommandHistory();
		try {
			history.addCommand(failingCommand);
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertEquals(0, history.getUndo().size());
		assertEquals(1, history.getDone().size());
		try {
			history.undo();
			fail("Command did not fail, although it was suppossed to.");
		} catch (CommandFailedToExecute e) {
			// should happen
		}
		assertEquals(0, history.getUndo().size());
		assertEquals(1, history.getDone().size());
	}

	/**
	 * Tests {@link TreeCommandHistory#undo()}
	 *
	 * @throws CommandFailedToExecute
	 */
	@Test
	void shouldNotRedoCommandWhenFailed() throws CommandFailedToExecute {
		TreeCommand failingCommand = mock(TreeCommand.class);
		doThrow(new CommandFailedToExecute()).when(failingCommand).redo();
		TreeCommandHistory history = new TreeCommandHistory();
		try {
			history.addCommand(failingCommand);
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		try {
			history.undo();
		} catch (CommandFailedToExecute e) {
			fail(COMMANDFAILED);
		}
		assertEquals(1, history.getUndo().size());
		assertEquals(0, history.getDone().size());
		try {
			history.redo();
			fail("Command did not fail, although it was suppossed to.");
		} catch (CommandFailedToExecute e) {
			// should happen
		}
		assertEquals(1, history.getUndo().size());
		assertEquals(0, history.getDone().size());
	}

	private static class TestCommand implements TreeCommand {
		@Override
		public void execute() throws CommandFailedToExecute {
			executed = true;
		}

		@Override
		public void redo() throws CommandFailedToExecute {
			redone = true;
		}

		@Override
		public void undo() throws CommandFailedToExecute {
			undone = true;
		}
	}

}
