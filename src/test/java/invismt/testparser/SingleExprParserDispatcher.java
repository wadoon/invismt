/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.testparser;

import invismt.grammar.Expression;
import invismt.parser.ParserDispatcher;
import invismt.parser.SMTLIBDescriptiveBailErrorStrategy;
import invismt.parser.SMTLIBExpressionProductionVisitor;
import invismt.parser.antlr.SMTLIBv2Lexer;
import invismt.parser.antlr.SMTLIBv2Parser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;

/**
 * Parser for single {@link Expression expressions} that aren't inside a
 * {@link invismt.grammar.Script}.
 * Exemplary use @see {@link invismt.testutil.FileToScriptUtil#getParameters(Path)}.
 */
public class SingleExprParserDispatcher implements ParserDispatcher<Expression> {

	private Method ctxGetter;
	private SMTLIBv2Parser toDispatch;

	public SingleExprParserDispatcher(String contextName) {
		toDispatch = new SMTLIBv2Parser(null);
		toDispatch.setErrorHandler(new SMTLIBDescriptiveBailErrorStrategy());
		try {
			ctxGetter = toDispatch.getClass().getMethod(contextName);
		} catch (NoSuchMethodException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	public Expression parseInto(String content) {
		ANTLRInputStream contentStream = new ANTLRInputStream(content);
		SMTLIBv2Lexer lexer = new SMTLIBv2Lexer(contentStream);
		CommonTokenStream contentTokens = new CommonTokenStream(lexer);
		toDispatch.setTokenStream(contentTokens);
		ParserRuleContext ctx;
		try {
			ctx = (ParserRuleContext) ctxGetter.invoke(toDispatch);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException(e.getMessage());
		} catch (InvocationTargetException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
		return ctx.getChild(0).accept(new SMTLIBExpressionProductionVisitor());
	}

}
