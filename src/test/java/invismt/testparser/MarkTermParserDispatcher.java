/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.testparser;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.term.Term;
import invismt.parser.ParserDispatcher;
import invismt.parser.SMTLIBDescriptiveBailErrorStrategy;
import invismt.parser.antlr.SMTLIBMarkTermLexer;
import invismt.parser.antlr.SMTLIBMarkTermParser;
import invismt.parser.antlr.SMTLIBMarkTermParser.ScriptContext;
import invismt.util.Tuple;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Parser for marked terms inside of a {@link Script}.
 * Exemplary use @see {@link invismt.testutil.FileToScriptUtil#getMarkedExpressionsScript(Path)}.
 */
public class MarkTermParserDispatcher implements ParserDispatcher<Tuple<Script, List<Term>>> {

	private SMTLIBMarkTermParser toDispatch;

	public MarkTermParserDispatcher() {
		this.toDispatch = new SMTLIBMarkTermParser(null);
		toDispatch.setErrorHandler(new SMTLIBDescriptiveBailErrorStrategy());
		toDispatch.removeErrorListeners();
	}

	@Override
	public Tuple<Script, List<Term>> parseInto(String content) {
		ANTLRInputStream contentStream = new ANTLRInputStream(content);
		SMTLIBMarkTermLexer lexer = new SMTLIBMarkTermLexer(contentStream);
		CommonTokenStream contentTokens = new CommonTokenStream(lexer);
		toDispatch.setTokenStream(contentTokens);
		List<Expression> parsedCommands = new ArrayList<>();
		ScriptContext root = toDispatch.script();
		// Feed logger entire parse tree before converting to AST
		SMTLIBMarkTermProductionVisitor visitor = new SMTLIBMarkTermProductionVisitor();
		for (int i = 0; i < root.getChildCount() - 1; i++) {
			parsedCommands.add(root.getChild(i).accept(visitor));
			// script -> command* EOF is deterministic
		}
		// Return script and marked expressions
		return new Tuple<Script, List<Term>>(new Script(parsedCommands), visitor.getMarked());
	}

}
