/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.testparser;

import invismt.grammar.Expression;
import invismt.grammar.GeneralExpression;
import invismt.grammar.SpecConstant;
import invismt.grammar.Token;
import invismt.grammar.Token.BinaryToken;
import invismt.grammar.Token.DecimalToken;
import invismt.grammar.Token.HexadecimalToken;
import invismt.grammar.Token.Keyword;
import invismt.grammar.Token.NumeralToken;
import invismt.grammar.Token.StringToken;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.AnnotationTerm;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.LetTerm;
import invismt.grammar.term.MatchCase;
import invismt.grammar.term.MatchTerm;
import invismt.grammar.term.Pattern;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.Term;
import invismt.grammar.term.VariableBinding;
import invismt.parser.antlr.SMTLIBMarkTermBaseVisitor;
import invismt.parser.antlr.SMTLIBMarkTermParser.AttributeContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Attribute_valueContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.BinaryContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_assertContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_checkSatContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_declareConstContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_declareDatatypeContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_declareDatatypesContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_declareFunContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_declareSortContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_defineFunContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_defineFunRecContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_defineFunsRecContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_defineSortContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_echoContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_exitContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_popContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_pushContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_resetContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_setInfoContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_setLogicContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Cmd_setOptionContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.CommandContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.DecimalContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Exclamation_termContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Exists_termContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Forall_termContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.GeneralReservedWordContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.HexadecimalContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.IdentifierContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.IndexContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.KeywordContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Let_termContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Match_caseContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Match_termContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.NumeralContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Parameter_termContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.PatternContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.PredefSymbolContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Qual_identifierContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Qual_termContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.QuotedSymbolContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.S_exprContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.ScriptContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.SimpleSymbolContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Single_termContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.SortContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Sorted_varContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Spec_constantContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.StringContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.SymbolContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.TermContext;
import invismt.parser.antlr.SMTLIBMarkTermParser.Var_bindingContext;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Parser visitor for a {@link invismt.grammar.Script} that contains marked terms according to
 * {@link MarkTermParserDispatcher}.
 * Should always do exactly the same as the real parser {@link invismt.parser.ScriptParserDispatcher}
 * as long as the latter works correctly, except for when a marked term is to be parsed.
 */
public class SMTLIBMarkTermProductionVisitor extends SMTLIBMarkTermBaseVisitor<Expression> {

	private static final String PAROPEN = "(";
	private static final String PARCLOSED = ")";
	private final List<Term> params;

	public SMTLIBMarkTermProductionVisitor() {
		this.params = new ArrayList<>();
	}

	public List<Term> getMarked() {
		return new ArrayList<>(params);
	}

	private GeneralExpression convertToGeneralExpression(ParserRuleContext ctx, boolean isGlobal, boolean brackets) {
		StringBuilder str = new StringBuilder();
		String spaces = ctx.start.getInputStream()
				.getText(new Interval(ctx.start.getStartIndex(), ctx.stop.getStopIndex()));
		str.append(spaces);
		if (brackets) {
			str.insert(0, PAROPEN);
			str.append(PARCLOSED);
		}
		return new GeneralExpression(str.toString(), isGlobal);
	}

	// TODO Implement visiting of bind nodes
	@Override
	public Expression visitTerm(TermContext ctx) {
		// TODO Replace switch expression with something more OOP-friendly
		if (ctx.getChildCount() == 1) {
			Expression singleValue = ctx.getChild(0).accept(this);
			if (singleValue instanceof SpecConstant) {
				return new ConstantTerm((SpecConstant) singleValue);
			} else if (singleValue instanceof QualIdentifier) {
				return new IdentifierTerm((QualIdentifier) singleValue, List.of());
			} else if (singleValue instanceof Term) {
				return (Term) singleValue;
			}
		} else {
			// Deterministic rule: term -> ParOpen *_term ParClose
			return ctx.getChild(1).accept(this);
		}
		// Should have zero coverage
		return null;
	}

	@Override
	public Expression visitParameter_term(Parameter_termContext ctx) {
		Expression term = ctx.term().accept(this);
		params.add((Term) term);
		return term;
	}

	@Override
	public Expression visitGeneralReservedWord(GeneralReservedWordContext ctx) {
		// generalReservedWord -> GRW_* is deterministic
		return ctx.getChild(0).accept(this);
	}

	@Override
	public Expression visitSimpleSymbol(SimpleSymbolContext ctx) {
		return new Token.Keyword(ctx.getText());
	}

	@Override
	public Expression visitQuotedSymbol(QuotedSymbolContext ctx) {
		return new Token.SymbolToken(ctx.getText());
		// quotedSymbol directly matches to SymbolToken
	}

	@Override
	public Expression visitPredefSymbol(PredefSymbolContext ctx) {
		// Dead code? predefSymbol can only be derived using simpleSymbol, but
		// visitSymbol(...) returns a token without calling the visitor again
		return new SymbolToken(ctx.getText());
	}

	@Override
	public Expression visitSymbol(SymbolContext ctx) {
		return new SymbolToken(ctx.getText());
	}

	@Override
	public Expression visitNumeral(NumeralContext ctx) {
		return new Token.NumeralToken(ctx.getText());
		// numeral directly matches to NumeralToken
	}

	@Override
	public Expression visitDecimal(DecimalContext ctx) {
		return new Token.DecimalToken(ctx.getChild(0).getText());
		// decimal directly matches to DecimalToken
	}

	@Override
	public Expression visitHexadecimal(HexadecimalContext ctx) {
		return new Token.HexadecimalToken(ctx.getChild(0).getText());
		// hexadecimal directly matches to HexademicalToken
	}

	@Override
	public Expression visitBinary(BinaryContext ctx) {
		return new Token.BinaryToken(ctx.getChild(0).getText());
		// binary directly matches to BinaryToken
	}

	@Override
	public Expression visitString(StringContext ctx) {
		return new Token.StringToken(ctx.getChild(0).getText());
		// string directly matches to StringToken
	}

	@Override
	public Expression visitKeyword(KeywordContext ctx) {
		return new Token.Keyword(":" + ctx.getChild(1).getText());
		// keyword is equivalent to simpleSymbol, and the Keyboard class is used to
		// represent both
	}

	@Override
	public Expression visitSpec_constant(Spec_constantContext ctx) {
		// TODO Replace switch expression with some arbitary Visitor structure
		if (ctx.getChildCount() == 1) {
			Expression singleValue = ctx.getChild(0).accept(this);
			if (singleValue instanceof NumeralToken) {
				return new SpecConstant.NumeralConstant((NumeralToken) singleValue);
			} else if (singleValue instanceof StringToken) {
				return new SpecConstant.StringConstant((StringToken) singleValue);
			} else if (singleValue instanceof HexadecimalToken) {
				return new SpecConstant.HexadecimalConstant((HexadecimalToken) singleValue);
			} else if (singleValue instanceof BinaryToken) {
				return new SpecConstant.BinaryConstant((BinaryToken) singleValue);
			} else if (singleValue instanceof DecimalToken) {
				return new SpecConstant.DecimalConstant((DecimalToken) singleValue);
			}
		}
		return null;
		// TODO Decide what to do if parser wonks
	}

	@Override
	public Expression visitS_expr(S_exprContext ctx) {
		return convertToGeneralExpression(ctx, false, ctx.getChildCount() > 1);
	}

	@Override
	public Expression visitIndex(IndexContext ctx) {
		// TODO Replace switch expression with some arbitrary Visitor structure
		if (ctx.getChildCount() == 1) {
			Expression singleValue = ctx.getChild(0).accept(this);
			if (singleValue instanceof NumeralToken) {
				return new Index.NumeralIndex((NumeralToken) singleValue);
			} else if (singleValue instanceof SymbolToken) {
				return new Index.SymbolIndex((SymbolToken) singleValue);
			}
		}
		return null;
		// Deterministic rule, this line should have zero coverage
	}

	@Override
	public Expression visitIdentifier(IdentifierContext ctx) {
		if (ctx.getChildCount() == 1) {
			return new Identifier((SymbolToken) ctx.symbol().accept(this), List.of());
		} else {
			// Collect further sorts in a list and call this visitor recursively on
			// individual sort nodes
			List<Index> index = new ArrayList<>();
			for (IndexContext e : ctx.index()) {
				index.add((Index) e.accept(this));
			}
			return new Identifier((SymbolToken) ctx.symbol().accept(this), index);
		}
		// TODO Implement error handling
	}

	@Override
	public Expression visitAttribute_value(Attribute_valueContext ctx) {
		if (ctx.getChildCount() == 1) {
			Expression singleValue = ctx.getChild(0).accept(this);
			if (singleValue instanceof SpecConstant) {
				return new AttributeValue.ConstantAttributeValue((SpecConstant) singleValue);
			} else if (singleValue instanceof SymbolToken) {
				return new AttributeValue.SymbolAttributeValue((SymbolToken) singleValue);
			}
			return null;
		} else {
			// Deterministic rule: attribute_value -> ParOpen s_expr* ParClose
			List<Expression> sexps = new ArrayList<>();
			for (int i = 1; i < ctx.getChildCount() - 1; i++) {
				sexps.add(ctx.getChild(i).accept(this));
			}
			return new AttributeValue.ListAttributeValue(sexps);
		}
	}

	@Override
	public Expression visitAttribute(AttributeContext ctx) {
		if (ctx.getChildCount() == 1) {
			return new Attribute((Keyword) ctx.getChild(0).accept(this), Optional.empty());
		} else {
			return new Attribute((Keyword) ctx.getChild(0).accept(this),
					Optional.of((AttributeValue) ctx.getChild(1).accept(this)));
		}
	}

	// TODO Decide how the parser should handle unsupported expressions
	@Override
	public Expression visitSort(SortContext ctx) {
		if (ctx.getChildCount() == 1) {
			return new Sort((Identifier) ctx.getChild(0).accept(this), List.of());
		} else {
			// Call recursively and collect
			List<Sort> sorts = new ArrayList<>();
			for (int i = 2; i < ctx.getChildCount() - 1; i++) {
				sorts.add((Sort) ctx.getChild(i).accept(this));
			}
			return new Sort((Identifier) ctx.getChild(1).accept(this), sorts);
		}
	}

	@Override
	public Expression visitQual_identifier(Qual_identifierContext ctx) {
		// Positions chosen according to grammar
		if (ctx.getChildCount() == 1) {
			return new QualIdentifier((Identifier) ctx.getChild(0).accept(this), Optional.empty());
		} else {
			return new QualIdentifier((Identifier) ctx.getChild(1).accept(this),
					Optional.of((Sort) ctx.getChild(2).accept(this)));
		}
	}

	@Override
	public Expression visitVar_binding(Var_bindingContext ctx) {
		return new VariableBinding((SymbolToken) ctx.getChild(1).accept(this), (Term) ctx.getChild(2).accept(this));
	}

	@Override
	public Expression visitSorted_var(Sorted_varContext ctx) {
		return new SortedVariable((SymbolToken) ctx.getChild(1).accept(this), (Sort) ctx.getChild(2).accept(this));
	}

	@Override
	public Expression visitPattern(PatternContext ctx) {
		List<SymbolToken> symbols = new ArrayList<>();
		if (ctx.getChildCount() == 1) {
			return new Pattern((SymbolToken) ctx.getChild(0).accept(this), symbols);
		} else {
			for (int i = 2; i < ctx.getChildCount() - 1; i++) {
				symbols.add((SymbolToken) ctx.getChild(i).accept(this));
			}
			return new Pattern((SymbolToken) ctx.getChild(1).accept(this), symbols);
		}
	}

	@Override
	public Expression visitMatch_case(Match_caseContext ctx) {
		return new MatchCase((Pattern) ctx.getChild(1).accept(this), (Term) ctx.getChild(2).accept(this));
	}

	@Override
	public Expression visitSingle_term(Single_termContext ctx) {
		return ctx.getChild(0).accept(this);
	}

	@Override
	public Expression visitQual_term(Qual_termContext ctx) {
		// Call recursively and collect
		List<Term> terms = new ArrayList<>();
		for (int i = 1; i < ctx.getChildCount(); i++) {
			terms.add((Term) ctx.getChild(i).accept(this));
		}
		return new IdentifierTerm((QualIdentifier) ctx.getChild(0).accept(this), terms);
	}

	@Override
	public Expression visitLet_term(Let_termContext ctx) {
		List<VariableBinding> binds = new ArrayList<>();
		for (int i = 2; i < ctx.getChildCount() - 2; i++) {
			binds.add((VariableBinding) ctx.getChild(i).accept(this));
		}
		return new LetTerm(binds, (Term) ctx.getChild(ctx.getChildCount() - 1).accept(this));
	}

	@Override
	public Expression visitForall_term(Forall_termContext ctx) {
		List<SortedVariable> svars = new ArrayList<>();
		for (int i = 2; i < ctx.getChildCount() - 2; i++) {
			svars.add((SortedVariable) ctx.getChild(i).accept(this));
		}
		return new QuantifiedTerm.ForallTerm(svars, (Term) ctx.getChild(ctx.getChildCount() - 1).accept(this));
	}

	@Override
	public Expression visitExists_term(Exists_termContext ctx) {
		List<SortedVariable> svars = new ArrayList<>();
		for (int i = 2; i < ctx.getChildCount() - 2; i++) {
			svars.add((SortedVariable) ctx.getChild(i).accept(this));
		}
		return new QuantifiedTerm.ExistsTerm(svars, (Term) ctx.getChild(ctx.getChildCount() - 1).accept(this));
	}

	@Override
	public Expression visitMatch_term(Match_termContext ctx) {
		List<MatchCase> matches = new ArrayList<>();
		for (int i = 3; i < ctx.getChildCount() - 1; i++) {
			matches.add((MatchCase) ctx.getChild(i).accept(this));
		}
		return new MatchTerm((Term) ctx.getChild(1).accept(this), matches);
	}

	@Override
	public Expression visitExclamation_term(Exclamation_termContext ctx) {
		List<Attribute> attributes = new ArrayList<>();
		for (int i = 2; i < ctx.getChildCount(); i++) {
			attributes.add((Attribute) ctx.getChild(i).accept(this));
		}
		return new AnnotationTerm((Term) ctx.getChild(1).accept(this), attributes);
	}

	@Override
	public Expression visitScript(ScriptContext ctx) {
		throw new RuntimeException();
		// TODO Ensure ANTLRExpressionProductionVisitor never visits a ScriptContext
	}

	@Override
	public Expression visitCmd_declareConst(Cmd_declareConstContext ctx) {
		return new SyntaxCommand.DeclareConstant((SymbolToken) ctx.getChild(1).accept(this),
				(Sort) ctx.getChild(2).accept(this));
	}

	@Override
	public Expression visitCmd_declareDatatype(Cmd_declareDatatypeContext ctx) {
		return convertToGeneralExpression(ctx, false, true);
	}

	@Override
	public Expression visitCmd_declareDatatypes(Cmd_declareDatatypesContext ctx) {
		return convertToGeneralExpression(ctx, false, true);
	}

	@Override
	public Expression visitCmd_declareSort(Cmd_declareSortContext ctx) {
		return convertToGeneralExpression(ctx, false, true);
	}

	@Override
	public Expression visitCmd_defineFun(Cmd_defineFunContext ctx) {
		return convertToGeneralExpression(ctx, false, true);
	}

	@Override
	public Expression visitCmd_defineFunRec(Cmd_defineFunRecContext ctx) {
		return convertToGeneralExpression(ctx, false, true);
	}

	@Override
	public Expression visitCmd_defineFunsRec(Cmd_defineFunsRecContext ctx) {
		return convertToGeneralExpression(ctx, false, true);
	}

	@Override
	public Expression visitCmd_defineSort(Cmd_defineSortContext ctx) {
		return convertToGeneralExpression(ctx, false, true);
	}

	// TODO Implement visiting of individual command types
	@Override
	public Expression visitCmd_assert(Cmd_assertContext ctx) {
		return new SyntaxCommand.Assert((Term) ctx.getChild(1).accept(this));
	}

	@Override
	public Expression visitCmd_checkSat(Cmd_checkSatContext ctx) {
		return new SyntaxCommand.CheckSat();
	}

	@Override
	public Expression visitCmd_declareFun(Cmd_declareFunContext ctx) {
		List<Sort> definitionArea = new ArrayList<>();
		for (SortContext sortCtx : ctx.sort()) {
			definitionArea.add((Sort) sortCtx.accept(this));
		}
		Sort targetArea = definitionArea.get(definitionArea.size() - 1);
		definitionArea.remove(definitionArea.size() - 1);
		return new SyntaxCommand.DeclareFunction((SymbolToken) ctx.symbol().accept(this), definitionArea, targetArea);
	}

	@Override
	public Expression visitCmd_echo(Cmd_echoContext ctx) {
		return convertToGeneralExpression(ctx, true, true);
	}

	@Override
	public Expression visitCmd_exit(Cmd_exitContext ctx) {
		return new SyntaxCommand.Exit();
	}

	@Override
	public Expression visitCmd_pop(Cmd_popContext ctx) {
		return new SyntaxCommand.Pop((NumeralToken) ctx.getChild(1).accept(this));
	}

	@Override
	public Expression visitCmd_push(Cmd_pushContext ctx) {
		return new SyntaxCommand.Push((NumeralToken) ctx.getChild(1).accept(this));
	}

	@Override
	public Expression visitCmd_reset(Cmd_resetContext ctx) {
		return new SyntaxCommand.Reset();
	}

	@Override
	public Expression visitCmd_setLogic(Cmd_setLogicContext ctx) {
		return convertToGeneralExpression(ctx, true, true);
	}

	@Override
	public Expression visitCmd_setInfo(Cmd_setInfoContext ctx) {
		return convertToGeneralExpression(ctx, true, true);
	}

	@Override
	public Expression visitCmd_setOption(Cmd_setOptionContext ctx) {
		return convertToGeneralExpression(ctx, true, true);
	}

	@Override
	public Expression visitCommand(CommandContext ctx) {
		// Position 1 always contains command node: ParOpen cmd_* ParClose
		return ctx.getChild(1).accept(this);
	}

}
