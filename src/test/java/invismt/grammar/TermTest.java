/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General License for more details.
 *
 * You should have received a copy of the GNU General License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.LetTerm;
import invismt.grammar.term.MatchCase;
import invismt.grammar.term.MatchTerm;
import invismt.grammar.term.Pattern;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.Term;
import invismt.grammar.term.VariableBinding;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class TermTest {

	@Nested
	class VariableGetterTests {

		private static Stream<Arguments> boundVariables() {
			Map<Term, Set<Token.SymbolToken>> boundVars = new HashMap<>();
			boundVars.put(new ConstantTerm(new SpecConstant.NumeralConstant(new Token.NumeralToken("42"))),
					new HashSet());
			boundVars.put(new IdentifierTerm(
					new QualIdentifier(
							ExpressionFactory.makeIdentifier("x", new ArrayList<>()),
							Optional.empty()),
					new ArrayList<>()), new HashSet());
			boundVars.put(
					new QuantifiedTerm.ForallTerm(
							List.of(new SortedVariable(new Token.SymbolToken("x"), ExpressionFactory.INT)),
							new IdentifierTerm(
									new QualIdentifier(
											ExpressionFactory.makeIdentifier("y", new ArrayList<>()),
											Optional.empty()),
									new ArrayList<>())),
					new HashSet(List.of(new Token.SymbolToken("x"))));
			boundVars.put(
					new QuantifiedTerm.ExistsTerm(
							List.of(new SortedVariable(new Token.SymbolToken("x"), ExpressionFactory.INT)),
							new IdentifierTerm(
									new QualIdentifier(
											ExpressionFactory.makeIdentifier("y", new ArrayList<>()),
											Optional.empty()),
									new ArrayList<>())),
					new HashSet(List.of(new Token.SymbolToken("x"))));
			boundVars.put(
					new LetTerm(
							List.of(new VariableBinding(new Token.SymbolToken("x"), ExpressionFactory.varTerm("x"))),
							new IdentifierTerm(
									new QualIdentifier(
											ExpressionFactory.makeIdentifier("y", new ArrayList<>()),
											Optional.empty()),
									new ArrayList<>())),
					new HashSet(List.of(new Token.SymbolToken("x"))));
			boundVars.put(new IdentifierTerm(
					new QualIdentifier(
							ExpressionFactory.makeIdentifier("+", new ArrayList<>()),
							Optional.empty()),
					List.of(
							new IdentifierTerm(
									new QualIdentifier(
											ExpressionFactory.makeIdentifier("a", new ArrayList<>()),
											Optional.empty()),
									new ArrayList<>()),
							new QuantifiedTerm.ForallTerm(
									List.of(new SortedVariable(new Token.SymbolToken("b"), ExpressionFactory.INT)),
									new IdentifierTerm(
											new QualIdentifier(
													ExpressionFactory.makeIdentifier("b", new ArrayList<>()),
													Optional.empty()),
											new ArrayList<>()))
					)), new HashSet());
			boundVars.put(new MatchTerm(
							ExpressionFactory.varTerm("x"),
							List.of(new MatchCase(
											new Pattern(new Token.SymbolToken("x"), new ArrayList<>()),
											ExpressionFactory.varTerm("x")),
									new MatchCase(
											new Pattern(new Token.SymbolToken("x"), new ArrayList<>()),
											ExpressionFactory.varTerm("y")))
					),
					new HashSet());
			return Stream.of(arguments(boundVars));
		}

		private static Stream<Arguments> freeVariables() {
			Map<Term, Set<Token.SymbolToken>> freeVars = new HashMap<>();
			freeVars.put(new ConstantTerm(new SpecConstant.NumeralConstant(new Token.NumeralToken("42"))),
					new HashSet());
			freeVars.put(new MatchTerm(
							ExpressionFactory.varTerm("x"),
							List.of(new MatchCase(
											new Pattern(new Token.SymbolToken("x"), new ArrayList<>()),
											ExpressionFactory.varTerm("x")),
									new MatchCase(
											new Pattern(new Token.SymbolToken("x"), new ArrayList<>()),
											ExpressionFactory.varTerm("y")))
							),
					new HashSet(List.of(new Token.SymbolToken("x"), new Token.SymbolToken("y"))));
			freeVars.put(new MatchTerm(
							new QuantifiedTerm.ForallTerm(
									List.of(new SortedVariable(new Token.SymbolToken("x"), ExpressionFactory.INT)),
									ExpressionFactory.varTerm("x")),
							List.of(new MatchCase(
											new Pattern(new Token.SymbolToken("x"), new ArrayList<>()),
											ExpressionFactory.varTerm("x")))
					),
					new HashSet());
			freeVars.put(new IdentifierTerm(
					new QualIdentifier(
							ExpressionFactory.makeIdentifier("x", new ArrayList<>()),
							Optional.empty()),
					new ArrayList<>()), new HashSet(List.of(new Token.SymbolToken("x"))));
			freeVars.put(new QuantifiedTerm.ForallTerm(
					List.of(new SortedVariable(new Token.SymbolToken("x"), ExpressionFactory.INT)),
					ExpressionFactory.varTerm("x")),
					new HashSet());
			freeVars.put(new QuantifiedTerm.ExistsTerm(
							List.of(new SortedVariable(new Token.SymbolToken("x"), ExpressionFactory.INT)),
							ExpressionFactory.varTerm("x")),
					new HashSet());
			freeVars.put(new LetTerm(
							List.of(new VariableBinding(new Token.SymbolToken("x"), ExpressionFactory.varTerm("y"))),
							ExpressionFactory.varTerm("x")),
					new HashSet(List.of(new Token.SymbolToken("y"))));
			freeVars.put(new IdentifierTerm(
					new QualIdentifier(
							ExpressionFactory.makeIdentifier("+", new ArrayList<>()),
							Optional.empty()),
					List.of(
							new IdentifierTerm(
							new QualIdentifier(
									ExpressionFactory.makeIdentifier("a", new ArrayList<>()),
									Optional.empty()),
							new ArrayList<>()),
							new QuantifiedTerm.ForallTerm(
									List.of(new SortedVariable(new Token.SymbolToken("b"), ExpressionFactory.INT)),
									new IdentifierTerm(
											new QualIdentifier(
													ExpressionFactory.makeIdentifier("b", new ArrayList<>()),
													Optional.empty()),
											new ArrayList<>()))
					)), new HashSet(List.of(new Token.SymbolToken("a"))));
			return Stream.of(arguments(freeVars));
		}

		@ParameterizedTest
		@MethodSource("boundVariables")
		void shouldReturnExpectedBoundVariables(HashMap<Term, Set<Token.SymbolToken>> boundVars) {
			for (Map.Entry<Term, Set<Token.SymbolToken>> entry: boundVars.entrySet()) {
				assertEquals(entry.getValue(), new HashSet(entry.getKey().getBoundVariables()));
			}
		}

		@ParameterizedTest
		@MethodSource("freeVariables")
		void shouldReturnExpectedFreeVariables(HashMap<Term, Set<Token.SymbolToken>> freeVars) {
			for (Map.Entry<Term, Set<Token.SymbolToken>> entry: freeVars.entrySet()) {
				assertEquals(entry.getValue(), new HashSet(entry.getKey().getFreeVariables()));
			}
		}

	}

}
