/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

class TokenTest {

	@Test
	void shouldCreateNumeralTokenWhenPatternsMatch() {
		try {
			new Token.NumeralToken("4");
		} catch (IllegalArgumentException e) {
			fail("Tokens should have been created.");
		}
	}

	@Test
	void shouldCreateStringTokenWhenPatternsMatch() {
		try {
			new Token.StringToken("\"string with \\n newline\"");
		} catch (IllegalArgumentException e) {
			fail("Tokens should have been created.");
		}
	}

	@Test
	void shouldCreateBinaryTokenWhenPatternsMatch() {
		try {
			new Token.BinaryToken("#b01010101101");
			new Token.BinaryToken("#b00");
		} catch (IllegalArgumentException e) {
			fail("Tokens should have been created.");
		}
	}

	@Test
	void shouldCreateDecimalTokenWhenPatternsMatch() {
		try {
			new Token.DecimalToken("4.00");
			new Token.DecimalToken("4.1");
			new Token.DecimalToken("4.0");
			new Token.DecimalToken("472.0530");
		} catch (IllegalArgumentException e) {
			fail("Tokens should have been created.");
		}
	}

	@Test
	void shouldCreateHexadecimalTokenWhenPatternsMatch() {
		try {
			new Token.HexadecimalToken("#xCd679");
		} catch (IllegalArgumentException e) {
			fail("Tokens should have been created.");
		}
	}

	@Test
	void shouldCreateSymbolTokenWhenPatternsMatch() {
		try {
			new Token.SymbolToken("|symbol|");
		} catch (IllegalArgumentException e) {
			fail("Tokens should have been created.");
		}
	}

	@Test
	void shouldCreateKeywordTokenWhenPatternsMatch() {
		try {
			new Token.Keyword(":keyword");
		} catch (IllegalArgumentException e) {
			fail("Tokens should have been created.");
		}
	}

	@Test
	void shouldCreateReservedWordTokenWhenConstructorIsCalled() {
		assertNotNull(new Token.ReservedWord(Token.ReservedWordName.ASSERT));
	}

	// ------------------------------------ Wrong parameters test ------------------------------------ //

	@Test
	void shouldNotCreateStringTokenWhenPatternsDoNotMatch() throws IllegalArgumentException {
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.StringToken("string with missing \"");
		});
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.StringToken("\"string with extra \"\"");
		});
	}

	@Test
	void shouldNotCreateNumeralTokenWhenPatternsDoNotMatch() throws IllegalArgumentException {
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.NumeralToken("4you");
		});
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.NumeralToken("00");
		});
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.NumeralToken("05");
		});
	}

	@Test
	void shouldNotCreateBinaryTokenWhenPatternsDoNotMatch() throws IllegalArgumentException {
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.BinaryToken("01010101101");
		});
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.BinaryToken("04101");
		});
	}

	@Test
	void shouldNotCreateDecimalTokenWhenPatternsDoNotMatch() throws IllegalArgumentException {
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.DecimalToken("4-0");
		});
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.DecimalToken("7");
		});
	}

	@Test
	void shouldNotCreateSymbolTokenWhenPatternsDoNotMatch() throws IllegalArgumentException {
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.SymbolToken("||hui|");
		});
	}

	@Test
	void shouldNotCreateHexadecimalTokenWhenPatternsDoNotMatch() throws IllegalArgumentException {
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.HexadecimalToken("63Ah");
		});
	}

	@Test
	void shouldNotCreateKeywordTokenWhenPatternsDoNotMatch() throws IllegalArgumentException {
		assertThrows(IllegalArgumentException.class, () -> {
			new Token.Keyword("7keyword");
		});
	}

}

