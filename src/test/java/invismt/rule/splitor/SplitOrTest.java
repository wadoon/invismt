/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.splitor;

import invismt.grammar.Script;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests {@link SplitOrRule} and {@link SplitOrWrapper}.
 *
 * @author janik
 */
class SplitOrTest {

	private SplitOrWrapper wrapper;
	private IdentifierTerm a;
	private Term b;
	private QualIdentifier identifier;

	/**
	 * Initializes attributes.
	 */
	@BeforeEach
	void init() {
		wrapper = new SplitOrWrapper();
		a = mock(IdentifierTerm.class);
		b = mock(Term.class);
		identifier = mock(QualIdentifier.class);
	}

	private IdentifierTerm getIdentifierTerm(Term a, Term b) {
		return new IdentifierTerm(
				new QualIdentifier(new Identifier(new SymbolToken("or"), List.of()), Optional.empty()), List.of(a, b));
	}

	/**
	 * Tests
	 */
	@Test
	void shouldNotCreateSplitOrRuleWhenTermIsNotAnAndTerm() {
		SyntaxCommand.Assert assertion = new SyntaxCommand.Assert(a);
		Script script = new Script(List.of(assertion));
		when(a.getIdentifier()).thenReturn(identifier);
		assertFalse(wrapper.mayApply(script, a));
		assertThrows(CannotApplyRuleException.class, () -> {
			new SplitOrRule(script, a, a);
		});
	}

}
