/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.term.Term;
import invismt.rule.cut.CutRule;
import invismt.rule.cut.CutWrapper;
import invismt.rule.demorgan.DeMorganRule;
import invismt.rule.demorgan.DeMorganWrapper;
import invismt.rule.doublenegation.DoubleNegationRule;
import invismt.rule.doublenegation.DoubleNegationWrapper;
import invismt.rule.hide.HideRule;
import invismt.rule.hide.HideWrapper;
import invismt.rule.induction.InductionRule;
import invismt.rule.induction.InductionWrapper;
import invismt.rule.instantiation.InstantiationRule;
import invismt.rule.instantiation.InstantiationWrapper;
import invismt.rule.removeannotations.RemoveAnnotationRule;
import invismt.rule.removeannotations.RemoveAnnotationWrapper;
import invismt.rule.rewrite.RewriteRule;
import invismt.rule.rewrite.RewriteWrapper;
import invismt.rule.skolemization.SkolemizationRule;
import invismt.rule.skolemization.SkolemizationWrapper;
import invismt.rule.splitand.SplitAndRule;
import invismt.rule.splitand.SplitAndWrapper;
import invismt.rule.splitor.SplitOrRule;
import invismt.rule.splitor.SplitOrWrapper;
import invismt.testutil.FileToScriptUtil;
import invismt.testutil.RuleMaker;
import invismt.util.Tuple;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Parameterized tests of the {@link Rule} classes.
 */
class RuleTest {

	private static HideWrapper hideWrapper = new HideWrapper();
	private static CutWrapper cutWrapper = new CutWrapper();
	private static InductionWrapper inductionWrapper = new InductionWrapper();
	private static InstantiationWrapper instantiationWrapper = new InstantiationWrapper();
	private static DeMorganWrapper deMorganWrapper = new DeMorganWrapper();
	private static SkolemizationWrapper skolemWrapper = new SkolemizationWrapper();
	private static DoubleNegationWrapper doubleNegationWrapper = new DoubleNegationWrapper();
	private static SplitAndWrapper splitAndWrapper = new SplitAndWrapper();
	private static SplitOrWrapper splitOrWrapper = new SplitOrWrapper();
	private static RewriteWrapper rewriteWrapper = new RewriteWrapper();
	private static RemoveAnnotationWrapper removeAnnotationWrapper = new RemoveAnnotationWrapper();

	private static Stream<Arguments> successData() {
		return Stream.of(
				arguments("deMorganSuccess1.smt2", "../emptyParams.smt2", List.of("deMorganExpect1.smt2"),
						DeMorganRule.class, deMorganWrapper),
				arguments("deMorganSuccess2.smt2", "../emptyParams.smt2", List.of("deMorganExpect2.smt2"),
						DeMorganRule.class, deMorganWrapper),
				arguments("cutSuccess1.smt2", "cutParams1.smt2", List.of("cutExpect1_1.smt2", "cutExpect1_2.smt2"),
						CutRule.class, cutWrapper),
				arguments("inductionSuccess1.smt2", "inductionParams1.smt2", List.of("inductionExpect1.smt2"),
						InductionRule.class, inductionWrapper),
				arguments("doubleNegationSuccess1.smt2", "../emptyParams.smt2", List.of("doubleNegationExpect1.smt2"),
						DoubleNegationRule.class, doubleNegationWrapper),
				arguments("splitAndSuccess1.smt2", "splitAndParams1.smt2", List.of("splitAndExpect1.smt2"),
						SplitAndRule.class, splitAndWrapper),
				arguments("splitAndSuccess2.smt2", "splitAndParams2.smt2", List.of("splitAndExpect2.smt2"),
						SplitAndRule.class, splitAndWrapper),
				arguments("splitOrSuccess1.smt2", "splitOrParams1.smt2", List.of("splitOrExpect1_1.smt2", "splitOrExpect1_2.smt2"),
						SplitOrRule.class, splitOrWrapper),
				arguments("instantiationSuccess1.smt2", "instantiationParams1.smt2", List.of("instantiationExpect1.smt2"),
						InstantiationRule.class, instantiationWrapper),
				arguments("skolemizationSuccess1.smt2", "skolemizationParams1.smt2", List.of("skolemizationExpect1.smt2"),
						SkolemizationRule.class, skolemWrapper),
				arguments("rewriteSuccess1.smt2", "rewriteParams1.smt2", List.of("rewriteExpect1_1.smt2", "rewriteExpect1_2.smt2"),
						RewriteRule.class, rewriteWrapper),
				arguments("removeAnnotationSuccess1.smt2", "../emptyParams.smt2", List.of("removeAnnotationExpect1.smt2"),
						RemoveAnnotationRule.class, removeAnnotationWrapper),
				arguments("hideSuccess1.smt2", "../emptyParams.smt2", List.of("hideExpect1.smt2"),
						HideRule.class, hideWrapper));
	}

	private static Stream<Arguments> failureData() {
		return Stream.of(
				arguments("inductionFail1.smt2", "inductionFailParams1.smt2",
						"Cannot apply rule Induction because (x Bool) is not of type Int.",
						InductionRule.class, inductionWrapper, false),
				arguments("skolemizationFail1.smt2", "skolemizationFailParams1.smt2",
						"Script already contains skolem_x.",
						SkolemizationRule.class, skolemWrapper, true),
				arguments("doubleNegationFail1.smt2", "../emptyParams.smt2",
						"DoubleNegation rule can only be applied to terms of the form \"not (not ...)\".",
						DoubleNegationRule.class, doubleNegationWrapper, false));
	}

	/**
	 * Parameterized rule test for successful {@link Rule} applications.
	 *
	 * @param filePath     the path to the script to which the rule will be applied,
	 * @param paramPath    the path to the additional parameters of the rule to be applied,
	 * @param comparePaths list of paths to the expected resulting scripts of the applied rule
	 * @param ruleClass    the class of the {@link Rule} to be applied
	 * @param wrapper      the {@link RuleWrapper} corresponding to the rule to be applied
	 * @throws IOException
	 * @see {@link FileToScriptUtil#getMarkedExpressionsScript(Path)} for more
	 * information on the syntax of that file: only the first marked expression is regarded
	 * @see {@link FileToScriptUtil#getParameters(Path)} for more information on the syntax of that file
	 */
	@ParameterizedTest
	@MethodSource("successData")
	void shouldCreateEqualScripts(String filePath, String paramPath, List<String> comparePaths,
								  Class<? extends Rule> ruleClass, RuleWrapper wrapper) throws IOException {
		Path applyTo = FileToScriptUtil.getFile("ruleTestParameterFiles/successFiles/" + filePath);
		Path paramDefs = FileToScriptUtil.getFile("ruleTestParameterFiles/successFiles/" + paramPath);

		List<Script> compareTo = new ArrayList<Script>();
		for (String path : comparePaths) {
			compareTo.add(FileToScriptUtil.toScript(FileToScriptUtil.getFile("ruleTestParameterFiles/successFiles/" + path)));
		}

		Tuple<Script, List<Term>> scriptExpr = FileToScriptUtil.getMarkedExpressionsScript(applyTo);
		Rule rule = RuleMaker.createRule(ruleClass, scriptExpr.getFirst(), scriptExpr.getSecond().get(0),
				FileToScriptUtil.getParameters(paramDefs));
		assertEquals(compareTo.size(), rule.getCreatedScripts().size());
		assertTrue(wrapper.mayApply(rule.script, rule.expression));
		assertTrue(compareTo.containsAll(rule.getCreatedScripts()));
		assertTrue(rule.getCreatedScripts().containsAll(compareTo));
	}

	/**
	 * Parameterized rule test for failing {@link Rule} applications.
	 *
	 * @param filePath  the path to the script to which the rule will be applied,
	 * @param paramPath the path to the additional parameters of the rule to be applied,
	 * @param ruleClass the class of the {@link Rule} to be applied
	 * @param wrapper   the {@link RuleWrapper} corresponding to the rule to be applied
	 * @param mayApply  whether the {@link RuleWrapper#mayApply(Script, Expression)} should return true or false
	 * @throws IOException
	 * @see {@link FileToScriptUtil#getMarkedExpressionsScript(Path)} for more
	 * information on the syntax of that file: only the first marked expression is regarded
	 * @see {@link FileToScriptUtil#getParameters(Path)} for more information on the syntax of that file
	 */
	@ParameterizedTest
	@MethodSource("failureData")
	void shouldNotCreateRuleSuccessfully(String filePath, String paramPath, String expectedExceptionMessage,
										 Class<? extends Rule> ruleClass, RuleWrapper wrapper, boolean mayApply) throws IOException {
		Path applyTo = FileToScriptUtil.getFile("ruleTestParameterFiles/failFiles/" + filePath);
		Path paramDefs = FileToScriptUtil.getFile("ruleTestParameterFiles/failFiles/" + paramPath);

		List<Script> compareTo = new ArrayList<Script>();

		Tuple<Script, List<Term>> scriptExpr = FileToScriptUtil.getMarkedExpressionsScript(applyTo);
		Optional<CannotApplyRuleException> exception = RuleMaker.failRule(ruleClass, scriptExpr.getFirst(), scriptExpr.getSecond().get(0),
				FileToScriptUtil.getParameters(paramDefs));
		assertTrue(exception.isPresent());
		assertEquals(mayApply, wrapper.mayApply(scriptExpr.getFirst(), scriptExpr.getSecond().get(0)));
		assertEquals(expectedExceptionMessage, exception.get().getMessage());
	}

}
