/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.instantiation;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.SpecConstant.NumeralConstant;
import invismt.grammar.Token;
import invismt.grammar.Token.NumeralToken;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm.ForallTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * @author Tim Junginger
 */
class InstantiationRuleTest {

	private Script scriptWithTerm;
	private Token.SymbolToken induceOnVariableName;
	private QualIdentifier variable;
	private Sort sort;
	private Sort otherSort;
	private Term innerTerm;
	private ForallTerm forallTermIntVariable;
	private List<SortedVariable> variables;
	private Term instance;

	@BeforeEach
	void setUp() {
		induceOnVariableName = new Token.SymbolToken("x");
		sort = new Sort(new Identifier(new Token.SymbolToken("Type"), new ArrayList<>()), new ArrayList<>());
		otherSort = new Sort(new Identifier(new Token.SymbolToken("otherType"), new ArrayList<>()), new ArrayList<>());
		variable = new QualIdentifier(new Identifier(induceOnVariableName.copy(), new ArrayList<>()), Optional.empty());
		innerTerm = new IdentifierTerm(variable, new ArrayList<>());
		variables = List.of(new SortedVariable(induceOnVariableName.copy(), sort.copy()));
		forallTermIntVariable = new ForallTerm(variables, innerTerm.copy());
		scriptWithTerm = new Script(List.of(new SyntaxCommand.Assert(forallTermIntVariable)));
		instance = new ConstantTerm(new NumeralConstant(new NumeralToken("42")));
	}

	@Nested
	class SuccessCases {

		@Test
		void shouldCreateNewTerms() {
			// (assert (forall ((x Bool)) x))
			SortedVariable var = new SortedVariable(new SymbolToken("x"),
					new Sort(new Identifier(new SymbolToken("Bool"), new ArrayList<Index>()), new ArrayList<Sort>()));
			IdentifierTerm term = new IdentifierTerm(
					new QualIdentifier(new Identifier(new SymbolToken("x"), new ArrayList<Index>()), Optional.empty()),
					new ArrayList<Term>());
			ForallTerm forall = new ForallTerm(List.of(var), term);
			Assert assertion = new Assert(forall);
			Script script = new Script(List.of(assertion));
			IdentifierTerm instance = new IdentifierTerm(
					new QualIdentifier(new Identifier(new SymbolToken("false"), new ArrayList<Index>()), Optional.empty()),
					new ArrayList<Term>());
			// create new rule
			// instantiate x with false
			InstantiationRule rule = new InstantiationRule(script, forall, var, instance);
			// test if only one script returned
			List<Script> result = rule.getCreatedScripts();
			assertEquals(1, result.size());
			// test if only one expression in script
			List<Expression> exprs = result.get(0).getTopLevelExpressions();
			assertEquals(1, exprs.size());
			// test if (assert (and A B))
			assert (exprs.get(0) instanceof Assert);
			Assert newAssertion = (Assert) exprs.get(0);
			Term andTerm = new IdentifierTerm(
					new QualIdentifier(new Identifier(new SymbolToken("and"), new ArrayList<>()), Optional.empty()),
					List.of(instance, forall));
			// test if A = (forall ((x Bool)) x)
			assert (newAssertion.equals(new Assert(andTerm)));
		}

		@Test
		void shouldCreateAndApplyInstantiationRuleWhenParametersAreCorrect() {
			InstantiationRule rule = new InstantiationRule(scriptWithTerm, forallTermIntVariable,
					new SortedVariable(induceOnVariableName, sort), instance);
			assertEquals(1, rule.getCreatedScripts().size());
			Script createdScript = rule.getCreatedScripts().get(0);
			assertNotEquals(scriptWithTerm, createdScript);
		}

	}

	@Nested
	class ExceptionCases {

		@Test
		void shouldNotCreateInstantiationRuleWhenVariableIsNotInExpression() {
			SortedVariable var = new SortedVariable(new Token.SymbolToken("y"), sort);
			assertThrows(CannotApplyRuleException.class, () -> {
				InstantiationRule rule = new InstantiationRule(scriptWithTerm, forallTermIntVariable,
						var, instance);
			});
		}

		@Test
		void shouldNotCreateInstantiationRuleWhenExpressionIsNotInScript() {
			SortedVariable var = new SortedVariable(induceOnVariableName, otherSort);
			assertThrows(CannotApplyRuleException.class, () -> {
				InstantiationRule rule = new InstantiationRule(scriptWithTerm, forallTermIntVariable.copy(),
						var, instance);
			});
		}

	}

}
