/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.hide;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.AnnotationTerm;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HideRuleTest {

	@Mock
	Script script;
	@Mock
	Term term;
	@Mock
	Attribute attribute;

	@BeforeEach
	void mockScript() {
		when(script.existsIdenticallySomewhere(any(Expression.class))).thenReturn(true);
	}

	@Nested
	class SuccessCases {

		@BeforeEach
		void mockScript() {
			// Make script.getTopLevel() return an Assert command containing the term.
			when(script.getTopLevel(term)).thenReturn(new SyntaxCommand.Assert(term));
			List<Expression> content = new ArrayList<>(
					List.of(new SyntaxCommand.Assert(term), new SyntaxCommand.Assert(term.deepCopy())));
			when(script.getTopLevelExpressions()).thenReturn(content);
		}

		@Test
		void shouldSucceed() {
			assertDoesNotThrow(() -> {
				new HideRule(script, term);
			});
		}

		@Test
		void shouldCreateExactlyOneScript() {
			HideRule hideRule = new HideRule(script, term);
			assertEquals(1, hideRule.getCreatedScripts().size());
		}

	}

	@Nested
	class RuleShouldNotSucceed {

		@Mock
		Assert mockAssertion;

		@Test
		void shouldThrowCannotApplyRuleExceptionWhenExpressionIsNotInAssertion() {
			// Make script.getTopLevel() return something that does not equal an Assert command containing the term.
			when(script.getTopLevel(term)).thenReturn(term);

			assertThrows(CannotApplyRuleException.class, () -> {
				new HideRule(script, term);
			});
		}

		@Test
		void shouldThrowCannotApplyExceptionWhenThereIsOnlyOneAssertionInScript() {
			Assert assertion = new Assert(term);
			when(script.getTopLevel(term)).thenReturn(assertion);
			when(script.getTopLevelExpressions()).thenReturn(List.of(assertion));
			assertThrows(CannotApplyRuleException.class, () -> {
				new HideRule(script, term);
			});
		}

		@Test
		void shouldThrowCannotApplyExceptionWhenHiddenTermContainsAnnotations() {
			Term annotationTerm = new AnnotationTerm(term, List.of(attribute));
			Assert assertion = new Assert(annotationTerm);
			when(script.getTopLevel(annotationTerm)).thenReturn(assertion);
			assertThrows(CannotApplyRuleException.class, () -> {
				new HideRule(script, annotationTerm);
			});
		}
	}

}