/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.skolemization;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.SpecConstant.StringConstant;
import invismt.grammar.Token;
import invismt.grammar.Token.StringToken;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.commands.SyntaxCommand.DeclareConstant;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm.ExistsTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Tim Junginger
 */
class SkolemizationRuleTest {

	private Script scriptWithTerm;
	private Token.SymbolToken skolemVar;
	private Sort sort;
	private QualIdentifier variable;
	private Term innerTerm;
	private ExistsTerm existsTerm;
	private List<SortedVariable> variables;

	@BeforeEach
	void setUp() {
		skolemVar = new Token.SymbolToken("x");
		sort = new Sort(new Identifier(new Token.SymbolToken("Type"), new ArrayList<>()), new ArrayList<>());
		variable = new QualIdentifier(new Identifier(skolemVar.copy(), new ArrayList<>()), Optional.empty());
		innerTerm = new IdentifierTerm(variable, new ArrayList<>());
		variables = List.of(new SortedVariable(skolemVar.copy(), sort.copy()));
		existsTerm = new ExistsTerm(variables, innerTerm.copy());
		scriptWithTerm = new Script(List.of(new SyntaxCommand.Assert(existsTerm)));
	}

	@Nested
	class SuccessCases {

		@Test
		void shouldContainDeclareAfterSkolemization() {
			// (assert (exists ((x Int)) "something"))
			SortedVariable var = new SortedVariable(new SymbolToken("x"),
					new Sort(new Identifier(new SymbolToken("Int"), new ArrayList<Index>()), new ArrayList<Sort>()));
			ConstantTerm term = new ConstantTerm(new StringConstant(new StringToken("\"something\"")));
			ExistsTerm exists = new ExistsTerm(List.of(var), term);
			Assert assertion = new Assert(exists);
			Script script = new Script(List.of(assertion));
			// create new rule
			// skolemise x
			SkolemizationRule rule = new SkolemizationRule(script, exists, var, new SymbolToken("skolem_x"));
			// test if only one script
			List<Script> result = rule.getCreatedScripts();
			assertEquals(1, result.size());
			Script newScript = result.get(0);
			List<Expression> exprs = newScript.getTopLevelExpressions();
			// test if only 2 expression
			assertEquals(2, exprs.size());
			// test if
			// (declare-const ...)
			// (assert "something")
			assert (exprs.get(0) instanceof SyntaxCommand.DeclareConstant);
			assert (exprs.get(1).getClass() == SyntaxCommand.Assert.class);
			Assert newAssertion = (Assert) exprs.get(1);
			assert (newAssertion.getTerm().equals(term));
		}

		@Test
		void shouldSucceedWhenParametersAreCorrect() {
			SkolemizationRule rule = new SkolemizationRule(scriptWithTerm,
					existsTerm,
					new SortedVariable(skolemVar, sort), new Token.SymbolToken("z"));
			assertEquals(1, rule.getCreatedScripts().size());
			assertEquals(2, rule.getCreatedScripts().get(0).getTopLevelExpressions().size());
			SyntaxCommand declare = new DeclareConstant(new SymbolToken("z"), sort);
			Term term = new IdentifierTerm(
					new QualIdentifier(
							new Identifier(
									new Token.SymbolToken("z").copy(), new ArrayList<>()),
							Optional.empty()),
					new ArrayList<>());
			assertEquals(declare, rule.getCreatedScripts().get(0).getTopLevelExpressions().get(0));
			assertEquals(new Assert(term), rule.getCreatedScripts().get(0).getTopLevelExpressions().get(1));
		}

	}

	@Nested
	class ExceptionCases {

		@Test
		void shouldNotCreateSkolemizationRuleWhenExpressionIsNotTopLevelTermInAssertion() {
			Script script = new Script(List.of(new Assert(new IdentifierTerm(ExpressionFactory.NOT, List.of(existsTerm)))));
			SortedVariable var = new SortedVariable(skolemVar, sort);
			SymbolToken token = new Token.SymbolToken("z");
			assertThrows(CannotApplyRuleException.class, () -> {
				SkolemizationRule rule = new SkolemizationRule(script, existsTerm, var, token);
			});
		}


		@Test
		void shouldNotCreateSkolemizationRuleWhenVariableIsNotInExpression() {
			SortedVariable var = new SortedVariable(new Token.SymbolToken("y"), sort);
			SymbolToken token = new Token.SymbolToken("z");
			assertThrows(CannotApplyRuleException.class, () -> {
				SkolemizationRule rule = new SkolemizationRule(scriptWithTerm, existsTerm, var, token);
			});
		}

		@Test
		void shouldNotCreateSkolemizationRuleWhenNewVariableIsAlreadyInUse() {
			SortedVariable var = new SortedVariable(skolemVar, sort);
			SymbolToken token = new Token.SymbolToken("x");
			assertThrows(CannotApplyRuleException.class, () -> {
				SkolemizationRule rule = new SkolemizationRule(scriptWithTerm,
						existsTerm, var, token);
			});
			Script script = new Script(List.of(new Assert(new IdentifierTerm(
							new QualIdentifier(
									ExpressionFactory.makeIdentifier(
											"a", new ArrayList<>()), Optional.empty()),
							new ArrayList<>())),
					existsTerm));
			SymbolToken token2 = new Token.SymbolToken("a");
			SortedVariable var2 = new SortedVariable(skolemVar, sort);
			assertThrows(CannotApplyRuleException.class, () -> {
				SkolemizationRule rule = new SkolemizationRule(script,
						existsTerm, var2, token2);
			});
		}

	}

}
