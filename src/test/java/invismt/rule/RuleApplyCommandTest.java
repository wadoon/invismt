/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.tree.PTElement;
import invismt.tree.exception.CommandFailedToExecute;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertThrows;

class RuleApplyCommandTest {

	@Mock
	private PTElement element;
	@Mock
	private Script script;
	@Mock
	private Rule rule;
	@Mock
	private Expression expression;

	@BeforeEach
	public void setUp() {
		element = new PTElement(null, script, rule, null);
	}

	@ParameterizedTest(name = "[{index}] nChildren = {0}")
	@ValueSource(ints = {1, 2, 3, 4, 5})
	void shouldThrowCommandFailedToExecuteExceptionIfPTElementHasChildren(int nChildren) {
		for (int i = 0; i < 1; i++) {
			element.addChildren(new PTElement(element, script, rule, null));
		}

		RuleApplyCommand command = new RuleApplyCommand(rule, element);
		assertThrows(CommandFailedToExecute.class, command::execute);
	}

}