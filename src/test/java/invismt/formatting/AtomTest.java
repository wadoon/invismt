/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.grammar.Expression;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class AtomTest {

	@Mock
	Expression associatedExpr;

	String styleClass;
	String text;
	Atom atom;
	Atom styledAtom;

	@BeforeEach
	public void setUp() {
		styleClass = "sampleStyleClass";
		text = "sample_text";
		atom = new Atom(text, associatedExpr);
		styledAtom = new Atom(text, associatedExpr, styleClass);
	}

	@Test
	void shouldReturnOptionalWithStyleClassIfStyleClassHasBeenSet() {
		assertTrue(styledAtom.getStyleClass().isPresent());
		assertEquals(styledAtom.getStyleClass().get(), styleClass);
	}

	@Test
	void shouldReturnEmptyOptionalAsDefaultStyleClass() {
		assertTrue(atom.getStyleClass().isEmpty());
	}

	@Test
	void shouldReturnIdenticalExpressionAsInConstructor() {
		assertSame(associatedExpr, atom.getAssociatedExpression());
	}

	@Test
	void shouldReturnEqualTextAsInConstructor() {
		assertEquals(atom.getText(), text);
	}

}
