/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;

class BlockTest {

	@Mock
	Atom atom;

	Block block;
	List<Atom> atoms;
	int indentationLevel;

	@BeforeEach
	public void setUp() {
		indentationLevel = 0;
		atoms = new ArrayList<>();
		atoms.add(atom);
		block = new Block(indentationLevel, atoms);
	}

	@Test
	void shouldReturnEqualButNotIdenticalAtomList() {
		assertEquals(block.getAtoms(), atoms);
		assertNotSame(atoms, block.getAtoms());
		assertSame(atoms.get(0), block.getAtoms().get(0));
	}

	@Test
	void shouldReturnIndentationLevelFromConstructor() {
		assertEquals(indentationLevel, block.getIndentationLevel());
		indentationLevel = (int) Math.random();
		block = new Block(indentationLevel, atoms);
		assertEquals(indentationLevel, block.getIndentationLevel());
	}

}
