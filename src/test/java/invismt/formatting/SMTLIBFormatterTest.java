/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.parser.ScriptParserDispatcher;
import invismt.parser.antlr.SMTLIBv2Lexer;
import invismt.testutil.FileToScriptUtil;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Test String outputs of the {@link SMTLIBFormatter} class.
 */
class SMTLIBFormatterTest {

	private SMTLIBFormatter formatter;

	@BeforeEach
	public void setUp() {
		this.formatter = new SMTLIBFormatter();
	}

	/**
	 * Tests also depend on {@link invismt.parser.ScriptParserDispatcher} and {@link invismt.testutil.FileToScriptUtil}.
	 */
	@Nested
	class SMTRoundTrip {

		private static Stream<Arguments> filePaths() {
			List<Arguments> list = new ArrayList<>();
			File f = new File("src/test/resources/smtexamples/SMT");
			for (String path: f.list()) {
				list.add(arguments("../smtexamples/SMT/" + path));
			}
			list.add(arguments("smtlib_roundtrip_test.smt2"));
			return list.stream();
		}

		/**
		 * Creates a {@link Script} out of the given file via {@link FileToScriptUtil#toScript(Path)}
		 * and compares the String produced by the {@link SMTLIBFormatter} out of that Script with
		 * the original String in the file by testing whether they are both parsed the same way.
		 * <p>
		 * Whitespaces are not matched.
		 */
		@ParameterizedTest
		@MethodSource("filePaths")
		void shouldCreateEqualScriptsExceptForWhitespaces(String filePath) throws IOException {
			Path path = FileToScriptUtil.getFile("smtlibFormatterTestFiles/" + filePath);
			Script script = FileToScriptUtil.toScript(path);
			List<Expression> topLevel = script.getTopLevelExpressions();
			String input = Files.readString(path);
			StringBuilder outputBuilder = new StringBuilder();
			for (Expression expr : topLevel) {
				outputBuilder.append(formatter.print(expr) + "\n");
			}

			// Should parse into the same script as before
			ScriptParserDispatcher toDispatch = new ScriptParserDispatcher();
			assertDoesNotThrow(() -> {
				toDispatch.parseInto(outputBuilder.toString());
			});
			assertEquals(script.getTopLevelExpressions().size(),
					new Script(toDispatch.parseInto(outputBuilder.toString())).getTopLevelExpressions().size());
			for (int i = 0; i < script.getTopLevelExpressions().size(); i++) {
				assertEquals(script.getTopLevelExpressions().get(i),
						new Script(toDispatch.parseInto(outputBuilder.toString())).getTopLevelExpressions().get(i));
			}

			// SMTLIBv2Lexer should create the same tokens
			assertDoesNotThrow(() -> {
						ANTLRInputStream inputStream = new ANTLRInputStream(input);
						final SMTLIBv2Lexer lexerIn = new SMTLIBv2Lexer(inputStream);
						ANTLRInputStream outputStream = new ANTLRInputStream(outputBuilder.toString());
						final SMTLIBv2Lexer lexerOut = new SMTLIBv2Lexer(outputStream);
						assertEquals(lexerIn.getAllTokens().size(), lexerOut.getAllTokens().size());
						for (int i = 0; i < lexerIn.getAllTokens().size(); i++) {
							assertEquals(lexerIn.getAllTokens().get(i), lexerOut.getAllTokens().get(i));
						}
					});
		}

	}

}
