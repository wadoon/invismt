/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.grammar.Expression;
import invismt.grammar.GeneralExpression;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.CheckSat;
import invismt.grammar.commands.SyntaxCommand.Exit;
import invismt.grammar.commands.SyntaxCommand.Reset;
import invismt.testutil.FileToScriptUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Test String outputs of the {@link PrettyPrinter} class.
 */
public class PrettyPrinterTest {

	PrettyPrinter printer;

	@BeforeEach
	public void setUp() {
		printer = new PrettyPrinter();
	}

	@Nested
	class PrettyPrintingRoundTrip {

		private static Stream<Arguments> filePaths() {
			return Stream.of(arguments("smtlib_roundtrip_test.smt2", "compare_smtlib_roundtrip.smt2"));
		}

		/**
		 * Creates a {@link Script} out of the given file via {@link FileToScriptUtil#toScript(Path)}
		 * and compares the String produced by the {@link SMTLIBFormatter} out of that Script with
		 * the expected String given in the corresponding pretty printed file.
		 * <p>
		 * Whitespaces have to match as well, only line breaks are ignored.
		 * Each line in the given file has to be one top-level expression
		 * (i.e. a SyntaxCommand or a GeneralExpression) for the test to work.
		 */
		@ParameterizedTest
		@MethodSource("filePaths")
		void shouldCreateExpectedOutput(String filePath, String compare) throws IOException {
			Path scriptPath = FileToScriptUtil.getFile("smtlibFormatterTestFiles/" + filePath);
			Path comparePath = FileToScriptUtil.getFile("smtlibFormatterTestFiles/" + compare);
			Script script = FileToScriptUtil.toScript(scriptPath);
			List<String> lines = Files.readAllLines(comparePath, StandardCharsets.UTF_8);
			List<Expression> topLevel = script.getTopLevelExpressions();
			assertEquals(lines.size(), topLevel.size());
			for (int i = 0; i < topLevel.size(); i++) {
				assertEquals(lines.get(i), printer.print(topLevel.get(i)).toString());
			}
		}

	}


	/**
	 * Test the style classes:
	 */

	@Nested
	class StyleClassTests {

		private static Stream<Arguments> generalExpressions() {
			List<GeneralExpression> expressions = new ArrayList<>();
			expressions.add(new GeneralExpression("testString", false));
			expressions.add(new GeneralExpression("(testBrackets)", false));
			expressions.add(new GeneralExpression("test whitespace", false));
			return Stream.of(arguments(expressions));
		}

		private static Stream<Arguments> simpleCommands() {
			List<SyntaxCommand> commands = new ArrayList<>();
			commands.add(new Exit());
			commands.add(new CheckSat());
			commands.add(new Reset());
			return Stream.of(arguments(commands));
		}

		/**
		 * Tests if simple commands such as {@link Exit}, {@link Reset}, {@link CheckSat} are built into one {@link Block} with the
		 * {@link Atom#getStyleClass()} "Command".
		 */
		@ParameterizedTest
		@MethodSource("simpleCommands")
		void shouldGiveCommandsCommandStyleClass(List<SyntaxCommand> simpleCommands) {
			for (SyntaxCommand command : simpleCommands) {
				FormattedExpression built = printer.print(command);
				assertEquals(1, built.getBlocks().size());
				assertSame(command, built.getExpression());
				for (Atom atom : built.getBlocks().get(0).getAtoms()) {
					assertTrue(atom.getStyleClass().isPresent());
					assertEquals("Command", atom.getStyleClass().get());
				}
			}
		}

		@ParameterizedTest
		@MethodSource("generalExpressions")
		void shouldGiveGeneralExpressionsGeneralExpressionStyleClass(List<GeneralExpression> expressions) {
			for (GeneralExpression expression : expressions) {
				FormattedExpression built = printer.print(expression);
				assertEquals(1, built.getBlocks().size());
				assertEquals(0, built.getBlocks().get(0).getIndentationLevel());
				assertEquals(1, built.getBlocks().get(0).getAtoms().size());
				assertTrue(built.getBlocks().get(0).getAtoms().get(0).getStyleClass().isPresent());
				assertEquals("GeneralExpression", built.getBlocks().get(0).getAtoms().get(0).getStyleClass().get());
			}
		}

	}

}
