/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package utils;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;
import invismt.grammar.term.Term;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GenericTerm implements Term {

	private List<Expression> children;
	private List<Token.SymbolToken> freeVariables;

	public GenericTerm() {
		children = Collections.emptyList();
	}

	public GenericTerm(Expression... children) {
		this.children = Arrays.asList(children);
	}

	@Override
	public List<Expression> getChildren() {
		return children;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return null;
	}

	@Override
	public List<Token.SymbolToken> getBoundVariables() {
		return null;
	}

	@Override
	public List<Token.SymbolToken> getFreeVariables() {
		return null;
	}

	@Override
	public Term copy() {
		return new GenericTerm(children.toArray(Expression[]::new));
	}

	@Override
	public Term deepCopy() {
		Expression[] copiedChildren = children.stream().map(Expression::deepCopy).toArray(Expression[]::new);
		return new GenericTerm(copiedChildren);
	}
}
