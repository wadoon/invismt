/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt;

/**
 * Non-JavaFX main class to be able to be run from a jar file.
 *
 * @author Tim Junginger
 */
public final class Main {

	private Main() throws InstantiationException {
		throw new InstantiationException();
	}

	/**
	 * Starts the main method in {@link App#main(String[])}.
	 *
	 * @param args is the Command line argument
	 */
	public static void main(String[] args) {
		App.main(args);
	}

}
