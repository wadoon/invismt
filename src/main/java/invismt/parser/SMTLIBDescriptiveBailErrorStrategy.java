/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

import invismt.parser.exceptions.SMTLIBParsingException;
import org.antlr.v4.runtime.DefaultErrorStrategy;
import org.antlr.v4.runtime.InputMismatchException;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;

/**
 * Error strategy that throws an exception immediately if it fails to parse any part.
 *
 * @author Kaan Berk Yaman
 */
public class SMTLIBDescriptiveBailErrorStrategy extends DefaultErrorStrategy {

	/**
	 * Does never recover.
	 *
	 * @throws SMTLIBParsingException always
	 */
	@Override
	public void recover(Parser recognizer, RecognitionException e) throws SMTLIBParsingException {
		for (ParserRuleContext context = recognizer.getContext(); context != null; context = context.getParent()) {
			context.exception = e;
		}

		throw SMTLIBParsingException.couldNotParse(e.getOffendingToken());
	}

	/**
	 * Does not recover from problems in subrules.
	 */
	@Override
	public void sync(Parser recognizer) { // ignore default imlpementation
	}

	/**
	 * Does not recover from inline problems.
	 *
	 * @throws SMTLIBParsingException always
	 */
	@Override
	public Token recoverInline(Parser recognizer) throws SMTLIBParsingException {
		InputMismatchException e = new InputMismatchException(recognizer);
		for (ParserRuleContext context = recognizer.getContext(); context != null; context = context.getParent()) {
			context.exception = e;
		}

		throw SMTLIBParsingException.notExpected(e.getOffendingToken());
	}

}
