/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

import invismt.grammar.Expression;
import invismt.tree.ListOfProofObligation;

import java.util.List;

/**
 * Parses a string into a {@link ListOfProofObligation}. It uses
 * {@link ScriptParserDispatcher} to parse the string into a list of
 * {@link Expression}'s which then are split into parts via
 * {@link SMTLIBProblemBuilder}.
 *
 * @author Kaan Berk Yaman
 */
public class ProofObligationParserDispatcher implements ParserDispatcher<ListOfProofObligation> {

	private final ParserDispatcher<List<Expression>> toSurrogate;

	/**
	 * Creates a new ProofObligationParserDispatcher.
	 */
	public ProofObligationParserDispatcher() {
		toSurrogate = new ScriptParserDispatcher();
		// Ensure that the parsing process is cancelled if ANTLR encounters an error
	}

	@Override
	public ListOfProofObligation parseInto(String content) {
		List<Expression> parsedCommands = toSurrogate.parseInto(content);
		SMTLIBProblemBuilder builder = new SMTLIBProblemBuilder();
		return builder.buildProblems(parsedCommands);
	}
}
