/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver;

/**
 * This Enumeration contains types that answers by {@link Solver} can have.
 * ({@link SolverAnswer})
 *
 * @author Tim Junginger
 */
public enum AnswerType {
	/**
	 * Signifies that a solver has encountered an error or that communication with a solver has failed.
	 */
	ERROR,
	/**
	 * Signifies that a solver has decided that the given problem is satisfiable.
	 */
	SAT,
	/**
	 * Signifies that a solver has decided that the given problem is unsatisfiable.
	 */
	UNSAT,
	/**
	 * Signifies that a solver has either encountered an unknown expression
	 * or that the return value of the solver could not be correctly processed.
	 */
	UNKNOWN,
	/**
	 * Signifies that a solver has returned some information.
	 */
	INFO,
	/**
	 * Signifies that no output could be read from the solver.
	 * This most often indicates that no (check-sat) was found in the file.
	 */
	NONE
}
