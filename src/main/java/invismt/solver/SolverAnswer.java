/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver;

/**
 * This class represents an answer returned by a {@link Solver}.
 *
 * @author Tim Junginger
 */
public class SolverAnswer {

	private final String message;
	private final AnswerType type;

	/**
	 * @param type    type of this answer
	 * @param message the complete text message returned from the solver
	 */
	public SolverAnswer(AnswerType type, String message) {
		this.message = message;
		this.type = type;
	}

	/**
	 * Returns the type of this answer ({@link AnswerType}).
	 *
	 * @return type of this answer
	 */
	public AnswerType getType() {
		return this.type;
	}

	/**
	 * Returns a message representing further {@link Solver} output.
	 * This message can for example contain information about an Error if one occurred.
	 *
	 * @return message of this answer
	 */
	public String getMessage() {
		return this.message;
	}

}
