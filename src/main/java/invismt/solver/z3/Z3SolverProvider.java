/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver.z3;

import invismt.solver.Solver;
import invismt.solver.SolverProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class will look for lines in a text file with the following syntax:
 * Z3Solver=SolverName=SolverCommandLine
 * trivial example:
 * solvertype= Z3Solver
 * name= WindowsAlwaysSat
 * args= cmd.exe /c echo sat
 *
 * @author Tim Junginger
 */
public class Z3SolverProvider implements SolverProvider {

	private static final Logger LOGGER = Logger.getLogger(Z3SolverProvider.class.getName());

	private static List<String> getCommand(String subject) {
		List<String> matchList = new ArrayList<>();
		Pattern regex = Pattern.compile("[^\\s\"']+|\"[^\"]*\"|'[^']*'");
		Matcher regexMatcher = regex.matcher(subject);
		while (regexMatcher.find()) {
			matchList.add(regexMatcher.group());
		}
		return matchList;
	}

	@Override
	public List<Solver> getSolvers(String config) {
		ArrayList<Solver> solvers = new ArrayList<>();
		InputStream stream = getClass().getResourceAsStream(config + "/solvers.txt");
		String[] lines;
		try (BufferedReader r = new BufferedReader(new InputStreamReader(stream))) {
			lines = r.lines().toArray(String[]::new);
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Could not read solvers.txt file.");
			return solvers;
		}
		// else if not file
		for (String line : lines) {
			Solver s = this.getFromFile(config + "/" + line);
			if (s != null) {
				solvers.add(s);
			}
		}
		return solvers;
	}

	private Solver getFromFile(String config) {
		InputStream stream = getClass().getResourceAsStream(config);
		if (stream == null) {
			return null;
		}
		Properties props = new Properties();
		try (BufferedReader r = new BufferedReader(new InputStreamReader(stream))) {
			props.load(r);
		} catch (IOException e) {
			return null;
		}

		String solvertype = props.getProperty("solvertype");
		String name = props.getProperty("name");
		String args = props.getProperty("args");
		if (solvertype != null && solvertype.contentEquals("Z3Solver") && name != null && args != null) {
			return new Z3Solver(name, getCommand(args));
		}
		return null;
	}

}
