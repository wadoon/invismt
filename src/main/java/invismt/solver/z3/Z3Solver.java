/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver.z3;

import invismt.solver.AnswerType;
import invismt.solver.Solver;
import invismt.solver.SolverAnswer;
import javafx.concurrent.Task;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tim Junginger
 */
public class Z3Solver implements Solver {

	private static final Logger LOGGER = Logger.getLogger(Z3Solver.class.getName());
	private final String name;
	private final List<String> command;

	/**
	 * Constructor for Z3Solver
	 *
	 * @param name    is a Sting which hols the Name of the Solver
	 * @param command is a List of Stings which holds all the Information on, Where to Call the Command (cmd.exe),
	 *                and What Solver to Call (z3).
	 */
	public Z3Solver(String name, List<String> command) {
		this.name = name;
		this.command = command;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public Task<SolverAnswer> getTask(File file) {
		if (file == null) {
			throw new IllegalArgumentException("null as value for file is not allowed");
		}
		final List<String> argsSplitted = new ArrayList<>(this.command);
		argsSplitted.add(file.getAbsolutePath());
		return new Z3Task(argsSplitted, file);

	}

	@Override
	public List<String> getCommand() {
		return command;
	}

	private static final class Z3Task extends Task<SolverAnswer> {

		private final List<String> argsSplitted;
		private Process process;
		private BufferedReader reader;
		private StringBuilder output;
		private File file;

		private Z3Task(List<String> argsSplitted, File file) {
			this.argsSplitted = argsSplitted;
			this.output = new StringBuilder();
			this.file = file;
		}

		@Override
		protected SolverAnswer call() throws Exception {
			try {
				init();
			} catch (IOException e) {
				return new SolverAnswer(AnswerType.ERROR, e.getMessage());
			}

			boolean exited;
			do {
				// check if cancelled
				if (this.isCancelled()) {
					break;
				}
				// collect output
				collect();
				// check if error appeared
				SolverAnswer answer = convertOutput(false);
				if (answer != null && answer.getType() == AnswerType.ERROR) {
					killProcess();
					return answer;
				}
				// wait for process
				try {
					exited = process.waitFor(100, TimeUnit.MILLISECONDS);
				} catch (InterruptedException inter) {
					// check if interrupt because of cancel
					if (this.isCancelled()) {
						break;
					}
					throw inter;
				}
			} while (!exited);

			// return output based on cancelled or not
			if (this.isCancelled()) {
				killProcess();
			}
			deleteFile();
			return convertOutput(!this.isCancelled());
		}

		private void init() throws IOException {
			ProcessBuilder builder = new ProcessBuilder(argsSplitted);
			process = builder.start();
			reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		}

		private void collect() throws IOException {
			while (reader.ready()) {
				int symbol = reader.read();
				// if end of stream
				if (symbol == -1) {
					return;
				}
				// valid char
				output.append((char) symbol);
			}
		}

		private SolverAnswer convertOutput(boolean force) throws IOException {
			// collect to be sure to get current output state
			collect();
			// dump output to string
			String result = output.toString();
			result = result.replaceAll("\n?success\\s*", "");
			// only do sth if forced or line finished
			if (force || result.endsWith("\n") || result.endsWith("\r") || result.endsWith("\r\n")) {
				if (result.isBlank()) {
					return new SolverAnswer(AnswerType.NONE, result);
				} else if (result.matches("sat\\s*")) {
					return new SolverAnswer(AnswerType.SAT, result);
				} else if (result.matches("unsat\\s*")) {
					return new SolverAnswer(AnswerType.UNSAT, result);
				} else if (result.matches("unknown\\s*")) {
					return new SolverAnswer(AnswerType.UNKNOWN, result);
				} else if (result.matches("timeout\\s*")) {
					return new SolverAnswer(AnswerType.INFO, result);
				} else {
					return new SolverAnswer(AnswerType.ERROR, result);
				}
			} else {
				return null;
			}
		}

		private void killProcess() {
			LOGGER.log(Level.FINE, "Killing process");
			process.destroyForcibly();
			try {
				process.waitFor();
			} catch (InterruptedException e) {
				killProcess();
				// to be compliant, idk if this works correctly
				Thread.currentThread().interrupt();
				return;
			}
			LOGGER.log(Level.FINE, "Process killed.");
		}
		
		private void deleteFile() {
			try {
				Files.delete(Paths.get(file.getAbsolutePath()));
			} catch (IOException e) {
				// ignore for now
				LOGGER.log(Level.WARNING, "Could not delete file: {0}", file.getAbsolutePath());
			}
		}
	}
}
