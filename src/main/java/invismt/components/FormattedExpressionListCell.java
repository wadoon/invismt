/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.components;

import invismt.controller.RuleContextMenuStarter;
import invismt.formatting.Atom;
import invismt.formatting.Block;
import invismt.formatting.FormattedExpression;
import invismt.grammar.Expression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Is the List Cell for Formatted Expressions
 *
 * @author Jonathan Hunz
 * @author uynu
 */
public class FormattedExpressionListCell extends ListCell<FormattedExpression> {

	private static final Logger LOGGER = Logger.getLogger(FormattedExpressionListCell.class.getName());
	private static final String FXML_RESOURCE = "/fxml/FormattedExpressionListCell.fxml";
	private static final int INDENTATION_FACTOR = 40;

	private static final String TRANSPARENT = "#dae8fc";

	private final IdentityHashMap<Expression, List<Label>> associatedLabels;
	private final BooleanProperty visibilityProperty;
	private final ObjectProperty<DragAndDropEvent> dragAndDropEventProperty;

	private HBox container;
	private RuleContextMenuStarter starter;

	@FXML
	private VBox content;

	/**
	 * Create a new FormattedExpressionListCell.
	 * <p>
	 * This constructor does not assign any values to any of the attributes of this object.
	 * The two final fields {@link #associatedLabels} and {@link #visibilityProperty} are
	 * initialized with empty objects instead of null.
	 */
	public FormattedExpressionListCell() {
		// getClass().getResource(...) may be null -> caught by the IOException
		FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_RESOURCE));
		loader.setController(this);
		try {
			container = loader.load();
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
		associatedLabels = new IdentityHashMap<>();
		visibilityProperty = new SimpleBooleanProperty();
		dragAndDropEventProperty = new SimpleObjectProperty<>();
	}

	/**
	 * Set the associated {@link RuleContextMenuStarter} of this FormattedExpressionListCell.
	 *
	 * @param starter the RuleContextMenuStarter to be associated with this
	 *                FormattedExpressionListCell
	 */
	public void setRuleContextMenuStarter(RuleContextMenuStarter starter) {
		this.starter = starter;
	}

	/**
	 * Returns the visibility property of this FormattedExpressionListCell.
	 *
	 * @return the visibility property of this FormattedExpressionListCell;
	 * a primitive boolean value can be retrieved with {@link BooleanProperty#get()}
	 */
	public BooleanProperty getVisibilityProperty() {
		return visibilityProperty;
	}

	/**
	 * Returns an ObjectProperty of type DragAndDropEvent which changes in case the user drags a cell onto another
	 * cell. The DragAndDropEvent-Object contains information about the original index of the dragged cell and the index
	 * at which the cell was dropped.
	 *
	 * @return the DragAndDropEventProperty of this FormattedExpressionListCell
	 */
	public ObjectProperty<DragAndDropEvent> getDragAndDropEventProperty() {
		return dragAndDropEventProperty;
	}

	/**
	 * Update the content of this FormattedExpressionListCell. This directly causes
	 * the ListCell to be redrawn.
	 *
	 * @param item  {@link FormattedExpression} which should be set as the content of
	 *              this FormattedExpressionListCell
	 * @param empty whether the content of this FormattedExpressionListCell should be
	 *              empty — this option overrides the item parameter — hence,
	 *              even if item is not null, the item parameter will be ignored
	 *              as long as empty is true
	 */
	@Override
	protected void updateItem(FormattedExpression item, boolean empty) {
		super.updateItem(item, empty);

		// Not sure if this is very intuitive...
		if (empty || item == null) {
			setText(null);
			setGraphic(null);
			setDisable(true);
		} else {
			// Set visibility of content according to the visibility of the item
			visibilityProperty.set(item.isVisible());
			// Enable cell
			setDisable(false);

			if (item.isVisible()) {
				draw(item);
			} else {
				content.getChildren().setAll(new Label("..."));
			}

			setupDragAndDrop(item);
			setGraphic(container);

			// Set width to width of list view
			prefWidthProperty().bind(getListView().widthProperty().subtract(20));
		}
	}

	/**
	 * Toggle the visibility of this FormattedExpressionListCell: if the
	 * FormattedExpresionListCell is already collapsed, calling this method will
	 * make its content visible again, and vice versa.
	 */
	@FXML
	private void toggleCollapse() {
		// Fetch item and toggle current visibility
		FormattedExpression item = itemProperty().get();
		item.setVisible(!item.isVisible());
		visibilityProperty.set(item.isVisible());

		if (item.isVisible()) {
			draw(item);
		} else {
			content.getChildren().setAll(new Label("..."));
		}

		// Set updated item
		itemProperty().set(item);
	}

	/**
	 * (Re-)Draws this FormattedExpressionListCell with the given
	 * FormattedExpression. This expectedly discards the current content of the
	 * ListCell.
	 *
	 * @param formattedExpression the formatted expression which should in be
	 *                            contained in this FormattedExpressionListCell
	 */
	private void draw(FormattedExpression formattedExpression) {
		// Clear content and associations from previous draw(...) calls
		content.getChildren().clear();
		associatedLabels.clear();

		// Create a new FlowPane for each Block and place it in the content VBox
		for (Block block : formattedExpression.getBlocks()) {
			HBox line = new HBox();

			// Indent line by adding an empty label of the preferred indentation width
			Label indent = new Label();
			indent.setPrefWidth(block.getIndentationLevel() * (double) INDENTATION_FACTOR);
			line.getChildren().add(indent);

			// Create a Label for each Atom and place it in its Blocks FlowPane
			for (Atom atom : block.getAtoms()) {
				Label label = new Label(atom.getText());

				// Set style class of Atom
				Optional<String> styleClass = atom.getStyleClass();
				if (styleClass.isPresent()) {
					label.getStyleClass().add(styleClass.get());
				}

				line.getChildren().add(label);

				// Associate expression with matching label
				Expression expression = atom.getAssociatedExpression();
				associatedLabels.computeIfAbsent(expression, expression1 -> new ArrayList<>());
				associatedLabels.get(expression).add(label);

				// Highlight label on hover and remove highlighting when the cursor's moved away
				label.setOnMouseEntered(m -> highlightAssociatedLabels(expression, true));
				label.setOnMouseExited(m -> highlightAssociatedLabels(expression, false));
				// Set a context menu handler for this label
				label.setOnContextMenuRequested(arg0 -> {
					starter.setExpression(expression);
					starter.start(label);
				});
			}

			content.getChildren().add(line);
		}
	}

	/**
	 * Toggles highlighting on labels (contained in this
	 * FormattedExpressionListCell) associated with the given @see Expression:
	 * whether this method turns off or turns on highlighting depends on the
	 * highlight parameter. Dehighlighting a label that isn't highlighted has no
	 * effect, and vice versa.
	 *
	 * @param expression the expression, whose associated labels should be
	 *                   (de-)highlighted
	 * @param highlight  whether this method should highlight (true) or dehighlight
	 *                   (false) the associated labels
	 */
	private void highlightAssociatedLabels(Expression expression, boolean highlight) {
		if (expression == null) {
			return;
		}

		// Highlight every child expression
		for (Expression childExpression : expression.getChildren()) {
			highlightAssociatedLabels(childExpression, highlight);
		}

		String color = highlight ? TRANSPARENT : "transparent";

		List<Label> labels = associatedLabels.get(expression);
		if (labels == null) {
			return;
		}
		// Set new background color for every associated label
		for (Label label : labels) {
			label.setStyle(String.format("-fx-background-color: %s", color));
		}
	}

	private void setupDragAndDrop(FormattedExpression item) {

		setOnDragDetected(event -> {
			if (item == null) {
				return;
			}

			Dragboard dragboard = startDragAndDrop(TransferMode.ANY);
			ClipboardContent clipboardContent = new ClipboardContent();

			int draggedIndex = getListView().getItems().indexOf(item);
			clipboardContent.putString(Integer.toString(draggedIndex));

			WritableImage snapshot = content.snapshot(new SnapshotParameters(), null);
			dragboard.setDragView(snapshot);

			dragboard.setContent(clipboardContent);
			event.consume();
		});

		setOnDragOver(event -> {
			if (event.getGestureSource() != this && event.getDragboard().hasString()) {
				event.acceptTransferModes(TransferMode.MOVE);
			}

			event.consume();
		});

		setOnDragEntered(event -> {
			if (event.getGestureSource() != this && event.getDragboard().hasString()) {
				setOpacity(0.5);
			}
		});

		setOnDragExited(event -> {
			if (event.getGestureSource() != this && event.getDragboard().hasString()) {
				setOpacity(1);
			}
		});

		setOnDragDropped(event -> {
			if (item == null) {
				return;
			}

			Dragboard dragboard = event.getDragboard();
			boolean success = false;

			if (dragboard.hasString()) {
				ObservableList<FormattedExpression> items = getListView().getItems();

				int draggedIndex = Integer.parseInt(dragboard.getString());
				int droppedIndex = items.indexOf(item);
				dragAndDropEventProperty.setValue(new DragAndDropEvent(draggedIndex, droppedIndex));

				success = true;
			}

			event.setDropCompleted(success);
			event.consume();
		});
	}

	/**
	 * Handles Drag And Drop Events
	 */
	public static class DragAndDropEvent {
		int draggedIndex;
		int droppedIndex;

		/**
		 * Handles Drag and Drop
		 *
		 * @param draggedIndex is the Index form which the assertion is taken
		 * @param droppedIndex is the Index to which the assertion is taken
		 */
		public DragAndDropEvent(int draggedIndex, int droppedIndex) {
			this.draggedIndex = draggedIndex;
			this.droppedIndex = droppedIndex;
		}

		/**
		 * Getter
		 *
		 * @return the Index form which the assertion is taken
		 */
		public int getDraggedIndex() {
			return draggedIndex;
		}

		/**
		 * Getter
		 *
		 * @return the Index to which the assertion is taken
		 */
		public int getDroppedIndex() {
			return droppedIndex;
		}
	}
}
