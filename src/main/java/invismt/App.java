/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt;

import com.google.common.eventbus.EventBus;
import invismt.events.CleanUpEvent;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class of InViSMT.
 */
public class App extends Application {

	private static final EventBus EVENTBUS = new EventBus();
	private static Stage primaryStage;

	/**
	 * Returns the {@link EventBus} for this application.
	 *
	 * @return EventBus for this application.
	 */
	public static EventBus getEventBus() {
		return EVENTBUS;
	}

	/**
	 * Entry point of InViSMT.
	 *
	 * @param args array of commandline arguments
	 */
	public static void main(String[] args) {
		launch();
	}

	/**
	 * Returns the primary stage thus the top level JavaFX container of the
	 * application.
	 *
	 * @return the applications primary stage.
	 */
	public static Stage getPrimaryStage() {
		return primaryStage;
	}

	@Override
	public void start(Stage stage) throws Exception {
		primaryStage = stage;
		primaryStage.setMaximized(true);
		primaryStage.setTitle("InViSMT");

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MasterView.fxml"));
		Parent root = fxmlLoader.load();

		primaryStage.setScene(new Scene(root));
		primaryStage.setOnCloseRequest(event -> {
			EVENTBUS.post(new CleanUpEvent());
			Platform.exit();
			System.exit(0);
		});
		primaryStage.show();
	}
}
