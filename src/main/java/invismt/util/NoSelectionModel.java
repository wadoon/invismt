/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.util;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.MultipleSelectionModel;

/**
 * Dummy Class that has no functionality, and is only use when an JavaFX Object needs a MultipleSelectionModel.
 *
 * @param <T> is not in use
 */
public class NoSelectionModel<T> extends MultipleSelectionModel<T> {
	@Override
	public ObservableList<Integer> getSelectedIndices() {
		return FXCollections.emptyObservableList();
	}

	@Override
	public ObservableList<T> getSelectedItems() {
		return FXCollections.emptyObservableList();
	}

	@Override
	public void selectIndices(int index, int... indices) {
		// ignore default implementation
	}

	@Override
	public void selectAll() {
		// ignore default implementation
	}

	@Override
	public void selectFirst() {
		// ignore default implementation
	}

	@Override
	public void selectLast() {
		// ignore default implementation
	}

	@Override
	public void clearAndSelect(int index) {
		// ignore default implementation
	}

	@Override
	public void select(int index) {
		// ignore default implementation
	}

	@Override
	public void select(Object obj) {
		// ignore default implementation
	}

	@Override
	public void clearSelection(int index) {
		// ignore default implementation
	}

	@Override
	public void clearSelection() {
		// ignore default implementation
	}

	@Override
	public boolean isSelected(int index) {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public void selectPrevious() {
		// ignore default implementation
	}

	@Override
	public void selectNext() {
		// ignore default implementation
	}
}
