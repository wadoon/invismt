/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import com.google.gson.annotations.SerializedName;
import invismt.grammar.Expression;

import java.util.List;

/**
 * Representation of a configuration of a {@link PrettyPrinter}.
 */
public class PrettyPrinterConfig {

	private static final String WHITESPACE = " ";
	private final List<Identifier> identifiers;

	/**
	 * Create a new PrettyPrinterConfig with a list of {@link Identifier}
	 * objects that encapsulate parameters defining different aspects
	 * of the representation of {@link invismt.grammar.Expression Expressions}.
	 *
	 * @param identifiers List of {@link Identifier} configurations.
	 */
	public PrettyPrinterConfig(List<Identifier> identifiers) {
		this.identifiers = identifiers;
	}

	/**
	 * Returns the configured {@link Notation} for an
	 * {@link Identifier} with a given {@link Identifier#name}.
	 *
	 * @param identifierName the {@link Identifier#name} of the {@link Identifier}
	 * @return the {@link Notation} or null, if the Identifier
	 * with the given name is not configured inside of this
	 * PrettyPrinterConfig
	 */
	public Notation getNotation(String identifierName) {
		Identifier identifier = getIdentifierByName(identifierName);
		return identifier != null ? identifier.notation : null;
	}

	/**
	 * Returns the configured alias String for an
	 * {@link Identifier} with a given {@link Identifier#name}.
	 * <p>
	 * That alias can be inserted instead of the
	 * Identifier's name in its representation.
	 *
	 * @param identifierName the {@link Identifier#name} of the {@link Identifier}
	 * @return the alias for the Identifier's name or null if the
	 * Identifier with the given name is not configured inside of
	 * this PrettyPrinterConfig
	 */
	public String getAlias(String identifierName) {
		Identifier identifier = getIdentifierByName(identifierName);
		return identifier != null ? identifier.alias : null;
	}

	/**
	 * Returns whether or not an object with the given {@link Identifier#name}
	 * is to be printed inside of parentheses.
	 *
	 * @param identifierName the {@link Identifier#name} of the {@link Identifier}
	 * @return whether or not an object with the given Identifier
	 * name is to be printed inside of parentheses, true if the Identifier
	 * with the given name is not configured inside of this PrettyPrinterConfig
	 */
	public boolean useParentheses(String identifierName) {
		Identifier identifier = getIdentifierByName(identifierName);
		return identifier == null || identifier.useParentheses;
	}

	/**
	 * @param name the {@link Identifier#name} of the desired Identifier
	 * @return the {@link Identifier} object with the given String
	 * name if it is configured inside this PrettyPrinterConfig,
	 * null if it is not.
	 */
	private Identifier getIdentifierByName(String name) {
		for (Identifier i : identifiers) {
			if (i.name.equals(name)) {
				return i;
			}
		}
		return null;
	}

	/**
	 * Mathematical notations for a term that contains an operator and one or more operands.
	 */
	public enum Notation {
		/**
		 * Notation that is commonly used in arithmetics and logic. Operators are places between operands.
		 */
		@SerializedName(value = "infix") INFIX {
			@Override
			public void printInNotation(Formatter formatter, FormattedExpressionBuilder builder, boolean useParentheses,
										Expression parent, Expression operator, Expression... operands) {

				if (onlyIdentifier(formatter, builder, parent, operator, operands)) {
					return;
				}

				if (operands.length >= 2 || useParentheses) {
					builder.append("(", parent);
				}

				for (int i = 0; i < operands.length - 1; i++) {
					operands[i].accept(formatter);
					builder.append(WHITESPACE, parent);
					builder.setAssociatedExpression(parent);
					operator.accept(formatter);
					builder.resetAssociatedExpression();
					builder.append(WHITESPACE, parent);
				}
				operands[operands.length - 1].accept(formatter);

				if (useParentheses || operands.length >= 2) {
					builder.append(")", parent);
				}
			}

		},
		/**
		 * Notation in which operators preceded their operands. Also known as Polish notation.
		 */
		@SerializedName(value = "prefix", alternate = "PN") PREFIX {
			@Override
			public void printInNotation(Formatter formatter, FormattedExpressionBuilder builder, boolean useParentheses,
										Expression parent, Expression operator, Expression... operands) {

				if (onlyIdentifier(formatter, builder, parent, operator, operands)) {
					return;
				}

				builder.setAssociatedExpression(parent);
				operator.accept(formatter);
				builder.resetAssociatedExpression();

				if (useParentheses) {
					builder.append("(", parent);
				}

				for (int i = 0; i < operands.length - 1; i++) {
					operands[i].accept(formatter);
					builder.append(",", parent);
					builder.append(WHITESPACE, parent);
				}
				operands[operands.length - 1].accept(formatter);

				if (useParentheses) {
					builder.append(")", parent);
				}

			}

		},
		/**
		 * Notation in which operators follow their operands. Also known as reverse Polish notation.
		 */
		@SerializedName(value = "postfix", alternate = "RPN") POSTFIX {
			@Override
			public void printInNotation(Formatter formatter, FormattedExpressionBuilder builder, boolean useParentheses,
										Expression parent, Expression operator, Expression... operands) {

				if (onlyIdentifier(formatter, builder, parent, operator, operands)) {
					return;
				}

				if (useParentheses) {
					builder.append("(", parent);
				}

				for (int i = 0; i < operands.length - 1; i++) {
					operands[i].accept(formatter);
					builder.append(",", parent);
					builder.append(WHITESPACE, parent);
				}
				operands[operands.length - 1].accept(formatter);

				if (useParentheses) {
					builder.append(")", parent);
				}

				builder.setAssociatedExpression(parent);
				operator.accept(formatter);
				builder.resetAssociatedExpression();
			}

		};

		/**
		 * Prints an {@link Expression} that consists of an operator (representing a function) and operands
		 * (representing function arguments) according to a given {@link Notation} and whether it is to be
		 * printed inside of parentheses or not.
		 *
		 * @param formatter      {@link Formatter} that is used to print the operator and operands
		 * @param builder        {@link FormattedExpressionBuilder} that is used to build the
		 *                       {@link FormattedExpression} that represents the printed parent expression
		 * @param useParentheses whether the builder is to append
		 *                       parentheses ( ) around the created formatted expression
		 * @param parent         the {@link Expression} associated with the atoms that are appended to the builder in
		 *                       the method at hand, e.g. the IdentifierTerm that is associated with the atoms in
		 *                       between the printed identifier and identified terms
		 * @param operator       the {@link Expression "function"} that is applied to the operands
		 * @param operands       the {@link Expression "function arguments"} list that the operator works on
		 */
		public abstract void printInNotation(Formatter formatter, FormattedExpressionBuilder builder,
											boolean useParentheses, Expression parent, Expression operator,
											Expression... operands);

		/**
		 * Checks whether there is only the operator and no operands and uses the builder on the operator
		 * if that is the case.
		 * <p>
		 * This method is used to avoid code duplications.
		 *
		 * @param formatter {@link Formatter} that is used to print the operator (and operands)
		 * @param builder   {@link FormattedExpressionBuilder} that is used to build the
		 *                  {@link FormattedExpression} that represents the printed parent expression
		 * @param parent    the {@link Expression} associated with the atoms that are appended to the builder in
		 *                  the method at hand, e.g. the IdentifierTerm that is associated with the atoms in
		 *                  between the printed identifier and identified terms
		 * @param operator  the {@link Expression "function"} that is applied to the operands
		 * @param operands  the {@link Expression "function arguments"} list that the operator works on
		 * @return whether the operands list is empty
		 */
		protected boolean onlyIdentifier(Formatter formatter, FormattedExpressionBuilder builder, Expression parent,
										Expression operator, Expression... operands) {

			if (operands.length == 0) {
				builder.setAssociatedExpression(parent);
				operator.accept(formatter);
				builder.resetAssociatedExpression();
				return true;
			}
			return false;
		}

	}

	/**
	 * Encapsulates information about the representation of an expression
	 * (e.g. an {@link invismt.grammar.Expression}, but not necessarily)
	 * that has been assigned a String name in some way about the
	 * {@link Notation notation}, the alias that can be inserted instead of
	 * the original name, and about whether or not to represent the expression
	 * inside of parentheses.
	 */
	private class Identifier {
		Notation notation;
		String name;
		String alias;
		boolean useParentheses;
	}
}
