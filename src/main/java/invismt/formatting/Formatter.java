/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;

/**
 * A Formatter produces a {@link FormattedExpression} out of an {@link Expression}.
 * Different implementations of this interface can define different ways of
 * creating formatted expressions, thus making it possible to define
 * and produce different representations of the same expression.
 * <p>
 * A formatted expression can serve as a structure in between an
 * expression itself and its display.
 * <p>
 * The visit() methods can be used to create representations out of
 * {@link Expression Expressions} depending on the concrete kind of
 * Expression at hand.
 *
 * @author Jonathan Hunz
 */
public interface Formatter extends ExpressionVisitor<Void> {

	/**
	 * Returns a unique and human-readable name
	 * of the formatter at hand.
	 *
	 * @return the name of the formatter at hand
	 */
	String getName();

	/**
	 * Formats an {@link Expression} by creating a {@link FormattedExpression}
	 *
	 * @param expression the {@link Expression} that is to be formatted
	 * @return the produced {@link FormattedExpression}
	 */
	FormattedExpression print(Expression expression);

}
