/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.grammar.Expression;
import invismt.grammar.GeneralExpression;
import invismt.grammar.SpecConstant;
import invismt.grammar.Token;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.AnnotationTerm;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.LetTerm;
import invismt.grammar.term.MatchCase;
import invismt.grammar.term.MatchTerm;
import invismt.grammar.term.Pattern;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.VariableBinding;

import java.util.List;
import java.util.Optional;

/**
 * Formats an expression into SMT-LIB Version 2.6 code.
 * For more information:
 *
 * @author Jonathan Hunz
 * @see <a href="http://smtlib.cs.uiowa.edu/papers/smt-lib-reference-v2.6-r2021-04-02.pdf">
 * SMT-LIB documentation</a>
 */
public class SMTLIBFormatter implements Formatter {

	private static final String NAME = "InViSMT SMT-LIB Formatter";

	private FormattedExpressionBuilder builder;

	@Override
	public String getName() {
		return NAME;
	}

	/**
	 * Create a {@link FormattedExpression} representing the given
	 * {@link Expression} by visiting that expression and using
	 * a {@link FormattedExpressionBuilder} to build the
	 * FormattedExpression during the visiting process.
	 *
	 * @param expression the {@link Expression} that is to be formatted
	 * @return the FormattedExpression representing the given expression
	 */
	@Override
	public FormattedExpression print(Expression expression) {
		builder = new FormattedExpressionBuilder();
		expression.accept(this);
		return builder.getFormattedExpression(expression);
	}

	@Override
	public Void visit(GeneralExpression expression) {
		builder.append(expression.toString(), expression);
		return null;
	}

	@Override
	public Void visit(QuantifiedTerm.ForallTerm forallTerm) {
		builder.append("(forall (", forallTerm);
		forallTerm.getVariables().forEach(variable -> variable.accept(this));
		builder.append(")", forallTerm);
		builder.createBlock();
		builder.increaseIndentationLevel();
		forallTerm.getTerm().accept(this);
		builder.createBlock();
		builder.decreaseIndentationLevel();
		builder.append(")", forallTerm);
		return null;
	}

	@Override
	public Void visit(QuantifiedTerm.ExistsTerm existsTerm) {
		builder.append("(exists (", existsTerm);
		existsTerm.getVariables().forEach(variable -> variable.accept(this));
		builder.append(")", existsTerm);
		builder.createBlock();
		builder.increaseIndentationLevel();
		existsTerm.getTerm().accept(this);
		builder.createBlock();
		builder.decreaseIndentationLevel();
		builder.append(")", existsTerm);
		return null;
	}

	@Override
	public Void visit(LetTerm letTerm) {
		builder.append("(let (", letTerm);
		letTerm.getBindings().forEach(binding -> binding.accept(this));
		builder.append(")", letTerm);
		letTerm.getTerm().accept(this);
		builder.append(")", letTerm);
		return null;
	}

	@Override
	public Void visit(AnnotationTerm annotationTerm) {
		builder.append("(! ", annotationTerm);
		annotationTerm.getTerm().accept(this);
		annotationTerm.getAttributes().forEach(attribute -> {
			builder.append(" ", annotationTerm);
			attribute.accept(this);
		});
		builder.append(")", annotationTerm);
		return null;
	}

	@Override
	public Void visit(MatchTerm matchTerm) {
		builder.append("(match ", matchTerm);
		matchTerm.getTerm().accept(this);
		builder.append(" (", matchTerm);
		matchTerm.getMatchCases().forEach((matchCase -> {
			builder.append(" ", matchTerm);
			matchCase.accept(this);
		}));
		builder.append("))", matchTerm);
		return null;
	}

	@Override
	public Void visit(ConstantTerm constantTerm) {
		builder.setAssociatedExpression(constantTerm);
		constantTerm.getConstant().accept(this);
		builder.resetAssociatedExpression();
		return null;
	}

	@Override
	public Void visit(IdentifierTerm identifierTerm) {
		if (!identifierTerm.getIdentifiedTerms().isEmpty()) {
			builder.append("(", identifierTerm);
		}

		builder.setAssociatedExpression(identifierTerm);
		identifierTerm.getIdentifier().accept(this);
		builder.resetAssociatedExpression();

		identifierTerm.getIdentifiedTerms().forEach((term -> {
			builder.append(" ", identifierTerm);
			term.accept(this);
		}));

		if (!identifierTerm.getIdentifiedTerms().isEmpty()) {
			builder.append(")", identifierTerm);
		}
		return null;
	}

	@Override
	public Void visit(VariableBinding variableBinding) {
		builder.append("(", variableBinding);
		variableBinding.getVariableName().accept(this);
		builder.append(" ", variableBinding);
		variableBinding.getBindingTerm().accept(this);
		builder.append(")", variableBinding);
		return null;
	}

	@Override
	public Void visit(SortedVariable sortedVariable) {
		builder.append("(", sortedVariable);
		sortedVariable.getVariableName().accept(this);
		builder.append(" ", sortedVariable);
		sortedVariable.getVariableType().accept(this);
		builder.append(")", sortedVariable);
		return null;
	}

	@Override
	public Void visit(Identifier identifier) {
		List<Index> indices = identifier.getIndices();
		if (indices.isEmpty()) {
			identifier.getSymbol().accept(this);
		} else {
			builder.append("(_ ", identifier);
			identifier.getSymbol().accept(this);
			indices.forEach(index -> {
				builder.append(" ", identifier);
				index.accept(this);
			});
			builder.append(")", identifier);
		}
		return null;
	}

	@Override
	public Void visit(Index.NumeralIndex index) {
		index.getToken().accept(this);
		return null;
	}

	@Override
	public Void visit(Index.SymbolIndex index) {
		index.getToken().accept(this);
		return null;
	}

	@Override
	public Void visit(QualIdentifier qualIdentifier) {
		Optional<Sort> identifierSort = qualIdentifier.getIdentifiedSort();
		if (identifierSort.isPresent()) {
			builder.append("(as ", qualIdentifier);
			qualIdentifier.getQualifiedIdentifier().accept(this);
			builder.append(" ", qualIdentifier);
			identifierSort.get().accept(this);
			builder.append(")", qualIdentifier);
		} else {
			qualIdentifier.getQualifiedIdentifier().accept(this);
		}
		return null;
	}

	@Override
	public Void visit(AttributeValue.ListAttributeValue value) {
		builder.append("(", value);
		value.getChildren().forEach((expression -> {
			expression.accept(this);
			builder.append(" ", value);
		}));
		builder.append(")", value);
		// TODO: Remove trailing whitespace.
		return null;
	}

	@Override
	public Void visit(AttributeValue.ConstantAttributeValue value) {
		value.getConstant().accept(this);
		return null;
	}

	@Override
	public Void visit(AttributeValue.SymbolAttributeValue value) {
		value.getToken().accept(this);
		return null;
	}

	@Override
	public Void visit(Attribute attribute) {
		attribute.getKeyword().accept(this);
		Optional<AttributeValue> attributeValue = attribute.getAttributeValue();
		if (attributeValue.isPresent()) {
			builder.append(" ", attribute);
			attributeValue.get().accept(this);
		}
		return null;
	}

	@Override
	public Void visit(Token.NumeralToken token) {
		builder.append(String.format("%s", token.getValue()), token);
		return null;
	}

	@Override
	public Void visit(Token.StringToken token) {
		builder.append(String.format(token.getValue()), token);
		return null;
	}

	@Override
	public Void visit(Token.BinaryToken token) {
		builder.append(String.format("#b%s", token.getValue()), token);
		return null;
	}

	@Override
	public Void visit(Token.DecimalToken token) {
		builder.append(String.format("%s", token.getValue()), token);
		return null;
	}

	@Override
	public Void visit(Token.HexadecimalToken token) {
		builder.append(String.format("#x%s", token.getValue()), token);
		return null;
	}

	@Override
	public Void visit(Token.SymbolToken token) {
		builder.append(token.getValue(), token);
		return null;
	}

	@Override
	public Void visit(Token.ReservedWord token) {
		builder.append(token.getValue(), token);
		return null;
	}

	@Override
	public Void visit(Token.Keyword token) {
		builder.append(String.format(":%s", token.getValue()), token);
		return null;
	}

	@Override
	public Void visit(SpecConstant.NumeralConstant constant) {
		constant.getToken().accept(this);
		return null;
	}

	@Override
	public Void visit(SpecConstant.StringConstant constant) {
		constant.getToken().accept(this);
		return null;
	}

	@Override
	public Void visit(SpecConstant.DecimalConstant constant) {
		constant.getToken().accept(this);
		return null;
	}

	@Override
	public Void visit(SpecConstant.HexadecimalConstant constant) {
		constant.getToken().accept(this);
		return null;
	}

	@Override
	public Void visit(SpecConstant.BinaryConstant constant) {
		constant.getToken().accept(this);
		return null;
	}

	@Override
	public Void visit(Sort sort) {
		List<Sort> sorts = sort.getSorts();
		boolean empty = sorts.isEmpty();
		builder.setAssociatedExpression(sort);
		if (!empty) {
			builder.append("(", sort);
		}
		sort.getSortIdentifier().accept(this);
		for (Sort listSort : sorts) {
			builder.append(" ", sort);
			listSort.accept(this);
		}
		if (!empty) {
			builder.append(")", sort);
		}
		builder.resetAssociatedExpression();
		return null;
	}

	@Override
	public Void visit(Pattern pattern) {
		List<Token.SymbolToken> symbolList = pattern.symbolList();
		if (symbolList.isEmpty()) {
			pattern.getSymbol().accept(this);
		} else {
			builder.append("(", pattern);
			pattern.getSymbol().accept(this);
			symbolList.forEach((symbolToken -> {
				builder.append(" ", pattern);
				symbolToken.accept(this);
			}));
			builder.append(")", pattern);
		}
		return null;
	}

	@Override
	public Void visit(MatchCase matchCase) {
		builder.append("(", matchCase);
		matchCase.getMatchedPattern().accept(this);
		builder.append(" ", matchCase);
		matchCase.getMatchedTerm().accept(this);
		builder.append(")", matchCase);
		return null;
	}

	@Override
	public Void visit(SyntaxCommand.Assert assertion) {
		builder.append("(assert ", assertion);
		assertion.getTerm().accept(this);
		builder.append(")", assertion);
		return null;
	}

	@Override
	public Void visit(SyntaxCommand.DeclareConstant declaration) {
		builder.append("(declare-const ", declaration);
		declaration.getConstName().accept(this);
		builder.append(" ", declaration);
		declaration.getConstType().accept(this);
		builder.append(")", declaration);
		return null;
	}

	@Override
	public Void visit(SyntaxCommand.CheckSat checkSat) {
		builder.append("(check-sat)", checkSat);
		return null;
	}

	@Override
	public Void visit(SyntaxCommand.Pop pop) {
		builder.append("(pop ", pop);
		pop.getMagnitude().accept(this);
		builder.append(")", pop);
		return null;
	}

	@Override
	public Void visit(SyntaxCommand.Push push) {
		builder.append("(push ", push);
		push.getMagnitude().accept(this);
		builder.append(")", push);
		return null;
	}

	@Override
	public Void visit(SyntaxCommand.Exit exit) {
		builder.append("(exit)", exit);
		return null;
	}

	@Override
	public Void visit(SyntaxCommand.Reset reset) {
		builder.append("(reset)", reset);
		return null;
	}

	@Override
	public Void visit(SyntaxCommand.DeclareFunction declareFunction) {
		builder.append("(declare-fun ", declareFunction);
		declareFunction.getConstName().accept(this);
		builder.append(" (", declareFunction);
		printWithDelimiter(declareFunction.getDefinitionArea(), " ", declareFunction);
		builder.append(") ", declareFunction);
		declareFunction.getTargetArea().accept(this);
		builder.append(")", declareFunction);
		return null;
	}

	private void printWithDelimiter(List<? extends Expression> expressions, String delimiter, Expression parent) {
		int lastIndex = expressions.size() - 1;

		if (lastIndex < 0) {
			return;
		}

		for (int i = 0; i < lastIndex; i++) {
			expressions.get(i).accept(this);
			builder.append(delimiter, parent);
		}
		expressions.get(lastIndex).accept(this);
	}
}
