/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

/**
 * check if two Expression are the Same
 */
public interface ExpressionComparator {
	/**
	 * check if two Expression are equal  (after the definition of Equals of the SubClasses)
	 *
	 * @param expr1 is Expression 1 of the Compare
	 * @param expr2 is Expression 2 of the Compare
	 * @return if the two Expression are teh same
	 */
	boolean equals(Expression expr1, Expression expr2);

	/**
	 * check if one Expression in the other, after the definition of Equals of the SubClasses
	 *
	 * @param outer is the Expression in which is check if the other is a part of
	 * @param inner is the Expression which is looked up in the outer
	 * @return check if one Expression in the other
	 */
	boolean containsEqually(Expression outer, Expression inner);

	/**
	 * check if two Expression are equal (with "==")
	 */
	class IdenticalComparator implements ExpressionComparator {

		@Override
		public boolean equals(Expression expr1, Expression expr2) {
			return expr1 == expr2;
		}

		@Override
		public boolean containsEqually(Expression outer, Expression inner) {
			return outer.containsIdenticallySomewhere(inner);
		}

	}

	/**
	 * check if two Expression are equal (with "equal")
	 */
	class EqualComparator implements ExpressionComparator {

		@Override
		public boolean equals(Expression expr1, Expression expr2) {
			return expr1.equals(expr2);
		}

		@Override
		public boolean containsEqually(Expression outer, Expression inner) {
			return outer.containsEqualExpressionSomewhere(inner);
		}

	}

}
