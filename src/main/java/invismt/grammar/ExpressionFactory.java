/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import invismt.grammar.Token.SymbolToken;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.Term;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Utility class providing commonly used {@link Expression expressions}
 * and generally facilitating the creation of {@link Expression expressions}.
 */
public final class ExpressionFactory {

	/**
	 * An empty list in Optional to avoid calling Optional.empty() all the time.
	 */
	public static final Optional<Sort> EMPTY_OPTIONAL = Optional.empty();
	/**
	 * An empty list in order to avoid calling the list constructor all the time.
	 */
	protected static final List<Index> EMPTY_LIST = new ArrayList<>();
	/**
	 * {@link QualIdentifier} representing the logical ite-operation.
	 */
	public static final QualIdentifier ITE = new QualIdentifier(makeIdentifier("ite", EMPTY_LIST), EMPTY_OPTIONAL);
	/**
	 * {@link QualIdentifier} representing the logical not-operation.
	 */
	public static final QualIdentifier NOT = new QualIdentifier(makeIdentifier("not", EMPTY_LIST), EMPTY_OPTIONAL);
	/**
	 * {@link QualIdentifier} representing the logical and-operation.
	 */
	public static final QualIdentifier AND = new QualIdentifier(makeIdentifier("and", EMPTY_LIST), EMPTY_OPTIONAL);
	/**
	 * {@link QualIdentifier} representing the logical or-operation.
	 */
	public static final QualIdentifier OR = new QualIdentifier(makeIdentifier("or", EMPTY_LIST), EMPTY_OPTIONAL);
	/**
	 * {@link Sort} representing the integer sort
	 */
	public static final Sort INT = new Sort(new Identifier(new SymbolToken("Int"), EMPTY_LIST), new ArrayList<>());

	private ExpressionFactory() {
	}

	// Variables, Identifiers, Sorts:

	/**
	 * Create a new {@link Identifier} out of a String name and a list of indices.
	 * The String name is used to create the {@link SymbolToken} representing the Identifier's
	 * name.
	 *
	 * @param name    the String value of the {@link SymbolToken} representing the identifier's name
	 * @param indices the list of {@link Index indices} that the identifier is derived into
	 * @return a new Identifier object with the given values
	 */
	public static Identifier makeIdentifier(String name, List<Index> indices) {
		return new Identifier(new SymbolToken(name), indices);
	}

	/**
	 * Create a new {@link IdentifierTerm} out of a String variable name.
	 * The IdentifierTerm only consists of the variable name {@link QualIdentifier}
	 * and an empty list of identified terms.
	 *
	 * @param name    the String name of the variable represented by the created term
	 * @return a new IdentifierTerm object with the given value
	 */
	public static Term varTerm(String name) {
		return new IdentifierTerm(
				new QualIdentifier(
						ExpressionFactory.makeIdentifier(name, new ArrayList<>()),
						Optional.empty()),
				new ArrayList<>());
	}
}
