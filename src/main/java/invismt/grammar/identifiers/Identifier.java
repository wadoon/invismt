/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.identifiers;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;
import invismt.grammar.Token.ReservedWordName;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * An identifier in the SMT-LIB standard version 2.6.
 * In that version, an identifier can be derived into either
 * "symbol" or "( _ symbol index+ )" where symbol is
 * a {@link Token.SymbolToken} and index is an {@link Index}.
 */
public class Identifier implements Expression {

	private final Token.SymbolToken identifierName;
	private final List<Index> indices;

	/**
	 * Create a new identifier with the token representing the identifier's
	 * name as well as a possibly empty list of indices.
	 * If the list is empty, the identifier is derived into "symbol".
	 * If it is not empty, the identifier is derived into "( _ symbol index+ )".
	 *
	 * @param identifierName {@link Token.SymbolToken} that represents the name of this identifier
	 * @param indices        list of {@link Index indices} this identifier is derived into, may be empty
	 */
	public Identifier(Token.SymbolToken identifierName, List<Index> indices) {
		this.identifierName = identifierName;
		this.indices = new ArrayList<>(indices);
	}

	/**
	 * Returns a new list of the identical {@link #indices} of this identifier.
	 * This also creates and returns a new empty list if {@link #indices} is empty.
	 *
	 * @return a new list of the identical {@link #indices} of this identifier
	 */
	public List<Index> getIndices() {
		return new ArrayList<>(indices);
	}

	/**
	 * Returns the {@link #identifierName} identically.
	 *
	 * @return the {@link #identifierName} of this term
	 */
	public Token.SymbolToken getSymbol() {
		return identifierName;
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(1);
		children.add(identifierName);
		if (indices.isEmpty()) {
			return children;
		}
		children.add(0, new Token.ReservedWord(Token.ReservedWordName.UNDERLINE));
		children.addAll(indices);
		return children;
	}

	@Override
	public Identifier copy() {
		return new Identifier(identifierName, indices);
	}

	@Override
	public Identifier deepCopy() {
		List<Index> newIndices = new ArrayList<>();
		for (Index index : indices) {
			newIndices.add(index.deepCopy());
		}
		return new Identifier(identifierName.deepCopy(), newIndices);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(identifierName, indices);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		Identifier o = (Identifier) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner;
		if (indices.isEmpty()) {
			joiner = new StringJoiner(SPACE);
		} else {
			joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
			joiner.add(ReservedWordName.UNDERLINE.toString());
		}
		joiner.add(identifierName.toString());
		for (Index index : indices) {
			joiner.add(index.toString());
		}
		return joiner.toString();
	}

}
