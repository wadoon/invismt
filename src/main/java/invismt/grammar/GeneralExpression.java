/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.List;

/**
 * An expression of no specific expression subtype in the current model of
 * version 2.6 of the SMT-LIB standard that is implemented by the respective
 * subclasses of the {@link Expression} interface.
 * <p>
 * A general expression consists of nothing but a String representation, as the
 * content of such an expression is mostly irrelevant in InViSMT (e.g. due to
 * the fact that no {@link invismt.rule.Rule} needs to process it).
 */
public class GeneralExpression implements Expression {

	private final String representation;
	private final boolean isGlobal;

	/**
	 * Create a new a general expression with a String representation and a boolean
	 * that states whether the expression is global or not which may be important
	 * when parsing something into a GeneralExpression,
	 *
	 * @param representation the String representation of the expression to be
	 *                       created
	 * @param isGlobal       if this expression is global (relevant while parsing
	 *                       groups of expressions in SMT problems),
	 * @see invismt.parser.SMTLIBProblemBuilder
	 */
	public GeneralExpression(String representation, boolean isGlobal) {
		this.representation = representation;
		this.isGlobal = isGlobal;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(representation, isGlobal);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		GeneralExpression o = (GeneralExpression) other;
		return o.representation.equals(this.representation) && o.isGlobal == this.isGlobal;
	}

	@Override
	public String toString() {
		return representation;
	}

	@Override
	public List<Expression> getChildren() {
		return new ArrayList<>();
	}

	@Override
	public Expression copy() {
		return new GeneralExpression(representation, isGlobal);
	}

	@Override
	public GeneralExpression deepCopy() {
		return new GeneralExpression(representation, isGlobal);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	/**
	 * Returns the {@link #isGlobal} attribute of this expression.
	 *
	 * @return whether the expression at hand is global or not
	 * @see invismt.parser.SMTLIBProblemBuilder
	 */
	public boolean isGlobal() {
		return isGlobal;
	}

}
