/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import com.google.common.base.Objects;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A terminal symbol in this model of version 2.6 of the SMT-LIB standard.
 * The list of children of a terminal symbol should be empty as it cannot
 * be derived any further.
 * Brackets are not explicitly modelled and have to be stored another way
 * (e.g. implicitly or via an attribute).
 */
public interface Token extends Expression {

	/**
	 * Each token's String representation can generally be viewed as a
	 * String value.
	 * <p>
	 * If there are other special interpretations of a token's value,
	 * there should be associated getter-methods in the corresponding
	 * subclass of the Token interface.
	 *
	 * @return the {@link String} value of the token at hand
	 */
	String getValue();

	/**
	 * As a token is generally a terminal symbol in this model of the SMT-LIB standard version 2.6,
	 * which means it is not derived further into anything, the default return value of this method
	 * is an empty list.
	 *
	 * @return an empty list as tokens do not have children
	 */
	default List<Expression> getChildren() {
		return new ArrayList<>();
	}

	/**
	 * The names of {@link ReservedWord reserved words} can have.
	 * If a new name out of the SMT-LIB standard version 2.6
	 * is needed in a reserved word it can be added to this enum.
	 */
	enum ReservedWordName {

		FORALL("forall"), EXISTS("exists"), LET("let"), EXCLAMATION("!"), AS("as"), UNDERLINE("_"), ASSERT("assert"),
		DECLARECONST("declare-const"), DECLAREFUN("declare-fun"), MATCH("match"), CHECKSAT("check-sat"), POP("pop"),
		PUSH("push"), EXIT("exit"), RESET("reset");

		private final String value;

		/**
		 * Creates a reserved word.
		 *
		 * @param value
		 */
		ReservedWordName(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}

	}

	/**
	 * Not an explicit category of the SMT-LIB standard version 2.6.
	 * This class is used to avoid code redundancies in the Token
	 * subclasses by bringing together their common functionalities.
	 * <p>
	 * Each subclass of this class represents a Token in the SMT-LIB
	 * standard that can only have special String values which match
	 * a concrete regex pattern.
	 */
	abstract class StringPatternToken implements Token {

		/**
		 * The String value returned by {@link Token#toString()} of the token at hand.
		 */
		protected final String value;

		/**
		 * Create a new StringPatternToken with a String value and a
		 * String pattern that the value has to be matched with.
		 * <p>
		 * The {@link Token#getValue() method does not have to return
		 * the given value identically, it may edit the value to its
		 * relevant parts, for example.
		 *
		 * @param value   the return value of the {@link Token#toString()} method
		 * @param pattern the regex pattern the value has to be matched with
		 */
		protected StringPatternToken(String value, String pattern) {
			Pattern p = Pattern.compile(pattern);
			Matcher m = p.matcher(value);
			if (!m.matches()) {
				throw new IllegalArgumentException(value + " does not match pattern: " + pattern);
			}
			this.value = value;
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(value);
		}

		@Override
		public boolean equals(Object other) {
			if (other == null) {
				return false;
			}
			if (!other.getClass().equals(this.getClass())) {
				return false;
			}
			StringPatternToken o = (StringPatternToken) other;
			return o.value.equals(value);
		}

		@Override
		public String toString() {
			return value;
		}

		@Override
		public String getValue() {
			return value;
		}

	}

	/**
	 * A string token in the SMT-LIB standard version 2.6.
	 * According to that standard, the value of a string token has to match a
	 * " sequence of whitespace and printable characters in double quotes
	 * with escape sequence "" "
	 */
	class StringToken extends StringPatternToken {

		/**
		 * The String pattern a StringToken's value matches with.
		 */
		public static final String STRING_PATTERN = "\"((\"\")|[\\p{Print}\\n\\t \\r\\x8A-\\xFF&&[^\\\"]])*\"";

		/**
		 * Create a new string token with a value that matches {@link #STRING_PATTERN}.
		 *
		 * @param value the value of the string token
		 */
		public StringToken(String value) {
			super(value, STRING_PATTERN);
		}

		@Override
		public StringToken copy() {
			return new StringToken(value);
		}

		@Override
		public StringToken deepCopy() {
			return new StringToken(value);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

	/**
	 * A numeral token in the SMT-LIB standard version 2.6.
	 * According to that standard, the value of a numeral token has to match
	 * " 0 | a non-empty sequence of digits not starting with 0 "
	 */
	class NumeralToken extends StringPatternToken {

		/**
		 * The String pattern a NumeralToken's value matches with.
		 */
		public static final String NUMERAL_PATTERN = "0|[1-9]\\d*";

		/**
		 * Create a new numeral token with a value that matches {@link #NUMERAL_PATTERN}.
		 *
		 * @param value the value of the numeral token
		 */
		public NumeralToken(String value) {
			super(value, NUMERAL_PATTERN);
		}

		/**
		 * Gets the value of this NumeralToken as a BigInteger.
		 *
		 * @return value of this token
		 */
		public BigInteger getInteger() {
			return new BigInteger(this.getValue());
		}

		@Override
		public NumeralToken copy() {
			return new NumeralToken(value);
		}

		@Override
		public NumeralToken deepCopy() {
			return new NumeralToken(value);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

	/**
	 * A binary token in the SMT-LIB standard version 2.6.
	 * According to that standard, the value of a binary token has to match
	 * " #b followed by a non-empty sequence of 0 and 1 characters "
	 */
	class BinaryToken extends StringPatternToken {

		/**
		 * The String pattern a BinaryToken's value matches with.
		 */
		public static final String BINARY_PATTERN = "#b[0-1]+";

		/**
		 * Create a new string token with a value that matches {@link #BINARY_PATTERN}.
		 *
		 * @param value the value of the binary token
		 */
		public BinaryToken(String value) {
			super(value, BINARY_PATTERN);
		}

		@Override
		public String getValue() {
			return value.substring(2);
		}

		/**
		 * Gets the value of this BinaryToken as a BigInteger.
		 *
		 * @return value of this token
		 */
		public BigInteger getInteger() {
			return new BigInteger(this.value.substring(2), 2);
		}

		@Override
		public BinaryToken copy() {
			return new BinaryToken(value);
		}

		@Override
		public BinaryToken deepCopy() {
			return new BinaryToken(value);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

	/**
	 * A decimal token in the SMT-LIB standard version 2.6.
	 * According to that standard, the value of a decimal token has to match
	 * " numeral.0*numeral " where numeral is a {@link NumeralToken}.
	 * <p>
	 * As tokens are generally treated as terminal symbols in this model of
	 * the SMT-LIB standard which is why a DecimalToken is not further derived
	 * into two NumeralTokens.
	 */
	class DecimalToken extends StringPatternToken {

		/**
		 * The String pattern a DecimalToken's value matches with.
		 */
		public static final String DECIMAL_PATTERN = "(" + NumeralToken.NUMERAL_PATTERN + ")" + "\\.[0]*" + "("
				+ NumeralToken.NUMERAL_PATTERN + ")";

		/**
		 * Create a new decimal token with a value that matches {@link #DECIMAL_PATTERN}.
		 *
		 * @param value the value of the decimal token
		 */
		public DecimalToken(String value) {
			super(value, DECIMAL_PATTERN);
		}

		/**
		 * Gets the value of this DecimalToken as a double.
		 *
		 * @return value of this token
		 */
		public Double getDecimal() {
			return Double.valueOf(this.getValue());
		}

		@Override
		public DecimalToken copy() {
			return new DecimalToken(value);
		}

		@Override
		public DecimalToken deepCopy() {
			return new DecimalToken(value);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

	/**
	 * A hexadecimal token in the SMT-LIB standard version 2.6.
	 * According to that standard, the value of a hexadecimal token has to match
	 * " #x followed by a non-empty sequence of digits and letters
	 * from A to F , capitalized or not "
	 */
	class HexadecimalToken extends StringPatternToken {

		/**
		 * The String pattern a HexadecimalToken's value matches with.
		 */
		public static final String HEXDEC_PATTERN = "#x[\\p{XDigit}]+";

		/**
		 * Create a new hexadecimal token with a value that matches {@link #HEXDEC_PATTERN}.
		 *
		 * @param value the value of the hexadecimal token
		 */
		public HexadecimalToken(String value) {
			super(value, HEXDEC_PATTERN);
		}

		@Override
		public String getValue() {
			return value.substring(2);
		}

		/**
		 * Gets the value of this HexadecimalToken as a BigInteger.
		 *
		 * @return value of this token
		 */
		public BigInteger getInteger() {
			return new BigInteger(this.value.substring(2), 16);
		}

		@Override
		public HexadecimalToken copy() {
			return new HexadecimalToken(value);
		}

		@Override
		public HexadecimalToken deepCopy() {
			return new HexadecimalToken(value);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

	/**
	 * A symbol token in the SMT-LIB standard version 2.6.
	 * According to that standard, the value of a symbol token has to match
	 * <p>
	 * " simple_symbol
	 * | a sequence of whitespace and printable characters that
	 * starts and ends with | and does not otherwise include | or \ "
	 * <p>
	 * where simple_symbol is " a non-empty sequence of letters,
	 * digits and the characters + - / * = % ? ! . $ _ ˜ & ˆ < > @
	 * that does not start with a digit ".
	 */
	class SymbolToken extends StringPatternToken {

		private static final String QUOTED_SYMBOL_PATTERN = "\\|[\\p{Print}\\n\\t \\r\\x8A-\\xFF&&[^\\\\\\|]]*\\|";
		private static final String SPECIAL_SYMBOLS = "\\+\\-/\\*\\=%\\?\\!\\.\\$_~&\\^\\<\\>@";

		public static final String BEGIN_PATTERN = "[A-Za-z" + SPECIAL_SYMBOLS + "]+";
		/**
		 * The String pattern a simple symbol token's value matches with.
		 * Simple symbol tokens are not an explicit part of this model of the SMT-LIB standard
		 * which is why the pattern can be found inside of the SymbolToken class.
		 */
		public static final String SIMPLE_SYMBOL_PATTERN = BEGIN_PATTERN + "[\\d|" + BEGIN_PATTERN + "]*";
		/**
		 * The String pattern a SymbolToken's value matches with.
		 */
		public static final String COMPLETE_SYMBOL_PATTERN = "(" + QUOTED_SYMBOL_PATTERN + ")|(" + SIMPLE_SYMBOL_PATTERN
				+ ")";

		/**
		 * Create a new symbol token with a value that matches {@link #COMPLETE_SYMBOL_PATTERN}.
		 *
		 * @param value the value of the symbol token
		 */
		public SymbolToken(String value) {
			super(value, COMPLETE_SYMBOL_PATTERN);
		}

		@Override
		public SymbolToken copy() {
			return new SymbolToken(value);
		}

		@Override
		public SymbolToken deepCopy() {
			return new SymbolToken(value);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

	/**
	 * A keyword token in the SMT-LIB standard version 2.6.
	 * According to that standard, the value of a keyword token has to match
	 * <p>
	 * " : simple_symbol "
	 * <p>
	 * where simple_symbol is " a non-empty sequence of letters,
	 * digits and the characters + - / * = % ? ! . $ _ ˜ & ˆ < > @
	 * that does not start with a digit ".
	 */
	class Keyword extends StringPatternToken {

		/**
		 * The String pattern a KeywordToken's value matches with.
		 */
		public static final String KEYWORD_PATTERN = ":" + SymbolToken.SIMPLE_SYMBOL_PATTERN;

		/**
		 * Create a new symbol token with a value that matches {@link #KEYWORD_PATTERN}.
		 *
		 * @param value the value of the symbol token
		 */
		public Keyword(String value) {
			super(value, KEYWORD_PATTERN);
		}

		@Override
		public String getValue() {
			return value.substring(1);
		}

		@Override
		public Keyword copy() {
			return new Keyword(value);
		}

		@Override
		public Keyword deepCopy() {
			return new Keyword(value);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

	/**
	 * A binary token in the SMT-LIB standard version 2.6.
	 * According to that standard, the value of a binary token has to match
	 * " #b followed by a non-empty sequence of 0 and 1 characters "
	 */
	class ReservedWord implements Token {

		private final ReservedWordName name;

		/**
		 * Create a new reserved word with a given name.
		 *
		 * @param name {@link ReservedWordName} name of the reserved word at hand
		 */
		public ReservedWord(ReservedWordName name) {
			this.name = name;
		}

		@Override
		public ReservedWord copy() {
			return new ReservedWord(name);
		}

		@Override
		public ReservedWord deepCopy() {
			return new ReservedWord(name);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(name);
		}

		@Override
		public boolean equals(Object other) {
			if (other == null) {
				return false;
			}
			if (!other.getClass().equals(this.getClass())) {
				return false;
			}
			ReservedWord o = (ReservedWord) other;
			return o.name.equals(this.name);
		}

		@Override
		public String toString() {
			return name.toString();
		}

		/**
		 * @return the {@link ReservedWordName} of the reserved word at hand
		 */
		public ReservedWordName getWord() {
			return this.name;
		}

		@Override
		public String getValue() {
			return name.value;
		}

	}

}
