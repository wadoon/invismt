/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import invismt.grammar.Expression;
import invismt.grammar.Token;

import java.util.List;

/**
 * A term in the SMT-LIB standard version 2.6.
 * Different kinds of terms are represented by the implementing classes of this class.
 * Terms have bound and free variables, also according to the SMT-LIB standard.
 */
public interface Term extends Expression {

	// Maybe add "boolean isWellSorted();" ? (well-sortedness)

	/**
	 * @return the names of the variables that are bound by the term at hand
	 */
	List<Token.SymbolToken> getBoundVariables();

	/**
	 * @return the names of the variables that occur free somewhere in the term at hand
	 */
	List<Token.SymbolToken> getFreeVariables();

	@Override
	Term copy();

	@Override
	Term deepCopy();

}
