/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;
import invismt.grammar.attributes.Attribute;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * An annotation term in the SMT-LIB standard version 2.6:
 * "( ! term attribute+ )"
 */
public class AnnotationTerm implements Term {

	private static final String CANNOT_CREATE = "Cannot create annotation term without attributes.";
	private final Term term;
	private final List<Attribute> attributes;

	/**
	 * Create a new annotation term with the annotated term
	 * and a non-empty list of attributes that the term is annotated with.
	 *
	 * @param annotatedTerm {@link Term} that is annotated with the given attributes
	 * @param attributes    list of {@link Attribute attributes} that the given term is annotated with
	 * @throws IllegalArgumentException if the list of attributes is empty
	 */
	public AnnotationTerm(Term annotatedTerm, List<Attribute> attributes) {
		if (attributes.isEmpty()) {
			throw new IllegalArgumentException(CANNOT_CREATE);
		}
		this.attributes = new ArrayList<>(attributes);
		this.term = annotatedTerm;
	}

	/**
	 * Returns the {@link #term annotated term} identically.
	 *
	 * @return the {@link #term annotated term} of this term
	 */
	public Term getTerm() {
		return term;
	}

	/**
	 * Returns a new list of the identical {@link #attributes} this term is annotated with.
	 *
	 * @return a new list of the identical {@link #attributes} of this term
	 */
	public List<Attribute> getAttributes() {
		return new ArrayList<>(attributes);
	}

	/**
	 * The bound variables of an annotation term are the same as the bound variables
	 * of its annotated {@link #term} as the annotated term is equivalent to the term
	 * without annotations.
	 */
	@Override
	public List<Token.SymbolToken> getBoundVariables() {
		return term.getBoundVariables();
	}

	/**
	 * The free variables of an annotation term are the same as the free variables
	 * of its annotated {@link #term} as the annotated term is equivalent to the term
	 * without annotations.
	 */
	@Override
	public List<Token.SymbolToken> getFreeVariables() {
		return term.getFreeVariables();
	}

	@Override
	public AnnotationTerm copy() {
		return new AnnotationTerm(term, attributes);
	}

	@Override
	public AnnotationTerm deepCopy() {
		List<Attribute> newAttributes = new ArrayList<>();
		for (Attribute attribute : attributes) {
			newAttributes.add(attribute.deepCopy());
		}
		return new AnnotationTerm(term.deepCopy(), newAttributes);
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(3);
		children.add(new Token.ReservedWord(Token.ReservedWordName.EXCLAMATION));
		children.add(term);
		children.addAll(attributes);
		return children;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(attributes, term);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		AnnotationTerm o = (AnnotationTerm) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		for (Expression child : getChildren()) {
			joiner.add(child.toString());
		}
		return joiner.toString();
	}

}
