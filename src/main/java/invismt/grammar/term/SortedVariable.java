/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;
import invismt.grammar.sorts.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * A sorted variable in the SMT-LIB standard version 2.6.
 * In that version, a sort can be derived into (symbol sort)
 * where symbol is a {@link Token.SymbolToken} and sort is
 * a {@link Sort}.
 */
public class SortedVariable implements Expression {

	private final Token.SymbolToken sortedVar;
	private final Sort sort;

	/**
	 * Create a new sorted variable with a symbol that represents the
	 * variable's name and a sort that represents the variable's datatype.
	 *
	 * @param sortedVar {@link Token.SymbolToken} representing this variable's name
	 * @param sort      {@link Sort} representing this variable's datatype
	 */
	public SortedVariable(Token.SymbolToken sortedVar, Sort sort) {
		this.sortedVar = sortedVar;
		this.sort = sort;
	}

	/**
	 * Returns the {@link #sort} of this sorted variable identically.
	 *
	 * @return the {@link #sort} of this sorted variable
	 */
	public Sort getVariableType() {
		return sort;
	}

	/**
	 * Returns the {@link #sortedVar} of this sorted variable identically.
	 *
	 * @return the {@link #sortedVar} of this sorted variable
	 */
	public Token.SymbolToken getVariableName() {
		return sortedVar;
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(2);
		children.add(sortedVar);
		children.add(sort);
		return children;
	}

	@Override
	public SortedVariable copy() {
		return new SortedVariable(sortedVar, sort);
	}

	@Override
	public SortedVariable deepCopy() {
		return new SortedVariable(sortedVar.deepCopy(), sort.deepCopy());
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(sortedVar, sort);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		SortedVariable o = (SortedVariable) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		joiner.add(sortedVar.toString());
		joiner.add(sort.toString());
		return joiner.toString();
	}

}

