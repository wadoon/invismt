/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * A quantified term in the SMT-LIB standard version 2.6:
 * "( quantifier ( (sorted variable)+ ) term )"
 * <p>
 * A quantified term is either an {@link ExistsTerm} with an existential quantifier
 * or a {@link ForallTerm} with a universal quantifier.
 */
public abstract class QuantifiedTerm implements Term {

	private static final String CANNOT_CREATE = "Cannot create quantified term with empty variable list.";
	protected final Term term;
	protected final Token.ReservedWord quantifier;
	protected final List<SortedVariable> variables;

	/**
	 * Create a new abstract quantified term with a quantifier represented by
	 * a {@link invismt.grammar.Token.ReservedWord},
	 * a non-empty list of {@link SortedVariable sorted variables}
	 * and the {@link Term quantified term}.
	 *
	 * @param quantifier the quantifier of this quantified term
	 * @param variables  the sorted variables that are quantified by this term
	 * @param term       the inner term of this quantified term
	 * @throws IllegalArgumentException if the list of sorted variables is empty
	 */
	protected QuantifiedTerm(Token.ReservedWord quantifier, List<SortedVariable> variables, Term term) {
		this.term = term;
		this.quantifier = quantifier;
		if (variables.isEmpty()) {
			throw new IllegalArgumentException(CANNOT_CREATE);
		}
		this.variables = new ArrayList<>(variables);
	}

	/**
	 * Returns the {@link #term quantified term} identically.
	 *
	 * @return the {@link #term quantified term} of this term
	 */
	public final Term getTerm() {
		return term;
	}

	/**
	 * Returns the {@link #quantifier} identically.
	 *
	 * @return the {@link #quantifier} of this term
	 */
	public final Token.ReservedWord getQuantifier() {
		return quantifier;
	}

	/**
	 * Returns a new list of the identical {@link #variables sorted variables} of this term.
	 *
	 * @return a new list of the identical {@link #variables} of this term
	 */
	public final List<SortedVariable> getVariables() {
		return new ArrayList<>(variables);
	}

	/**
	 * The bound variables of a quantified term are its sorted variables {@link #variables}.
	 */
	@Override
	public List<Token.SymbolToken> getBoundVariables() {
		List<Token.SymbolToken> boundVariables = new ArrayList<>();
		for (SortedVariable sortedVariable : variables) {
			boundVariables.add(sortedVariable.getVariableName());
		}
		return boundVariables;
	}

	/**
	 * The free variables of a quantified term are the free variables of its {@link #term}
	 * that are not bound by the term at hand according to the {@link #getBoundVariables()}
	 * method.
	 */
	@Override
	public List<Token.SymbolToken> getFreeVariables() {
		List<Token.SymbolToken> freeVariables = term.getFreeVariables();
		freeVariables.removeAll(getBoundVariables());
		return freeVariables;
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(3);
		children.add(quantifier);
		children.addAll(variables);
		children.add(term);
		return children;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(variables, quantifier, term);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		QuantifiedTerm o = (QuantifiedTerm) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		joiner.add(quantifier.toString());
		joiner.add(PAR_OPEN);
		for (SortedVariable sortedVariable : variables) {
			joiner.add(sortedVariable.toString());
		}
		joiner.add(PAR_CLOSED);
		joiner.add(term.toString());
		return joiner.toString();
	}

	/**
	 * A {@link QuantifiedTerm} with the universal quantifier
	 * {@link invismt.grammar.Token.ReservedWordName#FORALL}.
	 */
	public static class ForallTerm extends QuantifiedTerm {

		/**
		 * Create a new forall term with the forall quantifier
		 * represented by a {@link invismt.grammar.Token.ReservedWord}
		 * with the {@link invismt.grammar.Token.ReservedWordName#FORALL}.
		 * The quantifier is created anew each time the constructor is called.
		 *
		 * @param variables the sorted variables that are quantified by this term
		 * @param term      the inner term of this existence term
		 */
		public ForallTerm(List<SortedVariable> variables, Term term) {
			super(new Token.ReservedWord(Token.ReservedWordName.FORALL), variables, term);
		}

		@Override
		public ForallTerm copy() {
			return new ForallTerm(variables, term);
		}

		@Override
		public ForallTerm deepCopy() {
			List<SortedVariable> newVariables = new ArrayList<>();
			for (SortedVariable sortedVariable : variables) {
				newVariables.add(sortedVariable.deepCopy());
			}
			return new ForallTerm(newVariables, term.deepCopy());
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

	/**
	 * A {@link QuantifiedTerm} with the existence quantifier
	 * {@link invismt.grammar.Token.ReservedWordName#EXISTS}.
	 */
	public static class ExistsTerm extends QuantifiedTerm {

		/**
		 * Create a new existence term with the existence quantifier
		 * represented by a {@link invismt.grammar.Token.ReservedWord}
		 * with the {@link invismt.grammar.Token.ReservedWordName#EXISTS}.
		 * The quantifier is created anew each time the constructor is called.
		 *
		 * @param variables the sorted variables that are quantified by this term
		 * @param term      the inner term of this existence term
		 */
		public ExistsTerm(List<SortedVariable> variables, Term term) {
			super(new Token.ReservedWord(Token.ReservedWordName.EXISTS), variables, term);
		}

		@Override
		public ExistsTerm copy() {
			return new ExistsTerm(variables, term);
		}

		@Override
		public ExistsTerm deepCopy() {
			List<SortedVariable> newVariables = new ArrayList<>();
			for (SortedVariable sortedVariable : variables) {
				newVariables.add(sortedVariable.deepCopy());
			}
			return new ExistsTerm(newVariables, term.deepCopy());
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}
}
