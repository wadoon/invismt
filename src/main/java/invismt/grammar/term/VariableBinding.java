/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * A variable binding in the SMT-LIB standard version 2.6.
 * In that version, a sort can be derived into (symbol term)
 * where symbol is a {@link Token.SymbolToken} and term is
 * a {@link Term}.
 */
public class VariableBinding implements Expression {

	private final Token.SymbolToken boundVar;
	private final Term binder;

	/**
	 * Create a new variable binding with a symbol that represents the
	 * bound variable's name and term that the variable is bound to.
	 *
	 * @param boundVar {@link Token.SymbolToken} representing the bound variable's name
	 * @param binder   {@link Term} representing the term the variable is bound to
	 */
	public VariableBinding(Token.SymbolToken boundVar, Term binder) {
		this.boundVar = boundVar;
		this.binder = binder;
	}

	/**
	 * Returns the {@link #binder} of this variable binding identically.
	 *
	 * @return the {@link #binder} of this variable binding
	 */
	public Term getBindingTerm() {
		return binder;
	}

	/**
	 * Returns the {@link #boundVar} of this variable binding identically.
	 *
	 * @return the {@link #boundVar} of this variable binding
	 */
	public Token.SymbolToken getVariableName() {
		return boundVar;
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(2);
		children.add(boundVar);
		children.add(binder);
		return children;
	}

	@Override
	public VariableBinding copy() {
		return new VariableBinding(boundVar, binder);
	}

	@Override
	public VariableBinding deepCopy() {
		return new VariableBinding(boundVar.deepCopy(), binder.deepCopy());
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(boundVar, binder);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		VariableBinding o = (VariableBinding) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		joiner.add(boundVar.toString());
		joiner.add(binder.toString());
		return joiner.toString();
	}

}
