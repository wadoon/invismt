/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;
import invismt.grammar.Token.ReservedWordName;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.sorts.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * A qualified identifier in the SMT-LIB standard version 2.6.
 * In that version, a sort can be derived into either
 * "identifier" or "(as identifier sort)" where sort is a
 * {@link Sort} and identifier is an {@link Identifier}.
 */
public class QualIdentifier implements Expression {

	private final Identifier identifier;
	private final Optional<Sort> sort;

	/**
	 * Create a new qualified identifier with an identifier
	 * and, optionally, a sort.
	 * If the sort is present, the qualified identifier is
	 * derived into "(as identifier sort)".
	 * If the sort is not present, the qualified identifier
	 * is derived into "identifier".
	 *
	 * @param identifier {@link Identifier} of this qualified identifier
	 * @param sort       {@link Optional} of the {@link Sort} of this qualified identifier
	 */
	public QualIdentifier(Identifier identifier, Optional<Sort> sort) {
		this.identifier = identifier;
		if (sort.isEmpty()) {
			this.sort = Optional.empty();
		} else {
			this.sort = Optional.of(sort.get());
		}
	}

	/**
	 * Returns a new {@link Optional} of the identical optional
	 * {@link #sort} of this qualified identifier.
	 *
	 * @return a new {@link Optional} of the {@link #sort} of this
	 * qualified identifier
	 */
	public Optional<Sort> getIdentifiedSort() {
		if (sort.isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(sort.get());
	}

	/**
	 * Returns the {@link #identifier} of this qualified identifier identically.
	 *
	 * @return the {@link #identifier} of this qualified identifier
	 */
	public Identifier getQualifiedIdentifier() {
		return identifier;
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(3);
		children.add(identifier);
		if (sort.isEmpty()) {
			return children;
		}
		children.add(0, new Token.ReservedWord(Token.ReservedWordName.AS));
		children.add(sort.get());
		return children;
	}

	@Override
	public QualIdentifier copy() {
		return new QualIdentifier(identifier, sort);
	}

	@Override
	public QualIdentifier deepCopy() {
		Optional<Sort> copySort;
		copySort = Optional.empty();
		if (sort.isPresent()) {
			copySort = Optional.of(sort.get().deepCopy());
		}
		return new QualIdentifier(identifier.deepCopy(), copySort);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(identifier, sort);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		QualIdentifier o = (QualIdentifier) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		if (sort.isEmpty()) {
			return identifier.toString();
		}
		StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		joiner.add(ReservedWordName.AS.toString());
		joiner.add(identifier.toString());
		joiner.add(sort.get().toString());
		return joiner.toString();
	}

}