/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;
import invismt.grammar.Token.ReservedWordName;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * A let term in the SMT-LIB standard version 2.6:
 * "( let ( (variable binding)+ ) term )"
 */
public class LetTerm implements Term {

	private final Term term;
	private final List<VariableBinding> bindings;

	/**
	 * Create a new let term with a non-empty list of variable bindings
	 * and the term to which these variable bindings apply.
	 *
	 * @param bindings {@link VariableBinding bindings} that apply to the variables
	 *                 in the given term
	 * @param term     {@link Term} to which the given variable bindings apply
	 * @throws IllegalArgumentException if the list of variable bindings is empty
	 */
	public LetTerm(List<VariableBinding> bindings, Term term) {
		if (bindings.isEmpty()) {
			throw new IllegalArgumentException("Cannot create let term without variable bindings.");
		}
		this.term = term;
		this.bindings = new ArrayList<>(bindings);
	}


	/**
	 * Returns the {@link #term} identically.
	 *
	 * @return the {@link #term} of this term
	 */
	public Term getTerm() {
		return term;
	}

	/**
	 * Returns a new list of the identical {@link #bindings variable bindings} of this term.
	 *
	 * @return a new list of the identical {@link #bindings} of this term
	 */
	public List<VariableBinding> getBindings() {
		return new ArrayList<>(bindings);
	}

	/**
	 * The bound variables of a let term are the variables that are bound to one of the {@link #bindings}.
	 */
	@Override
	public List<Token.SymbolToken> getBoundVariables() {
		List<Token.SymbolToken> boundVariables = new ArrayList<>();
		for (VariableBinding variableBinding : bindings) {
			boundVariables.add(variableBinding.getVariableName());
		}
		return boundVariables;
	}

	/**
	 * According to the SMT-LIB standard, a variable occurrence in a let term of the
	 * form "let (bindings) t" is free
	 * "if it occurs free in some t_i (1 ≤ i ≤ n) and the corresponding x_i occurs
	 * free in t, or it does not occur in x 1 ,...,x n and occurs free in t"
	 */
	@Override
	public List<Token.SymbolToken> getFreeVariables() {
		List<Token.SymbolToken> freeVariables = term.getFreeVariables();
		for (VariableBinding binding : bindings) {
			for (Token.SymbolToken varName : binding.getBindingTerm().getFreeVariables()) {
				if (term.getFreeVariables().contains(binding.getVariableName())) {
					freeVariables.add(varName);
				}
			}
		}
		freeVariables.removeAll(getBoundVariables());
		return freeVariables;
	}

	@Override
	public LetTerm copy() {
		return new LetTerm(bindings, term);
	}

	@Override
	public LetTerm deepCopy() {
		List<VariableBinding> newBindings = new ArrayList<>();
		for (VariableBinding binding : bindings) {
			newBindings.add(binding.deepCopy());
		}
		return new LetTerm(newBindings, term.deepCopy());
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(3);
		children.add(new Token.ReservedWord(Token.ReservedWordName.LET));
		children.addAll(bindings);
		children.add(term);
		return children;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(bindings, term);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		LetTerm o = (LetTerm) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		joiner.add(ReservedWordName.LET.toString());
		joiner.add(PAR_OPEN);
		for (VariableBinding variableBinding : bindings) {
			joiner.add(variableBinding.toString());
		}
		joiner.add(PAR_CLOSED);
		joiner.add(term.toString());
		return joiner.toString();
	}

}
