/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;
import invismt.grammar.Token.SymbolToken;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * A pattern in the SMT-LIB standard version 2.6.
 * In that version, a sort can be derived into either
 * "symbol" or "(symbol symbol+)" where symbol is a
 * {@link SymbolToken}.
 */
public class Pattern implements Expression {

	private final Token.SymbolToken firstSymbol;
	private final List<Token.SymbolToken> symbolList;

	/**
	 * Create a new pattern with an obligatory first symbol
	 * and a possibly empty list of other symbols.
	 * If the list is empty, the pattern is derived into "symbol".
	 * If the list is not empty, the pattern is derived into
	 * "(symbol symbol+)".
	 *
	 * @param firstSymbol {@link SymbolToken} first symbol of this pattern
	 * @param symbolList  list of {@link SymbolToken} this pattern is additionally
	 *                    derived into, may be empty
	 */
	public Pattern(Token.SymbolToken firstSymbol, List<Token.SymbolToken> symbolList) {
		this.firstSymbol = firstSymbol;
		this.symbolList = new ArrayList<>(symbolList);
	}

	/**
	 * Returns the {@link #firstSymbol} of this pattern identically.
	 *
	 * @return the {@link #firstSymbol} of this pattern
	 */
	public Token.SymbolToken getSymbol() {
		return firstSymbol;
	}

	/**
	 * Creates and returns a new list of the identical
	 * {@link #symbolList symbols} of this pattern.
	 * This also creates and returns a new empty list
	 * if {@link #symbolList} is empty.
	 *
	 * @return a new list of the identical elements of the {@link #symbolList} of this identifier
	 */
	public List<Token.SymbolToken> symbolList() {
		return new ArrayList<>(symbolList);
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(3);
		children.add(firstSymbol);
		if (symbolList.isEmpty()) {
			return children;
		}
		children.addAll(symbolList);
		return children;
	}

	@Override
	public Pattern copy() {
		return new Pattern(firstSymbol, symbolList);
	}

	@Override
	public Pattern deepCopy() {
		List<Token.SymbolToken> newSymbolList = new ArrayList<>();
		for (Token.SymbolToken token : symbolList) {
			newSymbolList.add(token.deepCopy());
		}
		return new Pattern(firstSymbol.deepCopy(), newSymbolList);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(firstSymbol, symbolList);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		Pattern o = (Pattern) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner;
		if (symbolList.isEmpty()) {
			joiner = new StringJoiner(SPACE);
		} else {
			joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		}
		joiner.add(firstSymbol.toString());
		for (SymbolToken symbol : symbolList) {
			joiner.add(symbol.toString());
		}
		return joiner.toString();
	}

}
