/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import invismt.grammar.exceptions.CannotInsertExpressionException;
import invismt.grammar.exceptions.DoesNotContainExpressionException;
import invismt.util.Tuple;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * A script in the SMT-LIB standard with additional functionality.
 * <p>
 * The script does not adhere to the SMT-LIB standard as it can
 * consist of a list of any desired {@link Expression top level expression}.
 */
public class Script {

	private static final String INDEX_METADATA = "index";

	private final List<Expression> topLevelExpressions;
	private final IdentityHashMap<Expression, Metadata> metadata;

	/**
	 * Create a new Script with a list of expressions.
	 * Each expression object can only occur identically exactly once in the script.
	 *
	 * @param expressions list of {@link Expression expressions} the script at hand consists of
	 * @throws IllegalArgumentException if the list of expressions contains an identical object more than once
	 */
	public Script(List<? extends Expression> expressions) {
		topLevelExpressions = new ArrayList<>(expressions);
		if (!isUnique()) {
			throw new IllegalArgumentException("Script cannot contain the same expression twice.");
		}
		metadata = new IdentityHashMap<>();

		// Store initial ordering of expressions in metadata
		for (int i = 0; i < expressions.size(); i++) {
			setMetadata(expressions.get(i), INDEX_METADATA, i);
		}
	}

	private boolean isUnique() {
		Set<Expression> allExpressions = Collections.newSetFromMap(new IdentityHashMap<>());
		for (Expression topLevel : topLevelExpressions) {
			if (!topLevel.getChildrenOfChildren().stream().allMatch(allExpressions::add)) {
				return false;
			}
		}
		return true;
	}

	private Optional<Tuple<Expression, Integer>> findTopLevel(Expression expr) {
		int index = 0;
		for (Expression topLevel : topLevelExpressions) {
			if (topLevel.containsIdenticallySomewhere(expr)) {
				return Optional.of(new Tuple<Expression, Integer>(topLevel, Integer.valueOf(index)));
			}
			index++;
		}
		return Optional.empty();
	}

	/**
	 * Returns a list of all top-level expressions in this script.
	 * These are most often of a type declared in {@link invismt.grammar.commands.SyntaxCommand}.
	 *
	 * @return list of expressions
	 */
	public List<Expression> getTopLevelExpressions() {
		return new ArrayList<>(topLevelExpressions);
	}

	/**
	 * Check if the Expression is Toplevel
	 *
	 * @param expression is the Expression to check
	 * @return if the Expression is TopLevel
	 */
	public boolean isTopLevel(Expression expression) {
		Optional<Tuple<Expression, Integer>> topLevel = findTopLevel(expression);
		return topLevel.filter(expressionIntegerTuple -> expressionIntegerTuple.getFirst() == expression).isPresent();
	}

	/**
	 * Searches for an expression identical to the given
	 * expression in the script at hand.
	 * Returns true iff the expression can be found with ==.
	 *
	 * @param expression the expression that is searched in this script
	 * @return true if the expression is in the script, false if not
	 */
	public boolean existsIdenticallySomewhere(Expression expression) {
		return findTopLevel(expression).isPresent();
	}

	/**
	 * Searches for an expression that is equal to
	 * a given expression in the script at hand.
	 * Returns true if the expression can be found with equals().
	 *
	 * @param expression the expression that is searched in this script
	 * @return true if an expression equal to the given one is in the script
	 */
	public boolean containsEqualExpression(Expression expression) {
		for (Expression topLevel : topLevelExpressions) {
			if (topLevel.containsEqualExpressionSomewhere(expression)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Finds the top-level expression that contains the given expression.
	 *
	 * @param expression the expression whose top-level expression in this Script is searched
	 * @return top-level expression
	 * @throws DoesNotContainExpressionException if the expression parameter is not contained identically in this Script
	 */
	public Expression getTopLevel(Expression expression) {
		Optional<Tuple<Expression, Integer>> topLevel = findTopLevel(expression);
		if (topLevel.isEmpty()) {
			throw new DoesNotContainExpressionException(expression, this); // return this.clone() instead?
		}
		return topLevel.get().getFirst();
	}

	/**
	 * @param expression is the Expression
	 * @return the list of expressions the given expression is a child of (directly or indirectly)
	 */
	public List<Expression> getParentChain(Expression expression) {
		Optional<Tuple<Expression, Integer>> topLevel = findTopLevel(expression);
		if (topLevel.isEmpty()) {
			throw new DoesNotContainExpressionException(expression, this);
		}
		return topLevel.get().getFirst().getParentChain(expression);
	}

	/**
	 * Adds a list of top level insertions in the place of an already existent expression
	 * in the script at hand. If the existent expression is to be kept it has to be contained
	 * in the list of insertions.
	 *
	 * @param insertInstead the expression that will be replaced by the list of insertions
	 * @param insertions    the new expressions that will be inserted into the script at hand
	 * @return a new Script containing insertions instead of insertInstead
	 */
	public Script addTopLevelListInstead(Expression insertInstead, List<Expression> insertions) {
		Optional<Tuple<Expression, Integer>> topLevel = findTopLevel(insertInstead);

		if (topLevel.isEmpty()) {
			throw new DoesNotContainExpressionException(insertInstead, this); // return this.clone() instead?
		}
		List<Expression> newList = new ArrayList<>(topLevelExpressions);

		int insertAtIndex = topLevel.get().getSecond();
		for (Expression expr : insertions) {
			if (expr != topLevel.get().getFirst() && existsIdenticallySomewhere(expr)) {
				throw new CannotInsertExpressionException(expr, this); // return this.clone() instead?
			}
		}
		newList.remove(insertAtIndex);
		newList.addAll(insertAtIndex, insertions);
		return new Script(newList);
	}

	/**
	 * Copies the Script with the identical {@link #topLevelExpressions elements}.
	 *
	 * @return the copy of the Script
	 */
	public Script copy() {
		return new Script(topLevelExpressions);
	}

	/**
	 * Set the Metadata.
	 *
	 * @param expression is the Expression to set the Metadata to
	 * @param key        is the key
	 * @param value      is the Metadata
	 */
	public void setMetadata(Expression expression, String key, Object value) {
		metadata.computeIfAbsent(expression, expression1 -> new Metadata());
		metadata.get(expression).set(key, value);
	}

	/**
	 * Getter.
	 *
	 * @param expression is the Expression to get the Metadata to
	 * @param key        is the key
	 * @return the Metadata
	 */
	public Object getMetadata(Expression expression, String key) {
		if (!metadata.containsKey(expression) || metadata.get(expression) == null) {
			return null;
		} else {
			return metadata.get(expression).get(key);
		}
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(topLevelExpressions);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		Script o = (Script) other;
		return o.getTopLevelExpressions().equals(this.getTopLevelExpressions());
	}

	private static class Metadata {
		private final HashMap<String, Object> map;

		Metadata() {
			map = new HashMap<>();
		}

		public void set(String key, Object value) {
			if (key != null) {
				map.put(key, value);
			}
		}

		public Object get(String key) {
			return map.get(key);
		}
	}

}
