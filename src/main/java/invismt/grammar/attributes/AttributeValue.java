/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.attributes;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.SpecConstant;
import invismt.grammar.Token;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * An attribute value in version 2.6 of the SMT-LIB standard.
 * In that version, an attribute value can be derived into three forms
 * which are represented by subclasses of the AttributeValue class.
 */
public abstract class AttributeValue implements Expression {

	/**
	 * The list of expressions this attribute value is derived into.
	 */
	protected final List<Expression> children;

	/**
	 * Create an abstract attribute value with a list of children.
	 *
	 * @param children list of expressions this attribute value is derived into
	 */
	protected AttributeValue(List<? extends Expression> children) {
		this.children = new ArrayList<>(children);
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> childrenList = new ArrayList<>(1);
		childrenList.addAll(this.children);
		return childrenList;
	}

	@Override
	public abstract AttributeValue copy();

	@Override
	public abstract AttributeValue deepCopy();

	@Override
	public int hashCode() {
		return Objects.hashCode(children);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		AttributeValue o = (AttributeValue) other;
		return o.children.equals(this.children);
	}

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(SPACE);
		for (Expression child : children) {
			joiner.add(child.toString());
		}
		return joiner.toString();
	}

	/**
	 * An attribute value in the form of a list of {@link Expression expressions}.
	 */
	public static class ListAttributeValue extends AttributeValue {

		/**
		 * Create a new attribute value that is derived into
		 * "(expr*)" where expr is any {@link Expression}.
		 *
		 * @param expressionList the list of child expressions this attribute value is derived into
		 */
		public ListAttributeValue(List<? extends Expression> expressionList) {
			super(expressionList);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

		@Override
		public ListAttributeValue copy() {
			return new ListAttributeValue(children);
		}

		@Override
		public ListAttributeValue deepCopy() {
			List<Expression> newChildren = new ArrayList<>();
			for (Expression child : getChildren()) {
				newChildren.add(child.deepCopy());
			}
			return new ListAttributeValue(newChildren);
		}

		@Override
		public String toString() {
			StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
			joiner.add(super.toString());
			return joiner.toString();
		}

	}

	/**
	 * An attribute value in the form of a {@link invismt.grammar.Token.SymbolToken}.
	 */
	public static class SymbolAttributeValue extends AttributeValue {

		/**
		 * Create a new attribute value that is derived into
		 * a {@link Token.SymbolToken}.
		 *
		 * @param symbol the {@link Token.SymbolToken} this attribute value is derived into
		 */
		public SymbolAttributeValue(Token.SymbolToken symbol) {
			super(new ArrayList<>(List.of(symbol)));
		}

		/**
		 * @return the {@link Token.SymbolToken} this attribute value is derived into
		 */
		public Token.SymbolToken getToken() {
			return (Token.SymbolToken) children.get(0);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

		@Override
		public SymbolAttributeValue copy() {
			return new SymbolAttributeValue(getToken());
		}

		@Override
		public SymbolAttributeValue deepCopy() {
			return new SymbolAttributeValue(getToken().deepCopy());
		}

	}

	/**
	 * An attribute value in the form of a {@link invismt.grammar.SpecConstant}.
	 */
	public static class ConstantAttributeValue extends AttributeValue {

		/**
		 * Create a new attribute value that is derived into
		 * a {@link SpecConstant}.
		 *
		 * @param constant the {@link SpecConstant} this attribute value is derived into
		 */
		public ConstantAttributeValue(SpecConstant constant) {
			super(new ArrayList<>(List.of(constant)));
		}

		/**
		 * @return the {@link SpecConstant} this attribute value is derived into
		 */
		public SpecConstant getConstant() {
			return (SpecConstant) children.get(0);
		}

		@Override
		public ConstantAttributeValue copy() {
			return new ConstantAttributeValue(getConstant());
		}

		@Override
		public ConstantAttributeValue deepCopy() {
			return new ConstantAttributeValue(getConstant().deepCopy());
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

}