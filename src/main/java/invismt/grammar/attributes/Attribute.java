/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.attributes;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;
import invismt.grammar.Token.Keyword;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * An attribute value in version 2.6 of the SMT-LIB standard.
 * In that version, an attribute can be derived into either
 * "keyword" or "keyword attributeValue" where keyword is
 * a {@link Token.Keyword} and attributeValue is an
 * {@link AttributeValue}.
 */
public class Attribute implements Expression {

	private final Token.Keyword keyword;
	private final Optional<AttributeValue> attributeValue;

	/**
	 * Create a new Attribute that is derived into a {@link Token.Keyword}
	 * and, optionally, an {@link AttributeValue} according to the derivation
	 * rules attribute -> keyword | keyword attributeValue.
	 *
	 * @param keyword        {@link Token.Keyword} this attribute is derived into
	 * @param attributeValue {@link Optional} of the {@link AttributeValue} this attribute
	 *                       is derived into, empty Optional if this attribute
	 *                       is only derived into a single keyword
	 */
	public Attribute(Token.Keyword keyword, Optional<AttributeValue> attributeValue) {
		this.keyword = keyword;
		if (attributeValue.isEmpty()) {
			this.attributeValue = Optional.empty();
		} else {
			this.attributeValue = Optional.of(attributeValue.get());
		}
	}

	/**
	 * Returns an Optional of the {@link AttributeValue} this
	 * attribute is derived into, if it exists.
	 * Returns an empty Optional otherwise.
	 * The contained attribute value is identical to the one given to
	 * the constructor {@link #Attribute(Keyword, Optional)}.
	 *
	 * @return {@link Optional} of the {@link AttributeValue} this attribute
	 * is derived into or an empty Optional, if no attribute value exists
	 */
	public Optional<AttributeValue> getAttributeValue() {
		if (attributeValue.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of(attributeValue.get());
		}
	}

	/**
	 * Returns the identical {@link Keyword} to the one given to
	 * {@link #Attribute(Keyword, Optional)}.
	 *
	 * @return {@link Token.Keyword} this attribute is derived into
	 */
	public Token.Keyword getKeyword() {
		return keyword;
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(2);
		children.add(keyword);
		if (attributeValue.isPresent()) {
			children.add(attributeValue.get());
		}
		return children;
	}

	@Override
	public Attribute copy() {
		return new Attribute(keyword, attributeValue);
	}

	@Override
	public Attribute deepCopy() {
		Optional<AttributeValue> copyValue;
		copyValue = Optional.empty();
		if (attributeValue.isPresent()) {
			copyValue = Optional.of(attributeValue.get().deepCopy());
		}
		return new Attribute(keyword.deepCopy(), copyValue);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(keyword, attributeValue);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		Attribute o = (Attribute) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(SPACE);
		for (Expression child : getChildren()) {
			joiner.add(child.toString());
		}
		return joiner.toString();
	}

}
