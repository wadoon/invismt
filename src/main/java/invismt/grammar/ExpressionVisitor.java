/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.AnnotationTerm;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.LetTerm;
import invismt.grammar.term.MatchCase;
import invismt.grammar.term.MatchTerm;
import invismt.grammar.term.Pattern;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm.ExistsTerm;
import invismt.grammar.term.QuantifiedTerm.ForallTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.VariableBinding;

/**
 * Generic Visitor
 *
 * @param <T> Generic Object
 */
public interface ExpressionVisitor<T> {
	/**
	 * Generic Visitor
	 *
	 * @param expression expression
	 * @return a Generic Object
	 */
	T visit(GeneralExpression expression);

	// --------- Terms //

	/**
	 * Generic Visitor
	 *
	 * @param forallTerm forallTerm
	 * @return a Generic Object
	 */
	T visit(ForallTerm forallTerm);

	/**
	 * Generic Visitor
	 *
	 * @param existsTerm existsTerm
	 * @return a Generic Object
	 */
	T visit(ExistsTerm existsTerm);

	/**
	 * Generic Visitor
	 *
	 * @param letTerm letTerm
	 * @return a Generic Object
	 */
	T visit(LetTerm letTerm);

	/**
	 * Generic Visitor
	 *
	 * @param annotationTerm annotationTerm
	 * @return a Generic Object
	 */
	T visit(AnnotationTerm annotationTerm);

	/**
	 * Generic Visitor
	 *
	 * @param matchTerm matchTerm
	 * @return a Generic Object
	 */
	T visit(MatchTerm matchTerm);

	/**
	 * Generic Visitor
	 *
	 * @param constantTerm constantTerm
	 * @return a Generic Object
	 */
	T visit(ConstantTerm constantTerm);

	/**
	 * Generic Visitor
	 *
	 * @param identifierTerm identifierTerm
	 * @return a Generic Object
	 */
	T visit(IdentifierTerm identifierTerm);

	/**
	 * Generic Visitor
	 *
	 * @param variableBinding variableBinding
	 * @return a Generic Object
	 */
	T visit(VariableBinding variableBinding);

	// --------- Variables //

	/**
	 * Generic Visitor
	 *
	 * @param sortedVariable sortedVariable
	 * @return a Generic Object
	 */
	T visit(SortedVariable sortedVariable);

	/**
	 * Generic Visitor
	 *
	 * @param identifier identifier
	 * @return a Generic Object
	 */
	T visit(Identifier identifier);

	// --------- Identifiers //

	/**
	 * Generic Visitor
	 *
	 * @param index index
	 * @return a Generic Object
	 */
	T visit(Index.NumeralIndex index);

	/**
	 * Generic Visitor
	 *
	 * @param index index
	 * @return a Generic Object
	 */
	T visit(Index.SymbolIndex index);

	/**
	 * Generic Visitor
	 *
	 * @param qualIdentifier qualIdentifier
	 * @return a Generic Object
	 */
	T visit(QualIdentifier qualIdentifier);

	// --------- Attributes //

	/**
	 * Generic Visitor
	 *
	 * @param value ListAttributeValue
	 * @return a Generic Object
	 */
	T visit(AttributeValue.ListAttributeValue value);

	/**
	 * Generic Visitor
	 *
	 * @param value ConstantAttributeValue
	 * @return a Generic Object
	 */
	T visit(AttributeValue.ConstantAttributeValue value);

	/**
	 * Generic Visitor
	 *
	 * @param value value
	 * @return a Generic Object
	 */
	T visit(AttributeValue.SymbolAttributeValue value);

	/**
	 * Generic Visitor
	 *
	 * @param attribute attribute
	 * @return a Generic Object
	 */
	T visit(Attribute attribute);

	// --------- Tokens //

	/**
	 * Generic Visitor
	 *
	 * @param token NumeralToken
	 * @return a Generic Object
	 */
	T visit(Token.NumeralToken token);

	/**
	 * Generic Visitor
	 *
	 * @param token StringToken
	 * @return a Generic Object
	 */
	T visit(Token.StringToken token);

	/**
	 * Generic Visitor
	 *
	 * @param token BinaryToken
	 * @return a Generic Object
	 */
	T visit(Token.BinaryToken token);

	/**
	 * Generic Visitor
	 *
	 * @param token DecimalToken
	 * @return a Generic Object
	 */
	T visit(Token.DecimalToken token);

	/**
	 * Generic Visitor
	 *
	 * @param token HexadecimalToken
	 * @return a Generic Object
	 */
	T visit(Token.HexadecimalToken token);

	/**
	 * Generic Visitor
	 *
	 * @param token SymbolToken
	 * @return a Generic Object
	 */
	T visit(Token.SymbolToken token);

	/**
	 * Generic Visitor
	 *
	 * @param token ReservedWord
	 * @return a Generic Object
	 */
	T visit(Token.ReservedWord token);

	/**
	 * Generic Visitor
	 *
	 * @param token Keyword
	 * @return a Generic Object
	 */
	T visit(Token.Keyword token);

	// --------- Constants //

	/**
	 * Generic Visitor
	 *
	 * @param constant NumeralConstant
	 * @return a Generic Object
	 */
	T visit(SpecConstant.NumeralConstant constant);

	/**
	 * Generic Visitor
	 *
	 * @param constant StringConstant
	 * @return a Generic Object
	 */
	T visit(SpecConstant.StringConstant constant);

	/**
	 * Generic Visitor
	 *
	 * @param constant DecimalConstant
	 * @return a Generic Object
	 */
	T visit(SpecConstant.DecimalConstant constant);

	/**
	 * Generic Visitor
	 *
	 * @param constant HexadecimalConstant
	 * @return a Generic Object
	 */
	T visit(SpecConstant.HexadecimalConstant constant);

	/**
	 * Generic Visitor
	 *
	 * @param constant BinaryConstant
	 * @return a Generic Object
	 */
	T visit(SpecConstant.BinaryConstant constant);

	// --------- Sorts //

	/**
	 * Generic Visitor
	 *
	 * @param sort sort
	 * @return a Generic Object
	 */
	T visit(Sort sort);

	// --------- Match cases //

	/**
	 * Generic Visitor
	 *
	 * @param pattern pattern
	 * @return a Generic Object
	 */
	T visit(Pattern pattern);

	/**
	 * Generic Visitor
	 *
	 * @param matchCase matchCase
	 * @return a Generic Object
	 */
	T visit(MatchCase matchCase);

	// --------- Commands //

	/**
	 * Generic Visitor
	 *
	 * @param assertion assertion
	 * @return a Generic Object
	 */
	T visit(SyntaxCommand.Assert assertion);

	/**
	 * Generic Visitor
	 *
	 * @param declaration DeclareConstant
	 * @return a Generic Object
	 */
	T visit(SyntaxCommand.DeclareConstant declaration);

	/**
	 * Generic Visitor
	 *
	 * @param checkSat checkSat
	 * @return a Generic Object
	 */
	T visit(SyntaxCommand.CheckSat checkSat);

	/**
	 * Generic Visitor
	 *
	 * @param pop pop
	 * @return a Generic Object
	 */
	T visit(SyntaxCommand.Pop pop);

	/**
	 * Generic Visitor
	 *
	 * @param push push
	 * @return a Generic Object
	 */
	T visit(SyntaxCommand.Push push);

	/**
	 * Generic Visitor
	 *
	 * @param exit exit
	 * @return a Generic Object
	 */
	T visit(SyntaxCommand.Exit exit);

	/**
	 * Generic Visitor
	 *
	 * @param reset reset
	 * @return a Generic Object
	 */
	T visit(SyntaxCommand.Reset reset);

	/**
	 * Generic Visitor
	 *
	 * @param declareFunction declareFunction
	 * @return a Generic Object
	 */
	T visit(SyntaxCommand.DeclareFunction declareFunction);
}

