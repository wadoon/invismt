/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.controller;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import invismt.App;
import invismt.events.ChangeHistoryEvent;
import invismt.events.CheckScriptsEvent;
import invismt.events.ExecuteRuleEvent;
import invismt.events.SaveToFileEvent;
import invismt.events.SetFormatterEvent;
import invismt.events.SetListOfProofObligationEvent;
import invismt.events.StartSolverEvent;
import invismt.events.StopSolverEvent;
import invismt.events.UpdateSolverStateEvent;
import invismt.formatting.Formatter;
import invismt.formatting.FormatterManager;
import invismt.formatting.SMTLIBFormatter;
import invismt.grammar.Script;
import invismt.parser.ParserDispatcher;
import invismt.parser.ProofObligationParserDispatcher;
import invismt.parser.exceptions.SMTLIBParsingException;
import invismt.rule.RuleApplyCommand;
import invismt.solver.Solver;
import invismt.solver.SolverManager;
import invismt.tree.ListOfProofObligation;
import invismt.tree.PTElement;
import invismt.tree.ProofObligation;
import invismt.tree.exception.CommandFailedToExecute;
import invismt.util.Tuple;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyCombination.Modifier;
import javafx.stage.FileChooser;
import javafx.stage.WindowEvent;
import javafx.util.StringConverter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller for MasterView.
 */
public class MasterController implements EventDrivenController {

	private static final Logger LOGGER = Logger.getLogger(MasterController.class.getName());
	private static final String SHORTCUTFILE = "shortcuts.properties";
	private static final String CONFIGDIR = "/config";
	private final FileChooser fileChooser;
	private final FormatterManager formatterManager;
	private EventBus eventBus;
	private Formatter formatter;
	private SolverManager solverManager;
	private boolean verifyScripts = false;

	@FXML
	private Parent root;
	@FXML
	private Label filePathLabel;
	@FXML
	private ComboBox<Solver> solverComboBox;
	@FXML
	private ComboBox<Tuple<Class<? extends Formatter>, String>> formatterComboBox;
	@FXML
	private Button saveFileButton;
	@FXML
	private Button undoButton;
	@FXML
	private Button redoButton;
	@FXML
	private Button printCodeButton;
	@FXML
	private Button formatButton;
	@FXML
	private Button checkScriptsButton;
	@FXML
	private Button solverButton;

	/**
	 * Creates a new MasterController.
	 */
	public MasterController() {
		fileChooser = new FileChooser();
		// Configure the file chooser to only accept files with .smt2 extension
		FileChooser.ExtensionFilter smt2Filter = new FileChooser.ExtensionFilter("SMT-LIB (*.smt2)", "*.smt2");
		fileChooser.getExtensionFilters().setAll(smt2Filter);

		formatterManager = new FormatterManager();
		solverManager = new SolverManager();

		// set the eventBus to use
		this.setEventBus(App.getEventBus());
	}

	/**
	 * Called by JavaFX to initialize the controller after its root element has been
	 * completely processed.
	 */
	@FXML
	private void initialize() {
		// initialize solvers
		this.initSolvers();
		setupFormatterSelection();
		this.updateCheckScriptsButton();
		// has to be run later, when initialization is completed
		Platform.runLater(this::setupShortcuts);
		Platform.runLater(() -> this.root.getScene().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, this::cleanUp));
	}

	private void cleanUp(WindowEvent evt) {
		// cleanup function to be implemented
	}

	/**
	 * Sets up the shortcuts present in {@link #SHORTCUTFILE}. This method utilizes
	 * {@link #setShortCut(String, Properties, Runnable)}.
	 */
	private void setupShortcuts() {
		LOGGER.log(Level.INFO, "Initializing shortcuts:");
		// read shortcuts.properties for configuration
		InputStream stream = getClass().getResourceAsStream(CONFIGDIR + "/" + SHORTCUTFILE);
		Properties props = new Properties();
		try (BufferedReader r = new BufferedReader(new InputStreamReader(stream))) {
			props.load(r);
		} catch (IOException e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Could not read file " + SHORTCUTFILE + ".");
			return;
		}
		// setshortcuts
		// File open
		setShortCut("openFile", props, this::openFile);
		// File save
		setShortCut("saveFile", props, this::saveFile);
		// Format
		setShortCut("format", props, this::format);
		// SMTLIB Format
		setShortCut("SMTLIBformat", props, this::printSMTLIBCode);
		// Toggle Checking
		setShortCut("toggleChecking", props, this::toggleCheckScripts);
		// Undo
		setShortCut("undo", props, this::undo);
		// Redo
		setShortCut("redo", props, this::redo);
		// Solver start
		setShortCut("startSovler", props, this::startSolver);
		// Solver stop
		setShortCut("stopSolver", props, this::stopSolver);
	}

	private void setShortCut(String name, Properties props, Runnable r) {
		String shortcutString = props.getProperty(name);
		if (shortcutString == null) {
			LOGGER.log(Level.INFO, "No shortcut for {0}.", name);
			return;
		}
		shortcutString = shortcutString.toLowerCase();
		KeyCodeCombination shortcut = getShortCutFromString(shortcutString);
		if (shortcut == null) {
			LOGGER.log(Level.INFO, "Shortcut not parseable for: {0}", shortcutString);
			return;
		}
		Scene scene = this.root.getScene();
		scene.getAccelerators().put(shortcut, r);
	}

	private KeyCodeCombination getShortCutFromString(String s) {
		if (s == null) {
			return null;
		}
		String[] split = s.split("\\+");
		KeyCode code = null;
		List<KeyCombination.Modifier> modifiers = new ArrayList<>();
		for (String part : split) {
			switch (part) {
				case "shortcut":
					modifiers.add(KeyCombination.SHORTCUT_DOWN);
					break;
				case "alt":
					modifiers.add(KeyCombination.ALT_DOWN);
					break;
				case "control":
					modifiers.add(KeyCombination.CONTROL_DOWN);
					break;
				case "meta":
					modifiers.add(KeyCombination.META_DOWN);
					break;
				case "shift":
					modifiers.add(KeyCombination.SHIFT_DOWN);
					break;
				default:
					code = KeyCode.getKeyCode(part.toUpperCase());
			}
		}
		if (code == null) {
			LOGGER.log(Level.INFO, "No keycode in shortcut: {0}", s);
			return null;
		}
		return new KeyCodeCombination(code, modifiers.toArray(Modifier[]::new));
	}

	/**
	 * Initializes the solver combobox with solvers obtained by
	 * {@link SolverManager#init(String)}.
	 */
	private void initSolvers() {
		// set view
		solverComboBox.setCellFactory(view -> new ListCell<Solver>() {
			@Override
			protected void updateItem(Solver item, boolean empty) {
				super.updateItem(item, empty);
				if (item != null && !empty) {
					setText(item.getName());
				}
			}
		});
		solverComboBox.setButtonCell(solverComboBox.getCellFactory().call(null));
		// configure Z3SolverProvider if configDir exists
		// does not guarantee that a solver can be created
		solverManager.init(CONFIGDIR);
		if (!solverManager.getSolvers().isEmpty()) {
			solverComboBox.setValue(solverManager.getSolvers().get(0));
		}
		// set content
		ObservableList<Solver> content = FXCollections.observableArrayList(solverManager.getSolvers());
		solverComboBox.setItems(content);
	}

	@Override
	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
		this.eventBus.register(this);
	}

	/**
	 * Handles lost events to log them.
	 *
	 * @param dead are Dead Event which aren't used by other Classes
	 */
	@Subscribe
	public void handleDeadEvents(DeadEvent dead) {
		Object event = dead.getEvent();
		String message = String
				.format("Dead event encountered: %nType: %s%nEvent.toString(): %s%n", event.getClass().getName(),
						event.toString());
		LOGGER.log(Level.WARNING, message);
	}

	/**
	 * Opens a file dialog and load the file selected by the user.
	 */
	public void openFile() {
		File file = fileChooser.showOpenDialog(App.getPrimaryStage());
		if (file == null) {
			return;
		}

		String fileContent;
		try {
			fileContent = Files.readString(file.toPath());
		} catch (IOException e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Cannot read file " + file.getAbsolutePath());
			return;
		}

		ParserDispatcher<ListOfProofObligation> parseDispatcher = new ProofObligationParserDispatcher();

		ListOfProofObligation listOfProofObligation;
		try {
			listOfProofObligation = parseDispatcher.parseInto(fileContent);
		} catch (SMTLIBParsingException e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, e.getMessage());
			return;
		}
		// possibly check scripts before setting them
		if (verifyScripts) {
			List<Script> scripts = new ArrayList<>();
			for (ProofObligation ob : listOfProofObligation.getProofObligations()) {
				scripts.add(ob.getProofTree().getActivePTElement().getScript());
			}
			Solver solver = this.solverComboBox.getValue();
			if (solver == null) {
				Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Cannot check types: No solver selected.");
				return;
			}
			ScriptChecker verifier = new ScriptChecker(solver, scripts,
					() -> eventBus.post(new SetListOfProofObligationEvent(listOfProofObligation)),
					reason -> Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, reason));
			verifier.check();
		} else {
			eventBus.post(new SetListOfProofObligationEvent(listOfProofObligation));
		}

		// Enable buttons that require a file to be loaded
		saveFileButton.setDisable(false);
		undoButton.setDisable(false);
		redoButton.setDisable(false);
		printCodeButton.setDisable(false);
		formatButton.setDisable(false);
		checkScriptsButton.setDisable(false);
		formatterComboBox.setDisable(false);
		solverComboBox.setDisable(false);
		solverButton.setDisable(false);

		// Update file path indicator in status bar
		filePathLabel.setText(file.getAbsolutePath());
	}

	/**
	 * Opens a file dialog and saves the active proof tree element to the file
	 * selected by the user.
	 */
	public void saveFile() {
		File file = fileChooser.showSaveDialog(App.getPrimaryStage());
		if (file != null) {
			eventBus.post(new SaveToFileEvent(file, null));
		}
	}

	/**
	 * Changes the formatting to normal SMT-LIB standard.
	 */
	public void printSMTLIBCode() {
		eventBus.post(new SetFormatterEvent(new SMTLIBFormatter()));
	}

	/**
	 * Changes the formatting to the selected formatter. Note that
	 * {@link FormatterManager#getDefaultFormatterClass()} has to be installed in
	 * META-INF so it can be loaded in {@link FormatterManager#FormatterManager()}.
	 */
	public void format() {
		eventBus.post(new SetFormatterEvent(formatter));
	}

	/**
	 * Initializes the combobox for formatter if needed (if more than one formatter
	 * has been found) with formatters obtained via
	 * {@link FormatterManager#getInstalledFormatters()}.
	 */
	private void setupFormatterSelection() {
		Set<Tuple<Class<? extends Formatter>, String>> installedFormatters = formatterManager.getInstalledFormatters();
		if (installedFormatters.size() > 1) {
			// Only show ComboBox for formatter selection if there are more than one
			// Formatter available
			formatterComboBox.setItems(FXCollections.observableArrayList(installedFormatters));
			formatterComboBox.setConverter(new StringConverter<Tuple<Class<? extends Formatter>, String>>() {
				@Override
				public String toString(Tuple<Class<? extends Formatter>, String> tuple) {
					return tuple.getSecond();
				}

				@Override
				public Tuple<Class<? extends Formatter>, String> fromString(String string) {
					return formatterComboBox.getItems().stream().filter(tuple -> tuple.getSecond().equals(string))
							.findFirst().orElse(null);
				}
			});
			formatterComboBox.setValue(formatterComboBox.getItems().stream()
					.filter(tuple -> tuple.getFirst().equals(formatterManager.getDefaultFormatterClass())).findFirst()
					.orElse(null));
			// Update formatter when user changes selection
			formatterComboBox.valueProperty().addListener(
					(observable, oldValue, newValue) -> formatter = formatterManager.getInstance(newValue.getFirst())
							.get());
		} else {
			// Otherwise hide the ComboBox
			formatterComboBox.setManaged(false);
			formatterComboBox.setVisible(false);
		}

		// Set Formatter to an instance of the default Formatter at start
		Optional<Formatter> opt = formatterManager.getInstance(formatterManager.getDefaultFormatterClass());
		if (opt.isPresent()) {
			formatter = opt.get();
		} else {
			Alerts.showAlertAndLog(LOGGER, AlertType.WARNING, "No formatter available.");
		}
	}

	public void undo() {
		eventBus.post(new ChangeHistoryEvent(null, ChangeHistoryEvent.Type.UNDO));
	}

	public void redo() {
		eventBus.post(new ChangeHistoryEvent(null, ChangeHistoryEvent.Type.REDO));
	}

	public void startSolver() {
		this.startSolverWithElement(null);
	}

	private void startSolverWithElement(PTElement element) {
		Solver solver = this.solverComboBox.getValue();
		if (solver == null) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "No solver selected.");
			return;
		}
		eventBus.post(new StartSolverEvent(element, solver));
	}

	/**
	 * Handles the event that a solver on the specified element has been requested
	 *
	 * @param event is the Event for Solver Start
	 */
	@Subscribe
	public void handleStartSolver(StartSolverEvent event) {
		// if event completely configured ignore
		if (event.getSolver() != null) {
			return;
		}
		this.startSolverWithElement(event.getElement());
	}
	
	public void stopSolver() {
		eventBus.post(new StopSolverEvent(null));
	}

	/**
	 * Handles updates to the start and stop solver buttons in the top right corner.
	 *
	 * @param evt is the Event for Updating Solver
	 */
	@Subscribe
	public void handleUpdateSolverStateEvent(UpdateSolverStateEvent evt) {
		if (evt.getRunning()) {
			solverButton.setGraphic(new ImageView(new Image("/icons/stop.png")));
			solverButton.setOnMouseClicked(event -> stopSolver());
		} else {
			solverButton.setGraphic(new ImageView(new Image("/icons/start.png")));
			solverButton.setOnMouseClicked(event -> startSolver());
		}
		solverButton.setDisable(evt.getDisabled());
	}

	@FXML
	private void toggleCheckScripts() {
		this.verifyScripts = !this.verifyScripts;
		this.updateCheckScriptsButton();
	}

	private void updateCheckScriptsButton() {
		if (this.verifyScripts) {
			this.checkScriptsButton.setTooltip(new Tooltip("Typechecking active"));
			this.checkScriptsButton.setGraphic(new ImageView(new Image("/icons/type-checking-active.png")));
		} else {
			this.checkScriptsButton.setTooltip(new Tooltip("Typechecking inactive"));
			this.checkScriptsButton.setGraphic(new ImageView(new Image("/icons/type-checking-inactive.png")));
		}
		
		if (this.verifyScripts) {
			Solver solver = this.solverComboBox.getValue();
			if (solver == null) {
				Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Cannot check types: No solver selected.");
				return;
			}
			eventBus.post(new CheckScriptsEvent(solver));
		}
	}

	/**
	 * Verify the Rule
	 *
	 * @param event is the Event of an Executed Rule
	 */
	@Subscribe
	public void verifyRule(ExecuteRuleEvent event) {
		ProofObligation obligation = event.getObligation();
		List<Script> scripts = event.getCommand().getRule().getCreatedScripts();
		if (verifyScripts) {
			Solver solver = this.solverComboBox.getValue();
			if (solver == null) {
				Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Cannot check types: No solver selected.");
				return;
			}
			ScriptChecker verifier = new ScriptChecker(solver, scripts,
					() -> executeRuleCommand(event.getCommand(), obligation),
					reason -> Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, reason));
			verifier.check();
		} else {
			executeRuleCommand(event.getCommand(), obligation);
		}
	}

	private void executeRuleCommand(RuleApplyCommand command, ProofObligation obligation) {
		try {
			obligation.executeCommand(command);
		} catch (CommandFailedToExecute e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, e.getMessage());
		}
	}
}
