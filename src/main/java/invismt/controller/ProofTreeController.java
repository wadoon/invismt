/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.controller;

import com.google.common.collect.Lists;
import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import invismt.App;
import invismt.components.PTTreeItem;
import invismt.components.ProofTreeElementCell;
import invismt.events.ChangeHistoryEvent;
import invismt.events.CheckScriptsEvent;
import invismt.events.CleanUpEvent;
import invismt.events.ExecuteRuleEvent;
import invismt.events.RuleRequestedEvent;
import invismt.events.SetActiveProofObligationEvent;
import invismt.events.SetPTElementEvent;
import invismt.events.StartSolverEvent;
import invismt.events.StopSolverEvent;
import invismt.events.UpdateSolverStateEvent;
import invismt.exception.MissingUserInputException;
import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.rule.Rule;
import invismt.rule.RuleApplyCommand;
import invismt.solver.AnswerType;
import invismt.solver.Solver;
import invismt.solver.SolverAnswer;
import invismt.solver.SolverService;
import invismt.solver.SolverSucceededCommand;
import invismt.tree.PTElement;
import invismt.tree.ProofObligation;
import invismt.tree.ProofTree;
import invismt.tree.TreeCommand;
import invismt.tree.exception.CommandFailedToExecute;
import invismt.tree.exception.ConflictingStateException;
import invismt.tree.exception.PTElementLocked;
import invismt.util.NoSelectionModel;
import invismt.util.ScriptPrinter;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeView;
import javafx.util.Pair;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Controller of ProofTreeView.
 */
public class ProofTreeController implements EventDrivenController, PropertyChangeListener {

	private static final Logger LOGGER = Logger.getLogger(ProofTreeController.class.getName());

	private EventBus eventBus;

	@FXML
	private TabPane tabPane;
	@FXML
	private ListView<Pair<TreeCommand, Boolean>> history;
	@FXML
	private TreeView<PTElement> treeView;
	private ProofObligation activeProofObligation;

	@FXML
	private void initialize() {
		// initialize history
		this.history.setSelectionModel(new NoSelectionModel<>());
		this.history.setCellFactory(view -> new ListCell<Pair<TreeCommand, Boolean>>() {
			@Override
			protected void updateItem(Pair<TreeCommand, Boolean> item, boolean empty) {
				super.updateItem(item, empty);
				if (item != null && !empty) {
					setText(item.getKey().toString());
					if (!Boolean.TRUE.equals(item.getValue())) {
						// Reset the list cell's style to standard, thus undo any highlighting
						setStyle("");
					} else {
						// Highlight the list cell
						setStyle("-fx-background-color: -color-highlight;");
					}
				} else {
					setStyle("");
					setText(null);
					setGraphic(null);
				}
			}
		});
		this.updateHistory();
		this.setEventBus(App.getEventBus());
	}

	/**
	 * Updates the history to represent the current state.
	 */
	private void updateHistory() {
		if (this.activeProofObligation == null) {
			return;
		}
		List<Pair<TreeCommand, Boolean>> commands = new ArrayList<>();
		// add done commands to history
		List<TreeCommand> commandDoneStack = new ArrayList<>(
				this.activeProofObligation.getTreeCommandHistory().getDone());
		commandDoneStack = Lists.reverse(commandDoneStack);
		for (TreeCommand c : commandDoneStack) {
			commands.add(new Pair<>(c, true));
		}
		// add undone commands to history in reverse order
		List<TreeCommand> commandUndoStack = new ArrayList<>(
				this.activeProofObligation.getTreeCommandHistory().getUndo());
		for (TreeCommand c : commandUndoStack) {
			commands.add(new Pair<>(c, false));
		}
		this.history.setItems(FXCollections.observableArrayList(commands));
	}

	/**
	 * Handles a change to history.
	 *
	 * @param evt is the Event for changing the history
	 */
	@Subscribe
	public void handleChangeHistoryEvent(ChangeHistoryEvent evt) {
		switch (evt.getType()) {
		case EXECUTE:
			execute(evt.getCommand());
			break;
		case UNDO:
			undo();
			break;
		case REDO:
			redo();
			break;
		default:
			eventBus.post(new DeadEvent(this, evt));
		}
		eventBus.post(new UpdateSolverStateEvent(this.activeProofObligation.getProofTree().getActivePTElement()));
	}

	private void execute(TreeCommand command) {
		try {
			this.activeProofObligation.executeCommand(command);
		} catch (CommandFailedToExecute e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Could not execute: " + e);
		}
	}

	private void undo() {
		try {
			this.activeProofObligation.undo();
		} catch (CommandFailedToExecute e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Could not undo: " + e);
		}
	}

	private void redo() {
		try {
			this.activeProofObligation.redo();
		} catch (CommandFailedToExecute e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Could not redo: " + e);
		}
	}

	@Override
	public void setEventBus(EventBus eventBus) {
		eventBus.register(this);
		this.eventBus = eventBus;
	}

	/**
	 * Builds the view of the proof tree.
	 */
	private void buildTree() {
		PTElement rootElement = activeProofObligation.getProofTree().getRootPTElement();
		treeView.setCellFactory(treeview -> new ProofTreeElementCell(this::handleMouseClick,
				element -> eventBus.post(new StartSolverEvent(element)), this::stopSolver));
		treeView.setRoot(createTreeHierarchy(rootElement));
		treeView.setSelectionModel(new NoSelectionModel<>());
	}

	/**
	 * Creates a {@link PTTreeItem} tree from the actual proof tree.
	 *
	 * @param rootElement
	 * @return
	 */
	private PTTreeItem createTreeHierarchy(PTElement rootElement) {
		PTTreeItem root = new PTTreeItem(rootElement, treeView);
		// default expanded
		root.setExpanded(true);
		for (PTElement childElement : rootElement.getChildren()) {
			// Recursively call createTreeHierarchy on item
			root.getChildren().add(createTreeHierarchy(childElement));
		}

		return root;
	}

	private void setActiveProofObligation(ProofObligation activeProofObligation) {
		// remove old listener
		removeListeners();
		this.activeProofObligation = activeProofObligation;
		buildTree();
		eventBus.post(new SetPTElementEvent(activeProofObligation.getProofTree().getActivePTElement()));
		// Set Listener
		this.activeProofObligation.addChangeListener(this);
		this.activeProofObligation.getProofTree().addChangeListener(this);
		updateHistory();
	}

	private void removeListeners() {
		if (this.activeProofObligation != null) {
			this.activeProofObligation.rmChangeListener(this);
			this.activeProofObligation.getProofTree().rmChangeListener(this);
			((PTTreeItem) this.treeView.getRoot()).cleanUp();
		}
	}

	/**
	 * Handles a change in the active {@link ProofObligation}.
	 *
	 * @param evt is the event to change active {@link ProofObligation}
	 */
	@Subscribe
	public void handleSetActiveProofObligationEvent(SetActiveProofObligationEvent evt) {
		setActiveProofObligation(evt.getObligation());
	}

	/**
	 * @param evt {@link SetPTElementEvent} that is handled by this method
	 */
	@Subscribe
	public void handlePTElementSetEvent(SetPTElementEvent evt) {
		eventBus.post(new UpdateSolverStateEvent(this.activeProofObligation.getProofTree().getActivePTElement()));
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals(ProofTree.class.getName())) {
			eventBus.post(new SetPTElementEvent(this.activeProofObligation.getProofTree().getActivePTElement()));
		}
		this.updateHistory();
	}

	/**
	 * Handles a Rule Request
	 *
	 * @param event is the Event for a Rule Request
	 */
	@Subscribe
	public void handleRuleRequest(RuleRequestedEvent event) {
		Rule rule;
		try {
			rule = event.getRuleWrapper().createRule(event.getPTElement().getScript(), event.getExpression());
		} catch (MissingUserInputException e) {
			// ignore
			return;
		} catch (IllegalArgumentException e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, e.getMessage());
			return;
		}
		RuleApplyCommand command = new RuleApplyCommand(rule, event.getPTElement());
		this.eventBus.post(new ExecuteRuleEvent(command, activeProofObligation));
	}

	/**
	 * Handles the event that a PTElement has been clicked.
	 *
	 * @param ptElement on which was Clicked
	 */
	private void handleMouseClick(PTElement ptElement) {
		activeProofObligation.getProofTree().setActivePTElement(ptElement);
		eventBus.post(new SetPTElementEvent(activeProofObligation.getProofTree().getActivePTElement()));
		eventBus.post(new UpdateSolverStateEvent(this.activeProofObligation.getProofTree().getActivePTElement()));
	}

	private void stopSolver() {
		this.stopSolver(this.activeProofObligation.getProofTree().getActivePTElement());
	}

	private void stopSolver(PTElement item) {
		try {
			item.unlockAbort();
		} catch (ConflictingStateException e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, e.getMessage());
		}
		eventBus.post(new UpdateSolverStateEvent(this.activeProofObligation.getProofTree().getActivePTElement()));
	}

	/**
	 * Handles a request to stop a solver.
	 *
	 * @param evt is the Event to Stop the Solver
	 */
	@Subscribe
	public void handleStopSolverEvent(StopSolverEvent evt) {
		if (evt.getElement() == null) {
			stopSolver();
		} else {
			stopSolver(evt.getElement());
		}
	}

	/**
	 * Handles a request to start a solver.
	 *
	 * @param evt is the Event to Start the Solver
	 */
	@Subscribe
	public void handleStartSolverEvent(StartSolverEvent evt) {
		// ignore events which are not completely configured
		if (evt.getSolver() == null) {
			return;
		}
		startSolver(evt.getSolver(), evt.getElement());
	}

	private void startSolver(Solver solver, PTElement elementToProof) {
		// use active element if elementToProof is null
		PTElement element;
		if (elementToProof == null) {
			element = this.activeProofObligation.getProofTree().getActivePTElement();
		} else {
			element = elementToProof;
		}
		// dump script to file
		Script script = element.getScript();
		Expression last = script.getTopLevelExpressions().get(script.getTopLevelExpressions().size() - 1);
		script = script.addTopLevelListInstead(last, List.of(last.deepCopy(), new SyntaxCommand.CheckSat()));
		File file = ScriptPrinter.createTempSolverFile(script);
		// configure service
		SolverService service = new SolverService(file, solver);
		service.setOnSucceeded(event -> {
			solverFinished(service, element);
			eventBus.post(new UpdateSolverStateEvent(activeProofObligation.getProofTree().getActivePTElement()));
			deleteFile(file);
		});
		service.setOnFailed(event -> {
			solverFailed(service, element);
			eventBus.post(new UpdateSolverStateEvent(activeProofObligation.getProofTree().getActivePTElement()));
			deleteFile(file);
		});
		service.setOnCancelled(event -> {
			deleteFile(file);
			eventBus.post(new UpdateSolverStateEvent(activeProofObligation.getProofTree().getActivePTElement()));
		});
		// lock PTElement
		try {
			element.lock(service);
		} catch (PTElementLocked e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "A solver has already been started on this element.");
		}
		eventBus.post(new UpdateSolverStateEvent(this.activeProofObligation.getProofTree().getActivePTElement()));
	}

	private void solverFinished(SolverService service, PTElement element) {
		SolverAnswer answer = service.getValue();
		if (answer == null) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Unexpected answer: " + answer);
			try {
				element.unlockAbort();
			} catch (ConflictingStateException e) {
				Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, e.getMessage());
			}
			return;
		}
		if (answer.getType() == AnswerType.ERROR) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Solver output: " + answer.getMessage());
			try {
				element.unlockAbort();
			} catch (ConflictingStateException e) {
				Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, e.getMessage());
			}
			return;
		} else if (answer.getType() == AnswerType.INFO) {
			Alerts.showAlertAndLog(LOGGER, AlertType.INFORMATION, "Solver output: " + answer.getMessage());
			try {
				element.unlockAbort();
			} catch (ConflictingStateException e) {
				Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, e.getMessage());
			}
			return;
		}
		try {
			this.activeProofObligation.executeCommand(new SolverSucceededCommand(answer, element));
		} catch (CommandFailedToExecute e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Command Failed to execute: " + e.getMessage());
		}
	}

	private void solverFailed(SolverService service, PTElement element) {
		Throwable exception = service.getException();
		if (exception == null) {
			Alert alert = new Alert(AlertType.ERROR, "An unknown error in the solver process has occured.");
			alert.show();
		} else {
			Alert alert = new Alert(AlertType.ERROR, "Solver failed: " + exception.getMessage());
			alert.show();
		}
		try {
			element.unlockAbort();
		} catch (ConflictingStateException e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, e.getMessage());
		}
	}

	private void deleteFile(File file) {
		// moved to solver, stub here to not forget possibly changing the behaviour in
		// the future
	}

	/**
	 * Handles a {@link CleanUpEvent}.
	 *
	 * @param event the {@link CleanUpEvent} that is to be handled.
	 */
	@Subscribe
	public void handleCleanUp(CleanUpEvent event) {
		if (activeProofObligation != null) {
			PTElement root = this.activeProofObligation.getProofTree().getRootPTElement();
			abortAll(root);
			removeListeners();
		}
	}

	private void abortAll(PTElement element) {
		try {
			element.unlockAbort();
		} catch (ConflictingStateException e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, e.getMessage());
		}
		for (PTElement child : element.getChildren()) {
			abortAll(child);
		}
	}

	/**
	 * Check the currently selected script when asked to.
	 * @param evt
	 */
	@Subscribe
	public void handleCheckScriptsEvent(CheckScriptsEvent evt) {
		ScriptChecker verifier = new ScriptChecker(evt.getSolver(),
				List.of(this.activeProofObligation.getProofTree().getActivePTElement().getScript()), null,
				reason -> Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, reason));
		verifier.check();
	}
}
