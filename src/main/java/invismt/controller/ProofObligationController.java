/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.controller;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import invismt.App;
import invismt.components.ProofObligationListCell;
import invismt.events.SetActiveProofObligationEvent;
import invismt.events.SetListOfProofObligationEvent;
import invismt.tree.ListOfProofObligation;
import invismt.tree.ProofObligation;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.skin.SplitPaneSkin;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Controller of ProofObligationView.
 */
public class ProofObligationController implements EventDrivenController, PropertyChangeListener {

	private EventBus eventBus;
	private ListOfProofObligation listOfProofObligations;

	@FXML
	private ListView<ProofObligation> proofObligationList;
	@FXML
	private SplitPane splitPane;

	@Override
	public void setEventBus(EventBus eventBus) {
		eventBus.register(this);
		this.eventBus = eventBus;
	}

	/**
	 * Initializes this controller.
	 */
	@FXML
	private void initialize() {
		splitPane.setDividerPositions(0.2);
		splitPane.setSkin(new ProofObligationSplitPaneSkin(splitPane));
		proofObligationList.setCellFactory(view -> new ProofObligationListCell(this::handleMouseClick));
		this.setEventBus(App.getEventBus());
	}

	private void setListOfProofObligations(ListOfProofObligation listOfProofObligations) {
		this.listOfProofObligations = listOfProofObligations;
		this.listOfProofObligations.addChangeListener(this);
		proofObligationList.setItems(FXCollections.observableList(listOfProofObligations.getProofObligations()));
		// Select first proof obligation in list
		proofObligationList.getSelectionModel().select(0);
		eventBus.post(new SetActiveProofObligationEvent(listOfProofObligations.getActiveProofObligation()));
	}

	/**
	 * Handles an event setting the {@link ListOfProofObligation}
	 *
	 * @param evt is the Event to set {@link ListOfProofObligation}
	 */
	@Subscribe
	public void handleSetListOfProofObligationEvent(SetListOfProofObligationEvent evt) {
		this.setListOfProofObligations(evt.getList());
	}

	private void handleMouseClick(ProofObligation proofObligation) {
		this.listOfProofObligations.setActiveProofObligation(proofObligation);
		eventBus.post(new SetActiveProofObligationEvent(listOfProofObligations.getActiveProofObligation()));
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		proofObligationList.setItems(FXCollections.observableList(listOfProofObligations.getProofObligations()));
	}

	/**
	 * Auxiliary Class.
	 *
	 * @author Jonathan Hunz
	 */
	private static final class ProofObligationSplitPaneSkin extends SplitPaneSkin {

		private ProofObligationSplitPaneSkin(SplitPane control) {
			super(control);
		}

		@Override
		protected void layoutChildren(double x, double y, double w, double h) {
			double[] dividerPositions = getSkinnable().getDividerPositions();
			super.layoutChildren(x, y, w, h);
			getSkinnable().setDividerPositions(dividerPositions);
		}
	}
}
