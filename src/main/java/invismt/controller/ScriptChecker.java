/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.controller;

import invismt.grammar.Script;
import invismt.solver.AnswerType;
import invismt.solver.Solver;
import invismt.solver.SolverAnswer;
import invismt.solver.SolverService;
import invismt.util.ScriptPrinter;

import java.io.File;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class provides the functionality to check a list of {@link Script}. It
 * uses a given solver and checks whether or not the solver throws an error when
 * the scripts are given to it without any instruction to check for
 * satisfiability.
 *
 * @author Tim Junginger
 */
public class ScriptChecker {

	private static final Logger LOGGER = Logger.getLogger(ScriptChecker.class.getName());
	private final SolverService[] checker;
	private final File[] files;
	private final Solver solver;
	private final List<Script> scripts;
	private final Runnable success;
	private final Consumer<String> failed;
	private int finished;
	private String failedReason;

	/**
	 * Creates a new ScriptChecker with the given solver to check the given list of
	 * scripts. On success runs the given {@link Runnable} and on fail runs the
	 * given {@link Consumer} with the reason why it failed as argument.
	 *
	 * @param solver  Solver to check the scripts with
	 * @param scripts Scripts to check
	 * @param success Action to run when check is successful
	 * @param failed  Action to run when check failed, gets the reason why it failed
	 *                as argument
	 */
	public ScriptChecker(Solver solver, List<Script> scripts, Runnable success, Consumer<String> failed) {
		this.solver = solver;
		this.scripts = scripts;
		this.finished = scripts.size();
		this.checker = new SolverService[scripts.size()];
		this.files = new File[scripts.size()];
		this.success = success;
		this.failed = failed;
		this.failedReason = null;
	}

	/**
	 * Returns the reason why the script check failed if any.
	 *
	 * @return Reason or null if it was successful
	 */
	public String getFailedReason() {
		return this.failedReason;
	}

	private synchronized void revokeOk(String reason) {
		LOGGER.log(Level.FINE, "ScriptChecker revoked ok.");
		this.cleanUp();
		this.failedReason = "Script check failed: " + reason;
		this.failed.accept(this.failedReason);
	}

	private synchronized void checkOut() {
		LOGGER.log(Level.FINE, "ScriptChecker checked out.");
		this.finished--;
		if (finished <= 0 && this.success != null) {
			this.success.run();
		}
	}

	private void cleanUp() {
		for (SolverService service : this.checker) {
			service.cancel();
			// possibly wait for no longer running
		}
		for (File file : this.files) {
			// moved to solver, stub here to not forget possibly changing the behaviour in the future
			/*
			try {
				Files.delete(Paths.get(file.getAbsolutePath()));
				LOGGER.log(Level.FINE, "Deleted temp file {0} successfully.", file.getAbsolutePath());
			} catch (IOException e) {
				Alerts.showAlertAndLog(LOGGER, AlertType.WARNING, "Could not delete temp file " + file.getAbsolutePath()
						+ ". If this happens often your temp folder could be flooded with files. "
						+ "Check your system permissions.");
			}
			*/
		}
	}

	/**
	 * Starts the script check.
	 */
	public void check() {
		LOGGER.log(Level.FINE, "ScriptChecker started with {0} scripts to check.", this.finished);
		int i = 0;
		for (Script s : scripts) {
			files[i] = ScriptPrinter.createTempSolverFile(s);
			checker[i] = new SolverService(files[i], this.solver);
			checker[i].setOnSucceeded(event -> {
				SolverAnswer answer = (SolverAnswer) event.getSource().getValue();
				if (answer.getType() == AnswerType.ERROR) {
					revokeOk(answer.getMessage());
				} else {
					checkOut();
				}
			});
			checker[i].setOnFailed(event -> {
				Throwable e = event.getSource().getException();
				revokeOk((e == null) ? "An unknown error has occured while checking:" : e.getMessage());
			});
			checker[i].setOnCancelled(event -> checkOut());
			checker[i].start();
			i++;
		}
	}

}
