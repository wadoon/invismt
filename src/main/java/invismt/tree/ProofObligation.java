/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;

import invismt.grammar.Script;
import invismt.rule.CreationRule;
import invismt.tree.exception.CommandFailedToExecute;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an obligation that is to be proven or disproven. This obligation
 * can be altered via {@link TreeCommand}'s.
 *
 * @see TreeCommand
 * @see TreeCommandHistory
 */
public class ProofObligation implements PropertyChangeListener {
	private final TreeCommandHistory treeCommandHistory;
	private final ProofTree proofTree;

	private final List<PropertyChangeListener> listeners = new ArrayList<>();

	/**
	 * Creates a new proof obligation based on the given script.
	 *
	 * @param script script which will be the root state
	 */
	public ProofObligation(Script script) {
		treeCommandHistory = new TreeCommandHistory();
		PTElement root;
		// is this if statement necessary? is it possible to have a script with no expressions?
		if (script == null || script.getTopLevelExpressions().isEmpty()) {
			root = new PTElement(null, script, null, null);
		} else {
			root = new PTElement(null, script, new CreationRule(script, script.getTopLevelExpressions().get(0)), null);
		}
		proofTree = new ProofTree(root);
		root.setProofTree(proofTree);
		this.proofTree.addChangeListener(this);
	}

	private void notifyListeners() {
		for (PropertyChangeListener listener : listeners) {
			listener.propertyChange(new PropertyChangeEvent(this, ProofObligation.class.getName(), null, this));
		}
	}

	/**
	 * Adds a listener to listen when this obligation undergoes a change.
	 *
	 * @param newListener Listener to add
	 */
	public void addChangeListener(PropertyChangeListener newListener) {
		listeners.add(newListener);
	}

	/**
	 * Removes the given listener.
	 *
	 * @param rmListener Listener to remove
	 */
	public void rmChangeListener(PropertyChangeListener rmListener) {
		listeners.remove(rmListener);
	}

	/**
	 * Undo's one step represented by {@link TreeCommand} on this obligation.
	 *
	 * @throws CommandFailedToExecute when the TreeCommand fails to be undone
	 */
	public void undo() throws CommandFailedToExecute {
		treeCommandHistory.undo();
		notifyListeners();
	}

	/**
	 * Redo's one step represented by {@link TreeCommand} on this obligation.
	 *
	 * @throws CommandFailedToExecute when the TreeCommand fails to be redone
	 */
	public void redo() throws CommandFailedToExecute {
		treeCommandHistory.redo();
		notifyListeners();
	}

	/**
	 * Executes the given {@link TreeCommand} and saves in a
	 * {@link TreeCommandHistory}.
	 *
	 * @param treeCommand Command to execute
	 * @throws CommandFailedToExecute when the TreeCommand fails to be executed
	 */
	public void executeCommand(TreeCommand treeCommand) throws CommandFailedToExecute {
		treeCommandHistory.addCommand(treeCommand);
		notifyListeners();
	}

	/**
	 * Gets the {@link TreeCommandHistory} of this obligation.
	 *
	 * @return History of commands
	 */
	public TreeCommandHistory getTreeCommandHistory() {
		return treeCommandHistory;
	}

	/**
	 * Gets the {@link ProofTree} of this obligation.
	 *
	 * @return proof tree
	 */
	public ProofTree getProofTree() {
		return proofTree;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		notifyListeners();
	}
}
