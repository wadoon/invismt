/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree.exception;

/**
 * Exception when a {@link TreeCommand} could not be executed.
 */
@SuppressWarnings("serial")
public class CommandFailedToExecute extends Exception {

	/**
	 * Calls {@link Exception#Exception()}.
	 */
	public CommandFailedToExecute() {
		super();
	}

	/**
	 * Calls {@link Exception#Exception(String)}.
	 *
	 * @param message is the message for the Exception
	 */
	public CommandFailedToExecute(String message) {
		super(message);
	}
}
