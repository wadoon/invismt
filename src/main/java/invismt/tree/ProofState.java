/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/

package invismt.tree;

import java.util.List;

/**
 * Represents the satisfiability of a PTElement.
 */
public enum ProofState {

	/**
	 * Satisfiable
	 */
	SAT(CausedBy.EXTERNAL) {
		@Override
		public boolean compare(ProofState other) {
			return sat.contains(other);
		}
	},
	/**
	 * Unsatisfiable
	 */
	UNSAT(CausedBy.EXTERNAL) {
		@Override
		public boolean compare(ProofState other) {
			return unsat.contains(other);
		}
	},
	/**
	 * Unknown, should be of the highest priority according to its CausedBy value.
	 */
	UNKNOWN(CausedBy.EXTERNAL) {
		@Override
		public boolean compare(ProofState other) {
			return other == UNKNOWN;
		}

		@Override
		public boolean conflicts(ProofState other) {
			return false;
		}
	},
	/**
	 * Satisfiable, caused internally
	 */
	INTERNAL_SAT(CausedBy.INTERNAL) {
		@Override
		public boolean compare(ProofState other) {
			return sat.contains(other);
		}

		@Override
		public String toString() {
			return SAT + INTERNAL;
		}
	},
	/**
	 * Unsatisfiable, caused internally
	 */
	INTERNAL_UNSAT(CausedBy.INTERNAL) {
		@Override
		public boolean compare(ProofState other) {
			return unsat.contains(other);
		}

		@Override
		public String toString() {
			return UNSAT + INTERNAL;
		}
	},;

	static final List<ProofState> sat = List.of(SAT, INTERNAL_SAT);
	static final List<ProofState> unsat = List.of(UNSAT, INTERNAL_UNSAT);
	static final String INTERNAL = "+";

	private CausedBy causedBy;

	ProofState(CausedBy causedBy) {
		this.causedBy = causedBy;
	}

	/**
	 * @return {@link CausedBy reason} that caused this proof state
	 */
	public CausedBy causedBy() {
		return causedBy;
	}

	/**
	 * Compare the satisfiability value of one proof state to another one,
	 * e.g. {@link ProofState#INTERNAL_SAT} and {@link ProofState#SAT}
	 * both represent satisfiability.
	 *
	 * @param other the proof state to compare the one at hand to
	 * @return whether the two proof states represent the same satisfiability status
	 */
	public abstract boolean compare(ProofState other);

	/**
	 * Check whether two proof states are conflicting.
	 *
	 * @param other the proof state to compare the one at hand to
	 * @return whether the two proof states are conflicting
	 */
	public boolean conflicts(ProofState other) {
		return (other != UNKNOWN && !compare(other));
	}

	/**
	 * An enum marking the reason for a proof state.
	 * For exemplary use {@see {@link PTElement}}.
	 */
	public enum CausedBy {

		/**
		 * Used for internally caused proof states.
		 */
		INTERNAL(List.of()),

		/**
		 * Used for externally caused proof states.
		 */
		EXTERNAL(List.of(INTERNAL));

		private List<CausedBy> prioritized;

		CausedBy(List<CausedBy> prioritized) {
			this.prioritized = prioritized;
		}

		public boolean prioritizes(CausedBy other) {
			return prioritized.contains(other);
		}

	}


}
