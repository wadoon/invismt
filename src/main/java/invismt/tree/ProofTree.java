/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Tree of proof states.
 */
public class ProofTree {
	private final List<PropertyChangeListener> listeners = new ArrayList<>();
	private PTElement activePTElement;
	private PTElement rootPTElement;

	/**
	 * Creates a new tree with the given root element.
	 *
	 * @param root root of the tree is set to active node
	 */
	public ProofTree(PTElement root) {
		this.activePTElement = root;
		this.rootPTElement = root;
	}

	/**
	 * Gets the active node of this tree. Only one node can be active at a time.
	 *
	 * @return activeLeaf
	 */
	public PTElement getActivePTElement() {
		return activePTElement;
	}

	/**
	 * Sets the active node. Only one node can be active at a time.
	 *
	 * @param activePTElement Node to set active. This node has to be part of this
	 *                        tree.
	 */
	public void setActivePTElement(PTElement activePTElement) {
		// Check if activePTElement is in the Tree
		PTElement parent = activePTElement;
		while (parent.getParent() != null) {
			parent = parent.getParent();
		}
		if (parent.equals(rootPTElement)) {
			this.activePTElement.notifyListeners();
			this.activePTElement = activePTElement;
			this.activePTElement.notifyListeners();
		}
		notifyListeners();
	}

	/**
	 * Gets the root node of this tree.
	 *
	 * @return Root root of this tree.
	 */
	public PTElement getRootPTElement() {
		return rootPTElement;
	}

	/**
	 * Sets the root node of this tree. This basically creates a new tree.
	 *
	 * @param rootPTElement root to set
	 */
	public void setRootPTElement(PTElement rootPTElement) {
		this.rootPTElement = rootPTElement;
		this.activePTElement = rootPTElement;
		notifyListeners();
	}

	/**
	 * Checks the currently active node for correctness.
	 * If the active node would not be in this tree a new node is selected as active.
	 * This method is internally already called, but might be called from outside when this tree is changed.
	 */
	public void checkActive() {
		PTElement parent = activePTElement;
		while (parent.getParent() != null) {
			if (!parent.getParent().getChildren().contains(parent)) {
				activePTElement = parent.getParent();
				notifyListeners();
				return;
			}
			parent = parent.getParent();
		}
		if (parent != rootPTElement) {
			activePTElement = rootPTElement;
		}
		notifyListeners();
	}

	private void notifyListeners() {
		for (PropertyChangeListener listener : listeners) {
			listener.propertyChange(new PropertyChangeEvent(this, ProofTree.class.getName(), null, this));
		}
	}

	/**
	 * Adds a listener to listen when this obligation undergoes a change.
	 *
	 * @param newListener Listener to add
	 */
	public void addChangeListener(PropertyChangeListener newListener) {
		listeners.add(newListener);
	}

	/**
	 * Removes the given listener.
	 *
	 * @param rmListener Listener to remove
	 */
	public void rmChangeListener(PropertyChangeListener rmListener) {
		listeners.remove(rmListener);
	}
}
