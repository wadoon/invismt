/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds a set (list) of {@link ProofObligation} and saves one currently
 * selected.
 */
public class ListOfProofObligation implements PropertyChangeListener {
	private final List<ProofObligation> proofObligations;
	private final List<PropertyChangeListener> listeners = new ArrayList<>();
	private ProofObligation activeProofObligation;

	/**
	 * Creates a new ListOfProofObligation from the given list.
	 *
	 * @param proofObligations List of {@link ProofObligation} and sets the first to
	 *                         active if any
	 */
	public ListOfProofObligation(List<ProofObligation> proofObligations) {
		if (proofObligations == null) {
			throw new IllegalArgumentException("Null is not a valid argument.");
		} else if (proofObligations.isEmpty()) {
			throw new IllegalArgumentException("An empty list is not a valid argument.");
		}
		this.proofObligations = proofObligations;
		activeProofObligation = proofObligations.get(0);
		for (ProofObligation obl : proofObligations) {
			obl.addChangeListener(this);
		}
	}

	/**
	 * Gets the active obligation.
	 *
	 * @return activeProofObligation
	 */
	public ProofObligation getActiveProofObligation() {
		return activeProofObligation;
	}

	/**
	 * Sets the active obligation.
	 *
	 * @param activeProofObligation to set active to
	 */
	public void setActiveProofObligation(ProofObligation activeProofObligation) {
		this.activeProofObligation = activeProofObligation;
	}

	/**
	 * Gets all obligations in this list.
	 *
	 * @return List of {@link ProofObligation}
	 */
	public List<ProofObligation> getProofObligations() {
		return proofObligations;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		notifyListeners();
	}

	private void notifyListeners() {
		for (PropertyChangeListener listener : listeners) {
			listener.propertyChange(new PropertyChangeEvent(this, ProofTree.class.getName(), null, this));
		}
	}

	/**
	 * Adds a listener.
	 *
	 * @param newListener Listener to add
	 */
	public void addChangeListener(PropertyChangeListener newListener) {
		listeners.add(newListener);
	}

	/**
	 * Removes the given listener.
	 *
	 * @param rmListener Listener to remove
	 */
	public void rmChangeListener(PropertyChangeListener rmListener) {
		listeners.remove(rmListener);
	}
}
