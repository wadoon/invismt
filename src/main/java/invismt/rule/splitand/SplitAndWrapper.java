/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.splitand;

import invismt.exception.MissingUserInputException;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.Term;
import invismt.rule.CheckConstruct;
import invismt.rule.Rule;
import invismt.rule.RuleWrapper;
import javafx.scene.control.ChoiceDialog;

import java.util.List;
import java.util.Optional;

/**
 * {@link RuleWrapper} for the {@link SplitAndRule}.
 *
 * @author Tim Junginger
 */
public class SplitAndWrapper implements RuleWrapper {

	private static final String DIALOG_HEADER_TEXT = "SplitAnd";
	private static final String DIALOG_TEXT = "Select term to split:";

	@Override
	public String getName() {
		return SplitAndRule.RULE_NAME;
	}

	@Override
	public Rule createRule(Script script, Expression expr) {

		IdentifierTerm andTerm = expr.accept(new CheckConstruct<>() {
			@Override
			public IdentifierTerm visit(IdentifierTerm term) {
				return term;
			}
		});
		if (andTerm == null) {
			throw new IllegalArgumentException();
		}
		IdentifierTerm term = (IdentifierTerm) expr;
		List<Term> andTerms = term.getIdentifiedTerms();
		if (andTerms.size() == 2) {
			return new SplitAndRule(script, term, andTerms.get(1));
		}
		ChoiceDialog<Term> dialog = new ChoiceDialog<>(andTerm.getIdentifiedTerms().get(0),
				andTerm.getIdentifiedTerms());
		dialog.setHeaderText(DIALOG_HEADER_TEXT);
		dialog.setContentText(DIALOG_TEXT);
		Optional<Term> mayTerm = dialog.showAndWait();
		if (mayTerm.isEmpty()) {
			throw new MissingUserInputException("no term selected");
		}
		return new SplitAndRule(script, andTerm, mayTerm.get());
	}

	@Override
	public boolean mayApply(Script script, Expression expr) {
		CheckConstruct<IdentifierTerm> termCheck = new CheckConstruct<>() {
			@Override
			public IdentifierTerm visit(IdentifierTerm term) {
				if (!script.existsIdenticallySomewhere(term)) {
					return null;
				}
				if (!script.getTopLevel(term).equals(new SyntaxCommand.Assert(term))) {
					return null;
				}
				if (term.getIdentifiedTerms().size() < 2) {
					return null;
				}
				if (!term.getIdentifier().equals(ExpressionFactory.AND)) {
					return null;
				}
				return term;
			}
		};
		return expr.accept(termCheck) != null;
	}

}
