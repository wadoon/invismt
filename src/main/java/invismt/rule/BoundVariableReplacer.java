/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionBaseVisitor;
import invismt.grammar.GeneralExpression;
import invismt.grammar.term.AnnotationTerm;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.LetTerm;
import invismt.grammar.term.MatchTerm;
import invismt.grammar.term.QuantifiedTerm;
import invismt.grammar.term.Term;
import invismt.grammar.term.VariableBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * A term replacer that replaces an old term by a new term regardless
 * of whether there would be bound variable occurrences replaced.
 *
 * @author Alicia
 */
public class BoundVariableReplacer implements TermReplacer {

	protected final Term oldT;
	protected final Term newT;
	protected final ExpressionComparator comparator;
	protected boolean replacementUnclear;
	protected BoundVariableReplacer.ReplacementVisitor replacer;

	/*
	The last visited term in {@link applicable(Term)} and its substitute to save
	runtime when calling {@link #applicable(Term)} and {@link #substitute(Term)} in a row.
	 */
	private Term lastVisited;
	private Term lastSubstitute;

	/**
	 * Constructor.
	 *
	 * @param oldTerm    the term that should be replaced.
	 * @param newTerm    the term that is should replace the old term.
	 * @param comparator a {@link ExpressionComparator} used to compare terms to the old term.
	 */
	public BoundVariableReplacer(Term oldTerm, Term newTerm, ExpressionComparator comparator) {
		oldT = oldTerm;
		newT = newTerm;
		this.comparator = comparator;
		replacementUnclear = false;
		lastVisited = null;
		lastSubstitute = null;
		replacer = new BoundVariableReplacer.ReplacementVisitor();
	}

	/*
	This replacer does not replace anything if a {@link MatchTerm} or an {@link AnnotationTerm}
	would be replaced in the process.
	 */

	/**
	 * @return true iff a replacement is applicable.
	 */
	protected boolean applicable() {
		return !replacementUnclear;
	}

	/**
	 * Replacement is always applicable unless the replaced term contains a
	 * {@link MatchTerm} or an {@link AnnotationTerm} that would be replaced
	 * in the replacement process.
	 *
	 * @param term the term which is to be replaced
	 * @return true if {@link #applicable()} is true after having tried to replace oldT by newT in the term
	 */
	@Override
	public boolean applicable(Term term) {
		lastSubstitute = (Term) term.accept(replacer);
		lastVisited = term;
		return !term.equals(lastSubstitute);
	}

	/**
	 * Returns the term that is equal to replacing equal or identical occurrences of {@link #oldT}
	 * by {@link #newT} in the given term.
	 * <p>
	 * Returns the term itself if nothing could be replaced according to the {@link #applicable(Term)}
	 * method.
	 *
	 * @param term the term to be replaced
	 * @return the replacement if {@link #applicable(Term)} is true, the term itself if not
	 */
	@Override
	public Term substitute(Term term) {
		if (applicable(term)) {
			return getSubstitute(term);
		}
		return term;
	}

	private Term getSubstitute(Term term) {
		if (term == lastVisited) {
			return lastSubstitute;
		}
		return (Term) term.accept(replacer);
	}

	/**
	 * Visitor used to replace expressions.
	 */
	protected class ReplacementVisitor extends ExpressionBaseVisitor<Expression> {

		@Override
		public GeneralExpression visit(GeneralExpression expression) {
			return expression;
		}

		@Override
		public Term visit(QuantifiedTerm.ForallTerm forallTerm) {
			// No replacement if one of the used variables in the old term is bound in the term
			if (!applicable()) {
				return forallTerm;
			}
			if (comparator.equals(forallTerm, oldT)) {
				return newT.deepCopy();
			}
			Term innerTermReplaced = (Term) forallTerm.getTerm().accept(this);
			if (innerTermReplaced == forallTerm.getTerm()) {
				return forallTerm;
			}
			return new QuantifiedTerm.ForallTerm(forallTerm.getVariables(), innerTermReplaced);
		}

		@Override
		public Term visit(QuantifiedTerm.ExistsTerm existsTerm) {
			// No replacement if one of the used variables in the old term is bound in the term
			if (!applicable()) {
				return existsTerm;
			}
			if (comparator.equals(existsTerm, oldT)) {
				return newT.deepCopy();
			}
			Term innerTermReplaced = (Term) existsTerm.getTerm().accept(this);
			if (innerTermReplaced == existsTerm.getTerm()) {
				return existsTerm;
			}
			return new QuantifiedTerm.ExistsTerm(existsTerm.getVariables(), innerTermReplaced);
		}

		@Override
		public Term visit(LetTerm letTerm) {
			// No replacement if one of the used variables in the old term is bound in the term
			if (!applicable()) {
				return letTerm;
			}
			if (comparator.equals(letTerm, oldT)) {
				return newT.deepCopy();
			}
			boolean replacedTerm = false;
			for (VariableBinding binding : letTerm.getBindings()) {
				Term newBindingTerm = (Term) binding.getBindingTerm().accept(this);
				if (newBindingTerm != binding.getBindingTerm()) {
					replacedTerm = true;
				}
			}
			if (!applicable()) {
				return letTerm;
			}
			Term innerTermReplaced = (Term) letTerm.getTerm().accept(this);
			if (innerTermReplaced == letTerm.getTerm() && !replacedTerm) {
				return letTerm;
			}
			return new LetTerm(letTerm.getBindings(), innerTermReplaced);
		}

		@Override
		public Term visit(AnnotationTerm annotationTerm) {
			if (!applicable()) {
				return annotationTerm;
			}
			if (comparator.equals(annotationTerm, oldT)) {
				replacementUnclear = true;
			}
			if (!((Term) annotationTerm.getTerm().accept(this)).equals(annotationTerm.getTerm())) {
				replacementUnclear = true;
			}
			return annotationTerm;
		}

		@Override
		public Term visit(MatchTerm matchTerm) {
			if (!applicable()) {
				return matchTerm;
			}
			if (comparator.equals(matchTerm, oldT)) {
				replacementUnclear = true;
			}
			if (!((Term) matchTerm.getTerm().accept(this)).equals(matchTerm.getTerm())) {
				replacementUnclear = true;
			}
			return matchTerm;
		}

		@Override
		public Term visit(ConstantTerm constantTerm) {
			if (!applicable()) {
				return constantTerm;
			}
			if (comparator.equals(constantTerm, oldT)) {
				return newT.deepCopy();
			}
			return constantTerm;
		}

		@Override
		public Term visit(IdentifierTerm identifierTerm) {
			if (!applicable()) {
				return identifierTerm;
			}
			if (comparator.equals(identifierTerm, oldT)) {
				return newT.deepCopy();
			}
			List<Term> replacedTerms = new ArrayList<>();
			for (Term term : identifierTerm.getIdentifiedTerms()) {
				replacedTerms.add((Term) term.accept(this));
			}
			if (replacedTerms.equals(identifierTerm.getIdentifiedTerms())) {
				return identifierTerm;
			}
			return new IdentifierTerm(identifierTerm.getIdentifier(), replacedTerms);
		}

	}

}
