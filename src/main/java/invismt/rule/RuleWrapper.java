/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.exception.MissingUserInputException;
import invismt.grammar.Expression;
import invismt.grammar.Script;

/**
 * @author Tim Junginger
 */
public interface RuleWrapper {

	/**
	 * @return name of the rule represented by this wrapper
	 */
	String getName();

	/**
	 * @param script the {@link Script} on which the rule is supposed to be applied on.
	 * @param expr   an {@link Expression} on which the rule is supposed to be applied on.
	 * @return a {@link Rule} that can by applied on the given expression
	 * might return null, if such a rule could not be created
	 * @throws IllegalArgumentException  if illegal or inappropriate arguments were passed.
	 * @throws MissingUserInputException if a user input was expected but non was given.
	 */
	Rule createRule(Script script, Expression expr) throws IllegalArgumentException, MissingUserInputException;

	/**
	 * Every rule extending the {@link Rule} class is applied on a Script and an expression
	 * in that script, potentially with additional parameters.
	 * This method checks whether the rule represented by the rule wrapper at hand can be applied
	 * on the given expression and the given script syntactically, regardless of the need of additional
	 * parameters.
	 *
	 * @param script the script that the corresponding rule is to be applied to
	 * @param expr   the expression that the corresponding rule is to be applied to
	 * @return whether or not the corresponding rule may be applied to the given parameters
	 */
	boolean mayApply(Script script, Expression expr);

}
