/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.doublenegation;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.Term;
import invismt.rule.BoundVariableReplacer;
import invismt.rule.CannotApplyRuleException;
import invismt.rule.CheckConstruct;
import invismt.rule.ExpressionComparator;
import invismt.rule.Rule;
import invismt.rule.TermReplacer;

import java.util.List;
import java.util.ResourceBundle;

/**
 * Removes double negations.
 */
public class DoubleNegationRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "DoubleNegation";
	private static final boolean CONFLUENT = true;
	private final ResourceBundle errorMessages;
	IdentifierTerm innerTerm;
	Assert top;

	/**
	 * Constructor.
	 *
	 * @param script      the {@link Script} on which this rule is supposed to be applied on.
	 * @param negatedTerm the {@link IdentifierTerm} that represents the double negation.
	 */
	public DoubleNegationRule(Script script, IdentifierTerm negatedTerm) {
		super(RULE_NAME, CONFLUENT, script, negatedTerm);
		errorMessages = ResourceBundle.getBundle("i18n.ErrorMessages");
		checkParams();
		generateNewStates();
	}

	private void checkParams() {
		String errorMessage = errorMessages.getString("cannotApplyRule.doubleNegation");

		// Check whether term is somewhere in an assertion
		Expression toplevelExpression = script.getTopLevel(expression);
		top = toplevelExpression.accept(new CheckConstruct<>() {
			@Override
			public Assert visit(Assert assertion) {
				return assertion;
			}
		});
		if (top == null) {
			throw CannotApplyRuleException.notInAssertion(this, (Term) expression);
		}

		// Check whether term contains two "not"s in a row
		IdentifierTerm term = (IdentifierTerm) expression;
		if (term.getIdentifier().equals(ExpressionFactory.NOT) && term.getIdentifiedTerms().size() == 1) {
			innerTerm = term.getIdentifiedTerms().get(0).accept(new CheckConstruct<>() {
				@Override
				public IdentifierTerm visit(IdentifierTerm term) {
					if (term.getIdentifier().equals(ExpressionFactory.NOT) && term.getIdentifiedTerms().size() == 1) {
						return term;
					}
					throw new CannotApplyRuleException(errorMessage);
				}
			});
		} else {
			throw new CannotApplyRuleException(errorMessage);
		}
		if (innerTerm == null) {
			throw new CannotApplyRuleException(errorMessage);
		}
	}

	private void generateNewStates() {
		Term term = (Term) expression;
		TermReplacer replacer = new BoundVariableReplacer(term, innerTerm.getIdentifiedTerms().get(0),
				new ExpressionComparator.IdenticalComparator());
		Assert newTop = new Assert(replacer.substitute(top.getTerm()));
		createdScripts.add(script.addTopLevelListInstead(term, List.of(newTop)));
	}

}
