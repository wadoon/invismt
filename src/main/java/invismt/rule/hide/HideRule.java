/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.hide;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.AnnotationTerm;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import invismt.rule.CheckConstruct;
import invismt.rule.Rule;

import java.util.ArrayList;

/**
 * An inference rule that removes an assertion from a script.
 * <p>
 * The rule does not check the script for semantic correctness after the rule has been applied: e.g. removing an
 * {@link invismt.grammar.term.AnnotationTerm} which introduces a new variable name will lead to an incorrect script.
 *
 * @author Alicia
 */
public class HideRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "Hide";
	private static final boolean CONFLUENT = false;

	/**
	 * Constructor.
	 *
	 * @param script the {@link Script} on which this rule is supposed to be applied on.
	 * @param term   the {@link Term} on which this rule should be applied.
	 */
	public HideRule(Script script, Term term) {
		super(RULE_NAME, CONFLUENT, script, term);
		checkParams();
		createdScripts.add(script.addTopLevelListInstead(term, new ArrayList<>()));
	}

	private void checkParams() {

		Term term = (Term) expression;
		CheckConstruct<AnnotationTerm> annotationCheck = new CheckConstruct<>() {
			@Override
			public AnnotationTerm visit(AnnotationTerm term) {
				return term;
			}
		};
		CheckConstruct<SyntaxCommand.Assert> assertionCheck = new CheckConstruct<>() {
			@Override
			public SyntaxCommand.Assert visit(SyntaxCommand.Assert assertion) {
				return assertion;
			}
		};

		// Check whether the expression is a top level term in an assertion
		if (!script.getTopLevel(term).equals(new Assert(term))) {
			throw CannotApplyRuleException.notTopLevelTerm(this, term);
		}

		// Check whether there is an annotation term contained in the expression at hand
		for (Expression expr : term.getChildrenOfChildren()) {
			if (expr.accept(annotationCheck) != null) {
				throw CannotApplyRuleException.annotationTerm(this, term);
			}
		}

		// Check whether there are <= 1 assertions left in the script at the moment
		int assertionCount = 0;
		for (Expression expr : script.getTopLevelExpressions()) {
			if (expr.accept(assertionCheck) != null) {
				assertionCount++;
			}
		}
		if (assertionCount <= 1) {
			throw CannotApplyRuleException.moreThanOneAssertion(this);
		}

	}

}
