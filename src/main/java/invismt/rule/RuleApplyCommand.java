/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Script;
import invismt.tree.PTElement;
import invismt.tree.TreeCommand;
import invismt.tree.exception.CommandFailedToExecute;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tim Junginger
 */
public class RuleApplyCommand implements TreeCommand {

	private final Rule rule;
	private final PTElement element;
	private final List<PTElement> children;

	/**
	 * Constructor.
	 *
	 * @param rule    the rule that should be applied.
	 * @param element the {@link PTElement} on which the rule should be applied.
	 */
	public RuleApplyCommand(Rule rule, PTElement element) {
		this.rule = rule;
		this.element = element;
		children = new ArrayList<>();
	}

	/**
	 * @return the rule that should be applied.
	 */
	public Rule getRule() {
		return rule;
	}

	/**
	 * @return the {@link PTElement} on which the rule should be applied.
	 */
	public PTElement getPTElement() {
		return element;
	}

	@Override
	public void execute() throws CommandFailedToExecute {
		if (element.getChildren() == null || !element.getChildren().isEmpty()) {
			throw new CommandFailedToExecute("A rule has already been applied on the target element.");
		}
		for (Script s : rule.getCreatedScripts()) {
			PTElement child = new PTElement(element, s, rule, element.getProofTree());
			children.add(child);
			element.addChildren(child);
		}
	}

	@Override
	public void redo() throws CommandFailedToExecute {
		if (element.getChildren() == null || !element.getChildren().isEmpty()) {
			throw new CommandFailedToExecute("A rule has already been applied on the target element.");
		}
		for (PTElement e : children) {
			element.addChildren(e);
		}
	}

	@Override
	public void undo() {
		for (PTElement child : children) {
			element.rmChildren(child);
		}
	}

	@Override
	public String toString() {
		return rule.getName();
	}

}
