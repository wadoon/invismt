/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Expression;

/**
 * @author Alicia
 */
public interface ExpressionComparator {

	/**
	 * Compares to expressions and returns true iff they are considered equivalent.
	 *
	 * @param expr1 one of the expressions.
	 * @param expr2 one of the expressions.
	 * @return true iff the expressions are equivalent.
	 */
	boolean equals(Expression expr1, Expression expr2);

	/**
	 * ExpressionComparator that considers identical expression objects as equivalent.
	 */
	class IdenticalComparator implements ExpressionComparator {

		@Override
		public boolean equals(Expression expr1, Expression expr2) {
			return expr1 == expr2;
		}

	}

	/**
	 * ExpressionComparator that considers equal expression objects as equivalent.
	 */
	class EqualComparator implements ExpressionComparator {

		@Override
		public boolean equals(Expression expr1, Expression expr2) {
			return expr1.equals(expr2);
		}

	}

}
