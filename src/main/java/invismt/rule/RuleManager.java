/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

/**
 * @author Tim Junginger
 */
public class RuleManager {

	private final List<RuleWrapper> wrappers;

	/**
	 * Constructor.
	 */
	public RuleManager() {
		wrappers = new ArrayList<>();
		// Load formatters using ServiceLoader
		ServiceLoader.load(RuleWrapper.class).forEach(wrappers::add);
	}

	/**
	 * @return list of available rules
	 */
	public List<RuleWrapper> getRuleWrappers() {
		return new ArrayList<>(wrappers);
	}

}
