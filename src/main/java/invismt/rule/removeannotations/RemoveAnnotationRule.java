/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.removeannotations;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.AnnotationTerm;
import invismt.grammar.term.Term;
import invismt.rule.BoundVariableReplacer;
import invismt.rule.CannotApplyRuleException;
import invismt.rule.CheckConstruct;
import invismt.rule.ExpressionComparator;
import invismt.rule.Rule;
import invismt.rule.TermReplacer;

import java.util.List;

/**
 * An inference rule that copies an annotation term and removes all attributes from the copy.
 * That ensures that any definitions made by a :named attribute are kept, but the term can be edited by inference rules
 * that cannot work on annotation terms now.
 *
 * @author Alicia
 */
public class RemoveAnnotationRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "RemoveAnnotation";
	private static final boolean CONFLUENT = true;
	private Term removedAttributeTerm;
	private SyntaxCommand.Assert assertion;

	/**
	 * Constructor.
	 *
	 * @param script the {@link Script} on which this rule is supposed to be applied on.
	 * @param term   the {@link AnnotationTerm} that contains the assertion that should be removed.
	 */
	public RemoveAnnotationRule(Script script, AnnotationTerm term) {
		super(RULE_NAME, CONFLUENT, script, term);
		checkParams();
		generateScripts();
	}

	private void checkParams() {
		AnnotationTerm term = (AnnotationTerm) expression;

		// Check whether term is contained in assertion
		Expression top = script.getTopLevel(term);
		assertion = top.accept(new CheckConstruct<>() {
			@Override
			public SyntaxCommand.Assert visit(SyntaxCommand.Assert assertion) {
				return assertion;
			}
		});
		if (assertion == null) {
			throw CannotApplyRuleException.notInAssertion(this, term);
		}

		removedAttributeTerm = term.getTerm();
	}

	private void generateScripts() {
		TermReplacer replacer = new AnnotationReplacer();
		if (!replacer.applicable(assertion.getTerm())) {
			throw CannotApplyRuleException.cannotReplace(assertion.getTerm(), (Term) expression, removedAttributeTerm);
		}
		Term replaced = replacer.substitute(assertion.getTerm());
		createdScripts
				.add(script.addTopLevelListInstead(assertion, List.of(assertion, new Assert(replaced).deepCopy())));
	}

	private class AnnotationReplacer extends BoundVariableReplacer {

		AnnotationReplacer() {
			super((Term) expression, removedAttributeTerm, new ExpressionComparator.IdenticalComparator());
			replacer = new ReplacementVisitor();
		}

		protected class ReplacementVisitor extends BoundVariableReplacer.ReplacementVisitor {
			@Override
			public Term visit(AnnotationTerm annotationTerm) {
				if (!applicable()) {
					return annotationTerm;
				}
				if (comparator.equals(annotationTerm, oldT)) {
					return newT.deepCopy();
				}
				Term innerReplaced = (Term) annotationTerm.getTerm().accept(this);
				if (innerReplaced.equals(annotationTerm.getTerm())) {
					return annotationTerm;
				}
				return new AnnotationTerm(innerReplaced, annotationTerm.getAttributes());
			}
		}

	}

}
