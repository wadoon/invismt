/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.rewrite;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import invismt.rule.CheckConstruct;
import invismt.rule.ExpressionComparator;
import invismt.rule.NoBoundVariableReplacer;
import invismt.rule.Rule;
import invismt.rule.TermReplacer;

import java.util.List;
import java.util.Optional;

/**
 * @author Tim Junginger
 */
public class RewriteRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "Rewrite";
	private static final String EQUAL_PARAMS = "Replaced term is equal to new term, nothing would be replaced.";
	private static final boolean CONFLUENT = true;

	private final Term term;
	private final Term rewrittenTerm;
	private final TermReplacer replacer;

	/**
	 * Constructor.
	 *
	 * @param script        the {@link Script} on which this rule is supposed to be applied on.
	 * @param term          the {@link Term} that should be rewritten.
	 * @param rewrittenTerm the rewritten {@link Term}.
	 */
	public RewriteRule(Script script, Term term, Term rewrittenTerm) {
		super(RULE_NAME, CONFLUENT, script, term);
		replacer = new NoBoundVariableReplacer(term, rewrittenTerm, new ExpressionComparator.IdenticalComparator());
		this.term = term;
		this.rewrittenTerm = rewrittenTerm;
		checkParams();
		generateNewStates();
	}

	private void checkParams() {
		Expression top = script.getTopLevel(term);
		SyntaxCommand.Assert command = top.accept(new CheckConstruct<>() {
			@Override
			public SyntaxCommand.Assert visit(SyntaxCommand.Assert assertion) {
				return assertion;
			}
		});
		if (command == null) {
			throw CannotApplyRuleException.notTopLevelTerm(this, term);
		}
		boolean equal = term.equals(rewrittenTerm);
		if (equal) {
			throw new CannotApplyRuleException(EQUAL_PARAMS);
		}
		if (!replacer.applicable(command.getTerm())) {
			throw CannotApplyRuleException.cannotReplace(command.getTerm(), term, rewrittenTerm);
		}
	}

	private void generateNewStates() {
		SyntaxCommand.Assert top = (SyntaxCommand.Assert) script.getTopLevel(term);
		QualIdentifier equal = new QualIdentifier(new Identifier(new SymbolToken("="), List.of()), Optional.empty());

		IdentifierTerm equalTS = new IdentifierTerm(equal, List.of(term.deepCopy(), rewrittenTerm.deepCopy()));

		IdentifierTerm notEqualTS = new IdentifierTerm(ExpressionFactory.NOT.deepCopy(), List.of(equalTS));

		Assert assertNotEqual = new Assert(notEqualTS);
		Assert assertEqual = new Assert(equalTS);

		Expression newTop = new SyntaxCommand.Assert(replacer.substitute(top.getTerm()));

		Script replacedScript = script.addTopLevelListInstead(top, List.of(assertEqual, newTop));
		Script unsatScript = script.addTopLevelListInstead(top, List.of(assertNotEqual, top));
		createdScripts.add(replacedScript);
		createdScripts.add(unsatScript);

	}
}
