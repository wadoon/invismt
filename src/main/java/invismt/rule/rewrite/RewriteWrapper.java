/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.rewrite;

import invismt.exception.MissingUserInputException;
import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.term.AnnotationTerm;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.LetTerm;
import invismt.grammar.term.MatchTerm;
import invismt.grammar.term.QuantifiedTerm.ExistsTerm;
import invismt.grammar.term.QuantifiedTerm.ForallTerm;
import invismt.grammar.term.Term;
import invismt.parser.TermParserDispatcher;
import invismt.rule.CheckConstruct;
import invismt.rule.Rule;
import invismt.rule.RuleWrapper;
import javafx.scene.control.TextInputDialog;

import java.util.Optional;

/**
 * {@link invismt.rule.RuleWrapper} for the {@link RewriteRule}.
 *
 * @author Alicia, Tim Junginger
 */
public class RewriteWrapper implements RuleWrapper {

	private static final String DIALOG_HEADER_TEXT = "Rewrite";
	private static final String DIALOG_CONTENT_TEXT = "Enter the new term:";

	@Override
	public String getName() {
		return RewriteRule.RULE_NAME;
	}

	@Override
	public Rule createRule(Script script, Expression expr) throws IllegalArgumentException, MissingUserInputException {
		if (!mayApply(script, expr)) {
			throw new IllegalArgumentException();
		}
		// Dialog for entering instance term:
		TextInputDialog dialog = new TextInputDialog();
		dialog.setHeaderText(DIALOG_HEADER_TEXT);
		dialog.setContentText(DIALOG_CONTENT_TEXT);
		Optional<String> str = dialog.showAndWait();
		if (str.isEmpty()) {
			throw new MissingUserInputException();
		}
		TermParserDispatcher parser = new TermParserDispatcher();
		Term newTerm = parser.parseInto(str.get());
		return new RewriteRule(script, (Term) expr, newTerm);
	}

	@Override
	public boolean mayApply(Script script, Expression expr) {
		CheckConstruct<Term> termCheck = new CheckConstruct<>() {

			private Term check(Term term) {
				if (!script.existsIdenticallySomewhere(term)) {
					return null;
				}
				return term;
			}

			@Override
			public ForallTerm visit(ForallTerm term) {
				return (ForallTerm) check(term);
			}

			@Override
			public ExistsTerm visit(ExistsTerm term) {
				return (ExistsTerm) check(term);
			}

			@Override
			public LetTerm visit(LetTerm term) {
				return (LetTerm) check(term);
			}

			@Override
			public AnnotationTerm visit(AnnotationTerm term) {
				return (AnnotationTerm) check(term);
			}

			@Override
			public MatchTerm visit(MatchTerm term) {
				return (MatchTerm) check(term);
			}

			@Override
			public ConstantTerm visit(ConstantTerm term) {
				return (ConstantTerm) check(term);
			}

			@Override
			public IdentifierTerm visit(IdentifierTerm term) {
				return (IdentifierTerm) check(term);
			}

		};
		if (expr.accept(termCheck) == null) {
			return false;
		}
		Expression top = script.getTopLevel(expr);
		SyntaxCommand.Assert command = top.accept(new CheckConstruct<>() {
			@Override
			public SyntaxCommand.Assert visit(SyntaxCommand.Assert assertion) {
				return assertion;
			}
		});
		return command != null;
	}

}
