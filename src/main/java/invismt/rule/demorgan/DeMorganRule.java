/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.demorgan;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.Token.ReservedWord;
import invismt.grammar.Token.ReservedWordName;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm;
import invismt.grammar.term.QuantifiedTerm.ExistsTerm;
import invismt.grammar.term.QuantifiedTerm.ForallTerm;
import invismt.grammar.term.Term;
import invismt.rule.BoundVariableReplacer;
import invismt.rule.CannotApplyRuleException;
import invismt.rule.CheckConstruct;
import invismt.rule.ExpressionComparator;
import invismt.rule.Rule;
import invismt.rule.TermReplacer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * De Morgan's laws are a pair of transformation rules that either transform a
 * negation of a disjunction into the conjunction of the negations or a negation
 * of a conjunction into the disjunction of the negations.
 *
 * @author Alicia
 */
public class DeMorganRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "DeMorgan";
	private static final boolean CONFLUENT = true;
	private static final Map<QualIdentifier, QualIdentifier> ALLOWED_IDENTIFIERS = Map
			.ofEntries(Map.entry(ExpressionFactory.AND, ExpressionFactory.OR),
					Map.entry(ExpressionFactory.OR, ExpressionFactory.AND));

	private final ResourceBundle errorMessages;
	boolean not;
	private String errorMessage;
	private QualIdentifier newInnerIdentifier;
	private Assert top;
	private Term newInnerTerm;

	/**
	 * Constructor
	 *
	 * @param script the {@link Script} on which this rule is supposed to be applied
	 *               on.
	 * @param term   the {@link IdentifierTerm} on which this rule should be
	 *               applied.
	 */
	public DeMorganRule(Script script, IdentifierTerm term) {
		super(RULE_NAME, CONFLUENT, script, term);
		errorMessages = ResourceBundle.getBundle("i18n.ErrorMessages");
		checkParams();
		checkParamsIdentifier();
		generateNewStates();
	}

	/**
	 * Constructor
	 *
	 * @param script the {@link Script} on which this rule is supposed to be applied
	 *               on.
	 * @param term   the {@link QuantifiedTerm} on which this rule should be
	 *               applied.
	 */
	public DeMorganRule(Script script, QuantifiedTerm term) {
		super(RULE_NAME, CONFLUENT, script, term);
		errorMessages = ResourceBundle.getBundle("i18n.ErrorMessages");
		checkParams();
		checkParamsQuantified(term);
		not = false;
		generateNewStates();
	}

	private void checkParams() {
		errorMessage = errorMessages.getString("cannotApplyRule.deMorgan");
		Expression toplevelExpression = script.getTopLevel(expression);
		top = toplevelExpression.accept(new CheckConstruct<>() {
			@Override
			public Assert visit(Assert assertion) {
				return assertion;
			}
		});
		if (top == null) {
			throw CannotApplyRuleException.notInAssertion(this, (Term) expression);
		}
	}

	private void checkParamsQuantified(QuantifiedTerm quantifiedTerm) {
		if (quantifiedTerm == null) {
			throw new CannotApplyRuleException(errorMessage);
		}
		if (quantifiedTerm.getQuantifier().equals(new ReservedWord(ReservedWordName.FORALL))) {
			newInnerTerm = new ExistsTerm(quantifiedTerm.getVariables(), negate(quantifiedTerm.getTerm()));
		} else {
			newInnerTerm = new ForallTerm(quantifiedTerm.getVariables(), negate(quantifiedTerm.getTerm()));
		}
	}

	private void checkParamsIdentifier() {
		IdentifierTerm term = (IdentifierTerm) expression;
		not = term.getIdentifier().equals(ExpressionFactory.NOT) && term.getIdentifiedTerms().size() == 1;
		IdentifierTerm innerTerm;
		if (not) {
			innerTerm = term.getIdentifiedTerms().get(0).accept(new CheckConstruct<>() {
				@Override
				public IdentifierTerm visit(IdentifierTerm term) {
					return term;
				}
			});
		} else {
			innerTerm = term;
		}

		if (innerTerm == null) {
			checkParamsQuantified(term.getIdentifiedTerms().get(0).accept(new CheckConstruct<>() {
				@Override
				public ForallTerm visit(ForallTerm term) {
					return term;
				}

				@Override
				public ExistsTerm visit(ExistsTerm term) {
					return term;
				}
			}));
			return;
		}

		for (Map.Entry<QualIdentifier, QualIdentifier> possibleIdentifier : ALLOWED_IDENTIFIERS.entrySet()) {
			if (innerTerm.getIdentifier().equals(possibleIdentifier.getKey())) {
				newInnerIdentifier = possibleIdentifier.getValue();
				break;
			}
		}
		if (newInnerIdentifier == null) {
			throw new CannotApplyRuleException(errorMessage);
		}

		List<Term> invertedTerms = new ArrayList<>();
		for (Term negateTerm : innerTerm.getIdentifiedTerms()) {
			invertedTerms.add(negate(negateTerm));
		}
		newInnerTerm = new IdentifierTerm(newInnerIdentifier.deepCopy(), invertedTerms);
	}

	private IdentifierTerm negate(Term term) {
		return new IdentifierTerm((ExpressionFactory.NOT).deepCopy(), List.of(term));
	}

	private void generateNewStates() {
		Term term = (Term) expression;
		Term newTerm = newInnerTerm;
		if (!not) {
			newTerm = new IdentifierTerm(ExpressionFactory.NOT.deepCopy(), List.of(newInnerTerm));
		}
		TermReplacer replacer = new BoundVariableReplacer(term, newTerm,
				new ExpressionComparator.IdenticalComparator());
		Assert newTop = new Assert(replacer.substitute(top.getTerm()));
		createdScripts.add(script.addTopLevelListInstead(term, List.of(newTop)));
	}

}
