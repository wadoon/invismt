/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.instantiation;

import invismt.exception.MissingUserInputException;
import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.QuantifiedTerm.ForallTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.Term;
import invismt.parser.TermParserDispatcher;
import invismt.rule.CheckConstruct;
import invismt.rule.Rule;
import invismt.rule.RuleWrapper;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;

import java.util.Optional;

/**
 * {@link RuleWrapper} for the {@link InstantiationRule}.
 *
 * @author Alicia, Tim Junginger
 */
public class InstantiationWrapper implements RuleWrapper {

	private static final String DIALOG_HEADER_TEXT = "Instantiation";
	private static final String SELECT_VARIABLE_TO_INSTANTIATE = "Select variable to instantiate:";
	private static final String ENTER_AN_INSTANCE = "Enter an instance:";

	@Override
	public String getName() {
		return InstantiationRule.RULE_NAME;
	}

	@Override
	public Rule createRule(Script script, Expression expr) throws IllegalArgumentException, MissingUserInputException {

		ForallTerm forallTerm = expr.accept(new CheckConstruct<>() {
			@Override
			public ForallTerm visit(ForallTerm term) {
				return term;
			}
		});
		if (forallTerm == null) {
			throw new IllegalArgumentException();
		}

		// Show a dialog to let the user choose which the variable that should be instantiated.
		ChoiceDialog<SortedVariable> variableChoiceDialog = new ChoiceDialog<>(forallTerm.getVariables().get(0),
				forallTerm.getVariables());
		variableChoiceDialog.setHeaderText(DIALOG_HEADER_TEXT);
		variableChoiceDialog.setContentText(SELECT_VARIABLE_TO_INSTANTIATE);
		Optional<SortedVariable> mayVar = variableChoiceDialog.showAndWait();
		if (mayVar.isEmpty()) {
			throw new MissingUserInputException();
		}
		SortedVariable sortedVar = mayVar.get();

		// Show a dialog to ask the user for an instance term.
		TextInputDialog instanceTermDialog = new TextInputDialog();
		instanceTermDialog.setHeaderText(DIALOG_HEADER_TEXT);
		instanceTermDialog.setContentText(ENTER_AN_INSTANCE);
		Optional<String> str = instanceTermDialog.showAndWait();
		if (str.isEmpty()) {
			throw new MissingUserInputException();
		}

		TermParserDispatcher parser = new TermParserDispatcher();
		Term instanceTerm = parser.parseInto(str.get());
		return new InstantiationRule(script, forallTerm, sortedVar, instanceTerm);
	}

	@Override
	public boolean mayApply(Script script, Expression expr) {
		CheckConstruct<ForallTerm> termCheck = new CheckConstruct<>() {
			@Override
			public ForallTerm visit(ForallTerm term) {
				if (!script.existsIdenticallySomewhere(term)) {
					return null;
				}
				Assert topLevel = script.getTopLevel(term).accept(new CheckConstruct<>() {
					@Override
					public Assert visit(SyntaxCommand.Assert assertion) {
						return assertion;
					}
				});
				if (topLevel == null) {
					return null;
				}
				return term;
			}
		};
		return expr.accept(termCheck) != null;
	}

}
