/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.skolemization;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.Token;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm;
import invismt.grammar.term.QuantifiedTerm.ExistsTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import invismt.rule.ExpressionComparator;
import invismt.rule.NoBoundVariableReplacer;
import invismt.rule.Rule;
import invismt.rule.TermReplacer;

import java.util.List;
import java.util.Optional;

/**
 * Inference rule that removes a variable binding from an exists quantifier and introduces a new constant representing
 * the previously bound variable. If the variable is the only variable bound by the exists quantifier the exists
 * quantifier is removed.
 * <p/>
 * (exists (x Int) (y Int) (phi x y))
 * <=>
 * (declare-const x Int)
 * (exists (y Int) (phi x y))
 *
 * @author Alicia, Tim Junginger
 */
public class SkolemizationRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "Skolemization";
	private static final boolean CONFLUENT = true;

	private final SortedVariable variable;
	private final Token.SymbolToken newSymbol;

	/**
	 * Constructor.
	 *
	 * @param script    the {@link Script} on which this rule is supposed to be applied on.
	 * @param term      the {@link Term} that contains the variable that should be skolemized.
	 * @param variable  the {@link SortedVariable} that should be skolemized.
	 * @param newSymbol a new {@link Token.SymbolToken} to represent the skolemized variable.
	 */
	public SkolemizationRule(Script script, ExistsTerm term, SortedVariable variable, Token.SymbolToken newSymbol) {
		super(RULE_NAME, CONFLUENT, script, term);
		this.variable = variable;
		this.newSymbol = newSymbol;
		checkParams();
		generateNewStates();
	}

	private void checkParams() {
		ExistsTerm term = (ExistsTerm) expression;

		// Check if a constant with the newSymbol name can be created (if it is currently not in use anywhere)
		if (script.containsEqualExpression(newSymbol)) {
			throw CannotApplyRuleException.scriptAlreadyContains(script, newSymbol);
		}

		// Check whether the term is a top level term in an assertion
		if (!script.getTopLevel(term).equals(new SyntaxCommand.Assert(term))) {
			throw CannotApplyRuleException.notTopLevelTerm(this, term);
		}

		// Check whether the sorted variable is contained
		if (!term.getVariables().contains(variable)) {
			throw CannotApplyRuleException.notContained(this, variable, term);
		}
	}

	private void generateNewStates() {
		ExistsTerm term = (ExistsTerm) expression;
		// Create a declare expression for the new constant
		SyntaxCommand.DeclareConstant declare = new SyntaxCommand.DeclareConstant(newSymbol,
				variable.getVariableType());
		// Get top level element of the respective term
		Expression top = script.getTopLevel(term);

		Term oldSymbolTerm = new IdentifierTerm(
				new QualIdentifier(new Identifier(variable.getVariableName(), List.of()), Optional.empty()), List.of());
		Term newSymbolTerm = new IdentifierTerm(
				new QualIdentifier(new Identifier(newSymbol, List.of()), Optional.empty()), List.of());
		TermReplacer replacer = new NoBoundVariableReplacer(oldSymbolTerm, newSymbolTerm,
				new ExpressionComparator.EqualComparator());
		Term inner = replacer.substitute(term.getTerm());
		Term newTerm;

		// Create new Term depending on whether or not exists can be removed
		if (term.getVariables().size() == 1) {
			newTerm = inner;
		} else {
			List<SortedVariable> newVars = term.getVariables();
			newVars.remove(variable);
			newTerm = new QuantifiedTerm.ExistsTerm(newVars, inner);
		}

		// Replace the old term with the newTerm
		Expression newTop = new SyntaxCommand.Assert(newTerm);
		// Return one new script with the variable skolemized
		createdScripts.add(script.addTopLevelListInstead(top, List.of(declare, newTop)));
	}

}
