/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.induction;

import invismt.exception.MissingUserInputException;
import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.Token;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.QuantifiedTerm.ForallTerm;
import invismt.grammar.term.SortedVariable;
import invismt.rule.CheckConstruct;
import invismt.rule.Rule;
import invismt.rule.RuleWrapper;
import javafx.scene.control.ChoiceDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * {@link RuleWrapper} for the {@link InductionRule}.
 *
 * @author Alicia
 */
public class InductionWrapper implements RuleWrapper {

	private static final String CHOOSE_INDUCTION_VARIABLE_MSG = "Choose the variable to carry out the induction on: ";
	private static final String VARIABLE_CHOICE_MSG = "Variable Choice";
	private static final String VARIABLE_NOT_CHOSEN_MSG = "No variable has been chosen.";

	private static final Sort INTEGER_SORT = new Sort(new Identifier(new Token.SymbolToken("Int"), new ArrayList<>()),
			new ArrayList<>());

	@Override
	public String getName() {
		return InductionRule.RULE_NAME;
	}

	@Override
	public Rule createRule(Script script, Expression expr) throws IllegalArgumentException, MissingUserInputException {
		if (!mayApply(script, expr)) {
			throw new IllegalArgumentException();
		}

		ForallTerm forallTerm = (ForallTerm) expr;

		// Collect all integer variables that are bound by the quantifier.
		List<SortedVariable> integerVariables = new ArrayList<>();
		for (SortedVariable sortedVar : forallTerm.getVariables()) {
			if (sortedVar.getVariableType().equals(INTEGER_SORT)) {
				integerVariables.add(sortedVar);
			}
		}

		// Show a dialog to the user to fetch the variable that should be induced over.
		ChoiceDialog<SortedVariable> variableChoice = new ChoiceDialog<>(integerVariables.get(0), integerVariables);
		variableChoice.setContentText(CHOOSE_INDUCTION_VARIABLE_MSG);
		variableChoice.setHeaderText(VARIABLE_CHOICE_MSG);
		final Optional<SortedVariable> inductionVariable = variableChoice.showAndWait();
		if (inductionVariable.isEmpty()) {
			throw new MissingUserInputException(VARIABLE_NOT_CHOSEN_MSG);
		}

		return new InductionRule(script, forallTerm, inductionVariable.get());
	}

	@Override
	public boolean mayApply(Script script, Expression expr) {
		CheckConstruct<ForallTerm> termCheck = new CheckConstruct<>() {
			@Override
			public ForallTerm visit(ForallTerm term) {
				if (!script.existsIdenticallySomewhere(term)) {
					return null;
				}

				Assert topLevel = script.getTopLevel(term).accept(new CheckConstruct<>() {
					@Override
					public Assert visit(SyntaxCommand.Assert assertion) {
						return assertion;
					}
				});
				if (topLevel == null) {
					return null;
				}

				boolean containsIntegerVariable = false;
				for (SortedVariable sortedVar : term.getVariables()) {
					if (sortedVar.getVariableType().equals(INTEGER_SORT)) {
						containsIntegerVariable = true;
						break;
					}
				}

				if (!containsIntegerVariable) {
					return null;
				}

				return term;
			}
		};

		return expr.accept(termCheck) != null;
	}

}
