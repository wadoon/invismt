/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionBaseVisitor;

/**
 * Generic class that is used to check {@link Expression expressions} for some property.
 * The class can be extended or anonymously implemented.
 * <p>
 * Its visit-methods return null by default.
 * <p>
 * {@see InductionWrapper} for exemplary use.
 *
 * @param <T>
 * @author Tim Junginger
 */
public class CheckConstruct<T extends Expression> extends ExpressionBaseVisitor<T> {

}
