package invismt.events;

import invismt.solver.Solver;

/**
 * Event to check scripts with the given solver.
 * @author Tim Junginger
 *
 */
public class CheckScriptsEvent {
	
	private final Solver solver;
	
	/**
	 * Creates a new CheckScriptsEvent.
	 * @param solver
	 */
	public CheckScriptsEvent(Solver solver) {
		this.solver = solver;
	}
	
	/**
	 * Gets the solver with which to check the scripts.
	 * @return solver
	 */
	public Solver getSolver() {
		return this.solver;
	}

}
