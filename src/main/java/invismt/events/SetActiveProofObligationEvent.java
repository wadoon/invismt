/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.tree.ProofObligation;

/**
 * An event signalling that a new active {@link ProofObligation} should be set.
 * <p>
 * For example, a SetActiveProofObligationEvent object can be posted on an
 * {@link com.google.common.eventbus.EventBus} and then
 * be handled by any class that has access to the EventBus to set the currently
 * shown ProofObligation in the view to the one indicated by the event.
 * <p>
 * See
 * {@link
 * invismt.controller.ProofTreeController#handleSetActiveProofObligationEvent(
 *SetActiveProofObligationEvent)}
 * for exemplary use.
 *
 * @author Tim Junginger
 */
public class SetActiveProofObligationEvent {

	private ProofObligation obl;

	/**
	 * Creates a new event with the given {@link ProofObligation}.
	 *
	 * @param obl is the ProofObligation
	 */
	public SetActiveProofObligationEvent(ProofObligation obl) {
		this.obl = obl;
	}

	/**
	 * @return obligation so set active
	 */
	public ProofObligation getObligation() {
		return this.obl;
	}
}
