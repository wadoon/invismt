/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.tree.ListOfProofObligation;

/**
 * An event signalling that a new {@link ListOfProofObligation} should be set.
 * <p>
 * For example, a SetActiveProofObligationEvent object can be posted on an
 * {@link com.google.common.eventbus.EventBus} and then
 * be handled by any class that has access to the EventBus to set the currently
 * shown ListOfProofObligation in the view to the one indicated by the event.
 * <p>
 * See
 * {@link
 * invismt.controller.ProofObligationController#handleSetListOfProofObligationEvent(
 *SetListOfProofObligationEvent)}
 * for exemplary use.
 *
 * @author Tim Junginger
 */
public class SetListOfProofObligationEvent {

	private ListOfProofObligation list;

	/**
	 * Creates a new Event with the given {@link ListOfProofObligation}.
	 *
	 * @param list is the List of ProofObligation
	 */
	public SetListOfProofObligationEvent(ListOfProofObligation list) {
		this.list = list;
	}

	/**
	 * Returns this event's {@link ListOfProofObligation}.
	 *
	 * @return list of this event
	 */
	public ListOfProofObligation getList() {
		return list;
	}

}
