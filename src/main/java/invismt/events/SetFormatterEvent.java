/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.formatting.Formatter;

/**
 * An event that can be used to signal that a new {@link Formatter}
 * is to be set.
 * <p>
 * For example, a SetFormatterEvent object can be posted on an
 * {@link com.google.common.eventbus.EventBus} and then
 * be handled by any class that has access to the EventBus
 * to change the {@link Formatter} that is used to create the
 * representation of {@link invismt.grammar.Expression Expressions}.
 * <p>
 * See {@link invismt.controller.ScriptController#setFormatter(SetFormatterEvent)}
 * for exemplary use.
 */
public class SetFormatterEvent {

	private final Formatter newFormatter;

	/**
	 * Create a new SetFormatterEvent by calling its constructor with the
	 * {@link Formatter} that is to be set.
	 *
	 * @param newFormatter {@link Formatter} to be set
	 */
	public SetFormatterEvent(Formatter newFormatter) {
		this.newFormatter = newFormatter;
	}

	/**
	 * @return {@link Formatter} that is to be set according to this event
	 */
	public Formatter getNewFormatter() {
		return newFormatter;
	}
}
