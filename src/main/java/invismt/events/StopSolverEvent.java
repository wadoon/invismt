/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.tree.PTElement;

/**
 * An event signalling that a proof process on a {@link PTElement} is to be stopped.
 * <p>
 * For example, a StopSolverEvent object can be posted on an
 * {@link com.google.common.eventbus.EventBus} and then be handled by any
 * class that has access to the EventBus to stop a
 * {@link invismt.solver.Solver} on the PTElement's script.
 * <p>
 * See
 * {@link invismt.controller.ProofTreeController#handleStopSolverEvent(StopSolverEvent)}
 * for exemplary use.
 *
 * @author Tim Junginger
 */
public class StopSolverEvent {

	private final PTElement element;

	/**
	 * Creates a new event with the given {@link PTElement}. If element is null then
	 * the solver on the currently active element should be stopped.
	 *
	 * @param element is the PTElement
	 */
	public StopSolverEvent(PTElement element) {
		this.element = element;
	}

	/**
	 * @return element on which to stop the solver
	 */
	public PTElement getElement() {
		return element;
	}

}
